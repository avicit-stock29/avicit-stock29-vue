module.exports = {
  /**
   * 指定js(运行|版本)所处环境，以便提供所指定(环境|版本)默认的变量
   * 官网详细说明 https://eslint.org/docs/user-guide/configuring#specifying-environments
   */
  env: {
    //如为 false 或不配置, 在js文件中出现 require, exports等 NODE API 全局变量时，会提示 'is not defined' 警告
    node: true,
    //如为 false 或不配置, 在js文件中出现 Promise 等es6版本新增的对象或方法时，提示错误警告
    es6: false,
    //如为 false 或不配置, 在js文件中出现 window, document 等 游览器API 全局变量时，会提示 'is not defined' 警告
    browser: true
  },
  /**
   * eslint 规则继承自何处
   * 即如果未配置rules(自定义)规则，也未指定 extends(继承)规则，eslint便没有规则进行校验
   * 相当于，你买了部手机，但手机没有电，啥事也干不了
   * 官网详细说明 https://eslint.org/docs/user-guide/configuring#extending-configuration-files
   */
  extends: [
    // 使用eslint推荐规则检查
    'eslint:recommended',
    // 使用VUE-eslint中A级必要的规则检查
    'plugin:vue/essential',
    // 使用VUE-eslint中B级强烈推荐的规则检查
    'plugin:vue/strongly-recommended',
    // 使用prettier推荐规则检查
    'plugin:prettier/recommended'
  ],
  /**
   * 设置自定义全局变量，避免自定义全局变量出现 no-undef 警告
   * 有时候我们引入了第三方包(插件)，或我们自己写的一个全局方法；其提供了一个全局变量供我们使用，例如 axios (一个请求类库)
   * eslint 会校验 变量必须声明才能使用，但是第三方库，或我们自己的全局方法，不会在每个js文件中声明引入，这个时候便会出现 no-undef 警告
   * 因此，将其在 globals中配置即可
   * 官网详细说明 https://eslint.org/docs/user-guide/configuring#specifying-globals
   * 注意：
   * false | "readable" 等同于 "readonly"  不能重新定义(赋值)
   * true | "writeable" 等同于 "writable" 可重新定义(赋值)
   */
  globals: {
    Promise: 'readonly',
    Set:'readonly',
    Uint8Array:'readonly',
    XMLHttpRequest:'readonly',
    ActiveXObject:'readonly',
    Map:'readonly'
  },
  /**
   * 指定 js 语法版本，例如
   * "ecmaVersion": 6, 表明 启用es6语法进行校验
   * 使用 3, 5 , 6(默认) , 7, 8, 9, 10, 11 设置你预期的js语法版本
   * 也可以根据js版本的年份设置，例如
   * 2015(同 6)  2016(同 7)  2017(同 8)  2018(同 9)  2019(同 10)  2020(同 11)
   * 官网详细说明 https://eslint.org/docs/user-guide/configuring#specifying-parser-options
   */
  parserOptions: {
    ecmaVersion: 6,
    //类型为module，因为代码使用了import/export的语法的模块
    sourceType: 'module',
    //解析器，这里我们使用babel-eslint
    parser: 'babel-eslint'
  },
  /**
   * 指定 一个插件规则包，在eslint本身规则不满足情况下扩展校验规则
   * 例如本处vue引入的就是针对vue代码校验的规则
   * 对应的包名为：eslint-plugin-vue
   * 官网详细说明 https://eslint.org/docs/user-guide/configuring#using-the-configuration-from-a-plugin
   */
  plugins: ['prettier', 'vue'],
  /**
   * 自定义 eslint 校验规则
   * off 或 0  关闭校验
   * warn 或 1 提示警告
   * error 或 2 提示错误
   * 官网详细说明 https://eslint.org/docs/user-guide/configuring#configuring-rules
   * 可配置校验规则 https://eslint.org/docs/rules/
   * 可配置校验规则-中文 https://eslint.bootcss.com/docs/rules/
   */
  rules: {
    // 本处设置只是忽略prettier和eslint共性问题的报错，prettier格式化标准需要写在.prettierrc中
    'prettier/prettier': [
      'error',
      {
        // 使用单引号
        singleQuote: true,
        // 行尾需要有分号
        semi: true,
        // 箭头函数参数括号
        arrowParens: 'avoid',
        // 不允许使用尾逗号
        trailingComma: 'none'
      }
    ],
    // fix:规定了参数是否需要圆括号包围
    'arrow-parens': 0,
    // fix:禁止使用拖尾逗号
    'comma-dangle': ['error', 'never'],
    // fix:规定generator函数中星号在函数名前
    'generator-star-spacing': ['error', { before: true, after: false }],
    // 禁止使用debugger语句
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    // fix:该规则规定文件最后强制换行，仅需留一空行
    'eol-last': 0,
    // fix:函数定义时，function关键字后面的小括号前是否需要加空格
    'space-before-function-paren': 0,
    // fix:要求使用 let 或 const 而不是 var
    'no-var': 'error',
    // 禁用alert、prompt 和 confirm
    'no-alert': 'error',
    // fix:要求对象字面量简写语法
    // var foo = {
    //   w: function() {},
    //   x: function *() {},
    //   [y]: function() {},
    //   z: z
    // };
    // var foo = {
    //   w() {},
    //   *x() {},
    //   [y]() {},
    //   z
    // };
    'object-shorthand': ['error', 'always'],
    // 禁止未使用过的变量
    // ignoreRestSiblings 选项是个布尔类型 (默认: false)。使用 Rest 属性 可能会“省略”对象中的属性，但是默认情况下，其兄弟属性被标记为 “unused”。使用该选项可以使 rest 属性的兄弟属性被忽略。
    // let data = { type:12 a: 1, b: 2 };
    // let { type, ...a } = data;
    'no-unused-vars': [
      'error', 
      { 
        ignoreRestSiblings: true,
        caughtErrors: "none" ,
        // 参数不检查
        args: "none" 
      }
    ],
    // 禁用未声明的隐式全局变量
    'no-undef': 'error',
    // 不允许空块语句 ,catch除外
    'no-empty': [
      "error",
      {
        allowEmptyCatch: true
      }
    ],
    // fix:要求箭头函数的箭头之前或之后有空格
    'arrow-spacing': [
      2,
      {
        before: true,
        after: true
      }
    ],
    // fix:if while function 后面的{必须与if在同一行
    'brace-style': [
      2,
      '1tbs',
      {
        allowSingleLine: true
      }
    ],
    // 关闭强制使用骆驼拼写法命名约定，平台命名规范参考平台手册
    camelcase: [
      0,
      {
        properties: 'always'
      }
    ],
    // fix:文件末尾强制换行
    'eol-last': 2,
    // fix+prettier:不能用多余的空格
    'no-multi-spaces': 2,
    // [关闭]vue配置,模板缩进，统一用prettier管理
    'vue/html-indent':'off',
    // [关闭]vue配置,强制prop需要类型定义,基于strongly-recommended规则
    'vue/require-prop-types': 'off',
    // props中未标记为require的必须有default，基于上一条关闭
    'vue/require-default-prop': 'off',
    // 不允许从隐藏在外部作用域中声明的变量中声明同名的变量
    'vue/no-template-shadow': 'error',
    // [关闭]允许在template中写eslint-disable注释,基于base规则
    'vue/comment-directive': 'off',
    // fix:限制组件标签中每行的属性为1个
    'vue/max-attributes-per-line': [
      2,
      {
        singleline: 20,
        multiline: {
          max: 1,
          allowFirstLine: false
        }
      }
    ],
    // fix:此规则旨在将自动关闭符号强制作为配置样式。
    'vue/html-self-closing': [
      'error',
      {
        html: {
          void: 'always',
          normal: 'always',
          component: 'always'
        },
        svg: 'always',
        math: 'always'
      }
    ],
    // fix:除排除列表里的标签、无属性、空值的标签外，禁止内容和标签在单行
    'vue/singleline-html-element-content-newline': [
      0,
      {
        // 标签无属性时忽略校验
        ignoreWhenNoAttributes: true,
        // 标签为空时忽略校验
        ignoreWhenEmpty: true,
        ignores: ['p', 'textarea', 'span']
      }
    ]
  }
};
