/**
 * 本处设置默认采用官方推荐规则
 * 根据需求对默认规则进行了覆盖修改
 * 其他配置均采用prettier:recommended作为最优解
 * 官网详细说明 https://prettier.io/docs/en/options.html
 */
module.exports = {
  // 一行最多 100 字符
  printWidth: 100,
  // 箭头函数参数括号，默认avoid 可选 avoid| always,avoid 能省略括号的时候就省略 例如x => x，always 总是有括号
  arrowParens: 'avoid',
  // 行尾需要有分号
  semi: true,
  // 使用单引号
  singleQuote: true,
  // 不允许使用尾逗号
  trailingComma: 'none'
};
