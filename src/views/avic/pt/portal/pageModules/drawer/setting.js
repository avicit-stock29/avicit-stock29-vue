import { message } from 'ant-design-vue/es';

let lessNodesAppended;

const colorList = [
  // 6-9添加 start
  {
    key: '默认',
    name: 'blue-initial',
    theme: 'blue-initial-style',
    skin: 'blue-initial',
    color: {
      '@primary-color': '#0e94cd' // 全局主色
    },
    tagColor: 'background:linear-gradient(to right,#0b67a7,#29b8cc);'
  },
  {
    key: '蓝色',
    name: 'blue',
    theme: 'blue-style',
    skin: 'blue',
    color: {
      '@primary-color': '#3778eb' // 全局主色
    },
    tagColor: 'background:linear-gradient(to left,#56ccf2,#2f80ed);'
  },
  {
    key: '红色',
    name: 'red',
    theme: 'red-style',
    skin: 'red',
    color: {
      '@primary-color': '#e03d45' // 全局主色
    },
    tagColor: 'background:linear-gradient(to left,#f5505f,#da344c);'
  },
  {
    key: '紫色',
    name: 'purple',
    theme: 'purple-style',
    skin: 'purple',
    color: {
      '@primary-color': '#724eb7' // 全局主色
    },
    tagColor: 'background:linear-gradient(to left,#6155a3,#7264bf);'
  },
  {
    key: '绿色',
    name: 'green',
    theme: 'green-style',
    skin: 'green',
    color: {
      '@primary-color': '#3bbc81' // 全局主色
    },
    tagColor: 'background:linear-gradient(to left,#1c965e,#21b06e);'
  },
  {
    key: '灰白',
    name: 'ashen',
    theme: 'ashen-style',
    skin: 'ashen',
    color: {
      '@primary-color': '#66697a' // 全局主色
    },
    tagColor: 'background:#505363;'
  }
  // 6-9添加 end
];

window.nowTheme = '';

const updateTheme = (primaryColor, quiet = false) => {
  // Don't compile less in production!
  /* if (process.env.NODE_ENV === 'production') {
    return;
  } */
  // console.log('primaryColor',primaryColor)
  // Determine if the component is remounted
  if (!primaryColor) {
    return;
  }
  let themelink = primaryColor.theme
    ? '/assets/theme/' + primaryColor.theme + '.less'
    : '/assets/theme/normal-style.less';

  let hideMessage = function () {};

  if (!quiet) {
    hideMessage = message.loading('正在加载主题皮肤...', 0);
  }

  function buildIt() {
    if (!window.less) {
      return;
    }
    let colorSetting = {};
    if (typeof primaryColor === 'object') {
      colorSetting = primaryColor;
    } else {
      colorSetting = {
        color: {
          '@primary-color': primaryColor
        }
      };
    }
    setTimeout(() => {
      window.less
        .modifyVars(colorSetting.color)
        .then(() => {
          console.log('nowTheme link name', 'less:assets-theme-' + window.nowTheme);
          console.log(
            'nowTheme link',
            document.getElementById('less:assets-theme-' + window.nowTheme)
          );
          if (document.getElementById('less:assets-theme-' + window.nowTheme)) {
            document.getElementById('less:assets-theme-' + window.nowTheme).remove();
          }
          window.nowTheme = primaryColor.theme ? primaryColor.theme : 'normal-style';
          console.log('nowTheme', window.nowTheme);
          hideMessage();
        })
        .catch(() => {
          message.error('Failed to update theme');
          hideMessage();
        });
    }, 200);
  }

  if (!lessNodesAppended) {
    // insert less.js and color.less
    const lessStyleNode = document.createElement('link');
    const lessConfigNode = document.createElement('script');
    const lessScriptNode = document.createElement('script');
    lessStyleNode.setAttribute('rel', 'stylesheet/less');
    lessStyleNode.setAttribute('href', themelink);
    lessStyleNode.setAttribute('id', 'customStyle');
    lessConfigNode.innerHTML = `
      window.less = {
        async: true,
        env: 'production',
        javascriptEnabled: true
      };
    `;
    lessScriptNode.src = '/static/js/less.min.js';
    lessScriptNode.async = true;
    lessScriptNode.onload = () => {
      buildIt();
      lessScriptNode.onload = null;
    };
    document.body.appendChild(lessStyleNode);
    document.body.appendChild(lessConfigNode);
    document.body.appendChild(lessScriptNode);
    lessNodesAppended = true;
  } else {
    document.getElementById('customStyle').setAttribute('href', themelink);
    buildIt();
  }
};

const updateColorWeak = colorWeak => {
  // document.body.className = colorWeak ? 'colorWeak' : '';
  colorWeak
    ? document.body.classList.add('colorWeak')
    : document.body.classList.remove('colorWeak');
};

export { updateTheme, colorList, updateColorWeak };
