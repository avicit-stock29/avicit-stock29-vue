import UserLayout from '@portal/pageLayouts/UserLayout';
import BlankLayout from '@portal/pageLayouts/BlankLayout';
import BasicLayout from '@portal/pageLayouts/BasicLayout';
import RouteView from '@portal/pageLayouts/RouteView';
import PageView from '@portal/pageLayouts/PageView';
import TabLayout from '@portal/pageLayouts/TabLayout';

export { UserLayout, BasicLayout, BlankLayout, RouteView, PageView, TabLayout };
