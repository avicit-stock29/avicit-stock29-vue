import moment from 'moment';
/**
 *表单序列化后,提交表单前,表单的数据处理
 * @param obj 表单序列化结果值
 * @param formData 表单各字段控件类型等设置数据
 */
const formDataPretreatment = function (obj, formData) {
  // submitFormat: 提交后台接口时,时间处理格式(formatTime格式化时间 timeStamp时间戳,默认时间戳)
  if (obj && formData) {
    for (let key in obj) {
      for (let i = 0; i < formData.length; i++) {
        // 日期控件
        if (obj[key] && formData[i].dataIndex === key && formData[i].type === 'date') {
          if (formData[i].submitFormat && formData[i].submitFormat === 'formatTime') {
            obj[key] = moment(obj[key]).format(formData[i].format);
          } else {
            obj[key] = new Date(obj[key]).valueOf();
          }
        }
        // 日期时间控件
        if (obj[key] && formData[i].dataIndex === key && formData[i].type === 'datetime') {
          if (formData[i].submitFormat && formData[i].submitFormat === 'formatTime') {
            obj[key] = moment(obj[key]).format(formData[i].format);
          } else {
            obj[key] = new Date(obj[key]).valueOf();
          }
        }
        // 日期范围处理
        if (
          obj[key] &&
          (formData[i].dataIndex + 'Begin' === key || formData[i].dataIndex + 'End' === key) &&
          formData[i].type === 'daterange'
        ) {
          if (formData[i].submitFormat && formData[i].submitFormat === 'formatTime') {
            obj[key] = moment(obj[key]).format(formData[i].format);
          } else {
            obj[key] = new Date(obj[key]).valueOf();
          }
        }
        // 日期时间范围处理
        if (
          obj[key] &&
          (formData[i].dataIndex + 'Begin' === key || formData[i].dataIndex + 'End' === key) &&
          formData[i].type === 'datetimerange'
        ) {
          if (formData[i].submitFormat && formData[i].submitFormat === 'formatTime') {
            obj[key] = moment(obj[key]).format(formData[i].format);
          } else {
            obj[key] = new Date(obj[key]).valueOf();
          }
        }
        // checkbox控件
        if (obj[key] && formData[i].dataIndex === key && formData[i].type === 'checkbox') {
          obj[key] = obj[key].join(';');
        }
        // 通用选择框控件
        if (obj[key] && formData[i].dataIndex === key && formData[i].type === 'commonselect') {
          if (obj[key].ids || obj[key].ids == '') {
            obj[key] = obj[key].ids;
          }
        }
        //金额类型
        if (formData[i].dataIndex === key && formData[i].type === 'inputnumber') {
          if ('undefined' == typeof obj[key] || obj[key] === '' || obj[key] == null) {
            if (formData[i].min && formData[i].precision) {
              obj[key] = Number(formData[i].min.toFixed(formData[i].precision));
            } else {
              obj[key] = '';
            }
          } else {
            if (typeof obj[key] === 'number' && formData[i].precision) {
              obj[key] = obj[key].toFixed(formData[i].precision);
            }
          }
        }
      }
    }
  }
  return obj;
};

export { formDataPretreatment };
