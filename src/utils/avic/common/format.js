import moment from 'moment';
/**
 * 格式化时间
 * @param {时间值} value
 * @param {时间格式化形式} format
 */
const formatDate = function (value, format) {
  if (value) {
    return moment(value).format(format);
  }
};
/**
 * 数字保留小数位数超出禁止输入
 * @param {数值} value
 * @param {保留小数位数} precision
 */
const formatNumber = function (value, precision) {
  if (value.indexOf('.') < 0) {
    return value;
  } else {
    let num = precision ? precision + 1 : 3;
    let newValue = value.substr(0, value.indexOf('.') + num);
    return newValue;
  }
};
/**
 * 日期不可选范围
 * @param {当前时间} current
 */
const formatDisabledDate = function (current) {
  let year = moment().year() - 1;
  return current && current < moment().subtract(year, 'years');
};

export { formatDate, formatNumber, formatDisabledDate };
