import Vue from 'vue';
import axios from 'axios';
import store from '@/store';
import { VueAxios } from './axios';
import { Modal, notification } from 'ant-design-vue';
import {
  ACCESS_TOKEN,
  USER_INFO,
  MICRO_SERVICE_TOKEN,
  MICRO_SERVICE_TOKEN_TIME,
  MICRO_SERVICE_REFRESH
} from '@/store/mutationTypes';
// import Loding from '../components/plugins/Loading/index.js'
import defaultSettings from '@/config/defaultSettings';

let regHttp = new RegExp('^((https|http)?://)');
// 创建 axios 实例
const service = axios.create({
  baseURL: '', // api base_url
  timeout: defaultSettings.http.timeout // 请求超时时间
});
// let errorMessage = false; // 是否显示错误提示
let notificationDuration = 4; //消息的消失时间
let errorSystemMessage = true; //是否提示系统
const err = error => {
  if (error.response) {
    let data = error.response.data;
    const token = Vue.ls.get(ACCESS_TOKEN);
    switch (error.response.status) {
      // case 403:
      //   // if(defaultSettings.http.errorMessage!=false && errorMessage!=false){
      //   notification.error({ message: '系统提示', description: '拒绝访问', duration: notificationDuration });
      //   // }
      //   break;
      case 401:
        if (window._CONFIG['permissionChangeAlert'] === false && token) {
          window._CONFIG['permissionChangeAlert'] = true;
          Modal.error({
            title: '登录已过期',
            content: '很抱歉，登录已过期，请重新登录',
            okText: '重新登录',
            mask: false,
            onOk: () => {
              store.dispatch('Logout').then(() => {
                window._CONFIG['permissionChangeAlert'] = false;
                window.location.reload();
              });
            }
          });
        }

        break;
      // case 404:
      //   // if(defaultSettings.http.errorMessage!=false && errorMessage!=false){
      //   notification.error({ message: '系统提示', description: '很抱歉，资源未找到', duration: notificationDuration });
      //   // }
      //   break;
      // case 504:
      //   // if(defaultSettings.http.errorMessage!=false && errorMessage!=false){
      //   notification.error({ message: '系统提示', description: '网络超时', duration: notificationDuration });
      //   // }
      //   break;
      case 500:
        notification.error({
          message: '系统提示',
          description: data.errorDesc || '请求资源失败',
          duration: notificationDuration
        });
        break;
      default:
        // if(defaultSettings.http.errorMessage!=false && errorMessage!=false){
        if (!error.response.config.specialStatusCode.includes(error.response.status)) {
          notification.error({
            message: '系统提示',
            description: data.errorDesc || '请求失败',
            duration: notificationDuration
          });
        }
        // }
        break;
    }
  }
  return Promise.reject(error);
};

// request interceptor
service.interceptors.request.use(
  config => {
    const token = Vue.ls.get(ACCESS_TOKEN);
    const ms_token = Vue.ls.get(MICRO_SERVICE_TOKEN);
    if (token) {
      config.headers['X-Access-Token'] = token; // 让每个请求携带自定义 token 请根据实际情况自行修改
    }
    if (ms_token) {
      // 为微服务网关带上空权鉴校验
      // bearer经过王章确认是常量，写死就好
      config.headers['x-kong-api-key'] = 'bearer ' + ms_token;
    }
    notificationDuration = config.messageTime;
    errorSystemMessage = config.errorSystemMessage;
    if (!regHttp.test(config.url)) {
      config.url = window._CONFIG['domainURL'] + config.url;
    }
    // config.headers[ 'Content-Type' ] = 'application/json';
    if (config.method == 'get') {
      config.params = {
        _t: Date.parse(new Date()) / 1000,
        ...config.data
      };
    }
    if (config.contentType) {
      config.headers['Content-Type'] = config.contentType;
    } else {
      config.headers['Content-Type'] = defaultSettings.http.defaultContentType;
    }
    if (config.notUpdateSession) {
      config.headers['_notUpdateSession'] = true;
    }
    // errorMessage = config.errorMessage;
    if (defaultSettings.http.loading != false && config.loading != false) {
      // Loding.installMessage('加载中',(defaultSettings.http.loadingLock!=false&&config.loadingLock!=false)?true:false);
    }
    // return config;
    // resolve(config)

    return new Promise(resolve => {
      let xhr = window.XMLHttpRequest
          ? new window.XMLHttpRequest()
          : new ActiveXObject('Microsoft.XMLHTTP'),
        parameter = {
          client_id: window._CONFIG['kongConfig'].client_id,
          client_secret: window._CONFIG['kongConfig'].client_secret,
          grant_type: window._CONFIG['kongConfig'].grant_type,
          provision_key: window._CONFIG['kongConfig'].provision_key,
          authenticated_userid: window._CONFIG['kongConfig'].authenticated_userid
        },
        formData = '';

      let userinfo = Vue.ls.get(USER_INFO);

      if (userinfo && userinfo.id) {
        parameter.authenticated_userid = userinfo.id;
      } else {
        Vue.ls.remove(MICRO_SERVICE_TOKEN);
      }

      let ms_token_expires = Vue.ls.get(MICRO_SERVICE_TOKEN_TIME),
        ms_token = Vue.ls.get(MICRO_SERVICE_TOKEN),
        nowtime = new Date().valueOf() / 1000,
        need_ms_token = false;

      if (
        window._CONFIG['microServicePermissionURL'] &&
        window._CONFIG['microServicePermissionURL'] !== ''
      ) {
        if (!ms_token) {
          // 空权鉴token为空时的拉取
          need_ms_token = true;
        } else if (ms_token && ms_token_expires - nowtime <= 120) {
          // 空权鉴还差2分钟过期，调用refresh_token重新获取
          need_ms_token = true;
          parameter['grant_type'] = 'refresh_token';
          parameter['refresh_token'] = Vue.ls.get(MICRO_SERVICE_REFRESH);
        } else {
          // 空权鉴还能用
          need_ms_token = false;
        }
      } else {
        need_ms_token = false;
      }

      if (need_ms_token) {
        xhr.open('post', window._CONFIG['microServicePermissionURL'], true);
        xhr.responseType = 'json';
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function () {
          if (this.status === 200) {
            let res = this.response;
            if (res.access_token) {
              let nowsec = new Date().valueOf() / 1000;
              Vue.ls.set(MICRO_SERVICE_TOKEN, res.access_token, res.expires_in);
              Vue.ls.set(MICRO_SERVICE_TOKEN_TIME, nowsec + res.expires_in);
              Vue.ls.set(MICRO_SERVICE_REFRESH, res.refresh_token);
              config.headers['x-kong-api-key'] = 'bearer ' + res.access_token;
            }
            resolve(config);
          }
        };
        Object.keys(parameter).forEach(key => {
          formData += '&' + key + '=' + parameter[key];
        });
        formData = formData.slice(1);
        // formData = JSON.stringify(parameter)
        xhr.send(formData);
      } else {
        // 性能调试--记录请求响应时间 By CJ
        config.metadata = { startTime: new Date().valueOf() };
        resolve(config);
      }
    });
  },
  error => {
    // Loding.close();
    return Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(response => {
  // 性能调试--记录请求响应时间 By CJ
  if (response.config.metadata && response.config.metadata.startTime) {
    response.config.metadata.endTime = new Date().valueOf();
    let duration = response.config.metadata.endTime - response.config.metadata.startTime;
    let subUrl = response.config.url.substring(5);
    let moduleCode = subUrl.substring(0, subUrl.indexOf('/'));
    if (duration > 300) {
      store.commit('ADD_AXIOS_LOG', {
        moduleCode,
        url: response.config.url,
        duration
      });
    }
  }

  // Loding.close();

  //平台报文格式处理
  const newData = {};
  // const token = Vue.ls.get(ACCESS_TOKEN);
  newData.code = response.data.retCode;
  if (response.data.retCode == 200) {
    newData.success = true;
  } else if (response.data.retCode == 401) {
    if (window._CONFIG['permissionChangeAlert'] === false) {
      window._CONFIG['permissionChangeAlert'] = true;
      Modal.error({
        title: '登录已过期',
        content: '很抱歉，登录已过期，请重新登录',
        okText: '重新登录',
        mask: false,
        onOk: () => {
          store.dispatch('Logout').then(() => {
            window._CONFIG['permissionChangeAlert'] = false;
            window.location.reload();
          });
        }
      });
    }
    newData.success = true;
  } else {
    newData.success = false;
  }
  newData.message = response.data.message;
  newData.result = response.data.responseBody;
  newData.errorDesc = response.data.errorDesc;
  response.data = newData;

  if (response.data.success == false && errorSystemMessage == true) {
    let errmsg = response.data.message ? response.data.message : null;
    errmsg = errmsg ? errmsg : response.data.errorDesc || '操作失败！';
    if (newData.code == 902) {
      // 权限变更 permission.js中处理登出逻辑
    } else {
      notification.error({
        message: '系统提示',
        description: errmsg,
        duration: notificationDuration
      });
    }

    // console.log(response.data.message);
  }
  return response.data;
}, err);
const installer = {
  vm: {},
  install(Vue, router = {}) {
    Vue.use(VueAxios, router, service);
  }
};

export { installer as VueAxios, service as axios };
