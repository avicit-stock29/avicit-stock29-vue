function format(code, data, type) {
  // console.log("页面权限禁用--NODE--开始");
  let permissionList = data.permissionList;
  let counts = data.menuButtonCount;

  if (counts === 0) return;

  // console.log("permissionList=",permissionList)
  let permissions = [];
  for (let item of permissionList) {
    if (item.type === type) {
      permissions.push(item.action);
    }
  }
  //   console.log("页面权限----"+code);
  if (!permissions.includes(code)) {
    return true;
  }
  return false;
}

export function disabledAuthFilter(code, formData) {
  return format(code, formData, '2');
}

export function visiableFilter(code, formData) {
  return format(code, formData, '1');
}
