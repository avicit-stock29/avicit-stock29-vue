const hasPermission = {
  install(Vue) {
    Vue.directive('has', {
      inserted: (el, binding, vnode) => {
        let permissionList = vnode.context.$route.meta.permissionList;
        let counts = vnode.context.$route.meta.menuButtonCount;

        if (counts === 0) return;

        let permissions = [];
        if (permissionList && permissionList.length) {
          for (let item of permissionList) {
            if (item.type !== '2') {
              permissions.push(item.action);
            }
          }
        }

        // console.log("页面权限----"+permissions);
        // console.log("页面权限----"+binding.value);
        if (!permissions.includes(binding.value)) {
          // if(el.parentNode)
          el.parentNode.removeChild(el);
        }
      }
    });

    Vue.directive('editable', {
      inserted: (el, binding, vnode) => {
        let permissionList = vnode.context.$route.meta.permissionList;
        let counts = vnode.context.$route.meta.menuButtonCount;

        if (counts === 0) return;

        let permissions = [];
        for (let item of permissionList) {
          if (item.type === '2') {
            permissions.push(item.action);
          }
        }
        // console.log("页面权限----"+permissions);
        // console.log("页面权限----"+binding.value);
        if (!permissions.includes(binding.value)) {
          el.disabled = true;
        }
      }
    });
  }
};

export default hasPermission;
