/**
 * 控制表单列数布局
 * @param layout 列布局: 1,2,3,4
 */
const formColLayout = function (layout, colSetting, fixedSetting) {
  if (layout && layout === '1') {
    // 1列
    return {
      cols: colSetting && colSetting.cols ? colSetting.cols : { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol:
        colSetting && colSetting.labelCol ? colSetting.labelCol : { xs: 6, sm: 6, lg: 6, xl: 6 },
      wrapperCol:
        colSetting && colSetting.wrapperCol
          ? colSetting.wrapperCol
          : { xs: 18, sm: 18, lg: 18, xl: 18 },
      fixed: {
        cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
        labelCol:
          fixedSetting && fixedSetting.labelCol
            ? fixedSetting.labelCol
            : { xs: 6, sm: 6, lg: 6, xl: 6 },
        wrapperCol:
          fixedSetting && fixedSetting.wrapperCol
            ? fixedSetting.wrapperCol
            : { xs: 18, sm: 18, lg: 18, xl: 18 }
      }
    };
  } else if (layout && layout === '2') {
    // 2列
    return {
      cols:
        colSetting && colSetting.cols
          ? colSetting.cols
          : { xs: 24, sm: 24, md: 12, lg: 12, xl: 12 },
      labelCol:
        colSetting && colSetting.labelCol
          ? colSetting.labelCol
          : { xs: 8, sm: 8, md: 8, lg: 8, xl: 8 },
      wrapperCol:
        colSetting && colSetting.wrapperCol
          ? colSetting.wrapperCol
          : { xs: 16, sm: 16, md: 16, lg: 16, xl: 16 },
      fixed: {
        cols: { xs: 24, sm: 24, md: 24, lg: 24, xl: 24 },
        labelCol:
          fixedSetting && fixedSetting.labelCol
            ? fixedSetting.labelCol
            : { xs: 8, sm: 8, md: 4, lg: 4, xl: 4 },
        wrapperCol:
          fixedSetting && fixedSetting.wrapperCol
            ? fixedSetting.wrapperCol
            : { xs: 16, sm: 16, md: 20, lg: 20, xl: 20 }
      }
    };
  } else if (layout && layout === '3') {
    // 3列
    return {
      cols:
        colSetting && colSetting.cols ? colSetting.cols : { xs: 24, sm: 24, md: 12, lg: 8, xl: 8 },
      labelCol:
        colSetting && colSetting.labelCol
          ? colSetting.labelCol
          : { xs: 6, sm: 6, md: 8, lg: 9, xl: 9 },
      wrapperCol:
        colSetting && colSetting.wrapperCol
          ? colSetting.wrapperCol
          : { xs: 18, sm: 18, md: 16, lg: 15, xl: 15 },
      fixed: {
        cols: { xs: 24, sm: 24, md: 24, lg: 24, xl: 24 },
        labelCol:
          fixedSetting && fixedSetting.labelCol
            ? fixedSetting.labelCol
            : { xs: 6, sm: 6, md: 4, lg: 3, xl: 3 },
        wrapperCol:
          fixedSetting && fixedSetting.wrapperCol
            ? fixedSetting.wrapperCol
            : { xs: 18, sm: 18, md: 20, lg: 21, xl: 21 }
      }
    };
  } else if (layout && layout === '4') {
    // 4列
    return {
      cols:
        colSetting && colSetting.cols ? colSetting.cols : { xs: 24, sm: 12, md: 8, lg: 8, xl: 6 },
      labelCol:
        colSetting && colSetting.labelCol
          ? colSetting.labelCol
          : { xs: 8, sm: 8, md: 9, lg: 9, xl: 12 },
      wrapperCol:
        colSetting && colSetting.wrapperCol
          ? colSetting.wrapperCol
          : { xs: 16, sm: 16, md: 15, lg: 15, xl: 12 },
      fixed: {
        cols: { xs: 24, sm: 24, md: 24, lg: 24, xl: 24 },
        labelCol:
          fixedSetting && fixedSetting.labelCol
            ? fixedSetting.labelCol
            : { xs: 8, sm: 4, md: 3, lg: 3, xl: 3 },
        wrapperCol:
          fixedSetting && fixedSetting.wrapperCol
            ? fixedSetting.wrapperCol
            : { xs: 16, sm: 20, md: 21, lg: 21, xl: 21 }
      }
    };
  } else {
    // 默认1列
    return {
      cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol: { xs: 6, sm: 6, lg: 6, xl: 6 },
      wrapperCol: { xs: 18, sm: 18, lg: 18, xl: 18 }
    };
  }
};

/**
 * 控制表单列数布局---指定控件占两列
 * @param layout 列布局: 1,2,3,4
 */
const formColLayout2 = function (layout) {
  if (layout && layout === '1') {
    // 1列
    return {
      cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol: { xs: 6, sm: 6, lg: 6, xl: 6 },
      wrapperCol: { xs: 18, sm: 18, lg: 18, xl: 18 }
    };
  } else if (layout && layout === '2') {
    // 2列
    return {
      cols: { xs: 24, sm: 24, md: 24, lg: 24, xl: 24 },
      labelCol: { xs: 8, sm: 8, md: 4, lg: 4, xl: 4 },
      wrapperCol: { xs: 16, sm: 16, md: 20, lg: 20, xl: 20 }
    };
  } else if (layout && layout === '3') {
    // 3列
    return {
      cols: { xs: 24, sm: 24, md: 24, lg: 16, xl: 16 },
      labelCol: { xs: 6, sm: 6, md: 4, lg: 45, xl: 45 },
      wrapperCol: { xs: 18, sm: 18, md: 20, lg: 195, xl: 195 }
    };
  } else if (layout && layout === '4') {
    // 4列
    return {
      cols: { xs: 24, sm: 24, lg: 12, xl: 12 },
      labelCol: { xs: 8, sm: 4, lg: 4, xl: 6 },
      wrapperCol: { xs: 16, sm: 20, lg: 20, xl: 18 }
    };
  } else {
    // 默认1列
    return {
      cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol: { xs: 6, sm: 6, lg: 6, xl: 6 },
      wrapperCol: { xs: 18, sm: 18, lg: 18, xl: 18 }
    };
  }
};

/**
 * 控制表单列数布局---指定控件占三列
 * @param layout 列布局: 1,2,3,4
 */
const formColLayout3 = function (layout) {
  if (layout && layout === '1') {
    // 1列
    return {
      cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol: { xs: 6, sm: 6, lg: 6, xl: 6 },
      wrapperCol: { xs: 18, sm: 18, lg: 18, xl: 18 }
    };
  } else if (layout && layout === '2') {
    // 2列
    return {
      cols: { xs: 24, sm: 24, md: 24, lg: 24, xl: 24 },
      labelCol: { xs: 8, sm: 8, md: 4, lg: 4, xl: 4 },
      wrapperCol: { xs: 16, sm: 16, md: 20, lg: 20, xl: 20 }
    };
  } else if (layout && layout === '3') {
    // 3列
    return {
      cols: { xs: 24, sm: 24, md: 24, lg: 24, xl: 24 },
      labelCol: { xs: 6, sm: 6, md: 4, lg: 3, xl: 3 },
      wrapperCol: { xs: 18, sm: 18, md: 20, lg: 21, xl: 21 }
    };
  } else if (layout && layout === '4') {
    // 4列
    return {
      cols: { xs: 24, sm: 24, md: 24, lg: 24, xl: 18 },
      labelCol: { xs: 8, sm: 4, md: 3, lg: 3, xl: 4 },
      wrapperCol: { xs: 16, sm: 20, md: 21, lg: 21, xl: 20 }
    };
  } else {
    // 默认1列
    return {
      cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol: { xs: 6, sm: 6, lg: 6, xl: 6 },
      wrapperCol: { xs: 18, sm: 18, lg: 18, xl: 18 }
    };
  }
};

/**
 * 控制详情页表单列数布局
 * @param layout 列布局: 1,2,3,4
 */
const flowformColLayout = function (layout, colSetting, fixedSetting) {
  if (layout && layout === '1') {
    // 1列
    return {
      cols: colSetting && colSetting.cols ? colSetting.cols : { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol:
        colSetting && colSetting.labelCol
          ? colSetting.labelCol
          : { xs: 22, sm: 22, lg: 22, xl: 22 },
      wrapperCol:
        colSetting && colSetting.wrapperCol
          ? colSetting.wrapperCol
          : { xs: 22, sm: 22, lg: 22, xl: 22 },
      fixed: {
        cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
        labelCol:
          fixedSetting && fixedSetting.labelCol
            ? fixedSetting.labelCol
            : { xs: 24, sm: 24, lg: 24, xl: 24 },
        wrapperCol:
          fixedSetting && fixedSetting.wrapperCol
            ? fixedSetting.wrapperCol
            : { xs: 20, sm: 20, lg: 20, xl: 20 }
      }
    };
  } else if (layout && layout === '2') {
    // 2列
    return {
      cols: colSetting && colSetting.cols ? colSetting.cols : { xs: 12, sm: 12, lg: 12, xl: 12 },
      labelCol:
        colSetting && colSetting.labelCol ? colSetting.labelCol : { xs: 6, sm: 6, lg: 6, xl: 6 },
      wrapperCol:
        colSetting && colSetting.wrapperCol
          ? colSetting.wrapperCol
          : { xs: 18, sm: 18, lg: 18, xl: 18 },
      fixed: {
        cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
        labelCol:
          fixedSetting && fixedSetting.labelCol
            ? fixedSetting.labelCol
            : { xs: 3, sm: 3, lg: 3, xl: 3 },
        wrapperCol:
          fixedSetting && fixedSetting.wrapperCol
            ? fixedSetting.wrapperCol
            : { xs: 21, sm: 21, lg: 21, xl: 21 }
      }
    };
  } else if (layout && layout === '3') {
    // 3列
    return {
      cols: colSetting && colSetting.cols ? colSetting.cols : { xs: 8, sm: 8, lg: 8, xl: 8 },
      labelCol:
        colSetting && colSetting.labelCol ? colSetting.labelCol : { xs: 6, sm: 6, lg: 6, xl: 6 },
      wrapperCol:
        colSetting && colSetting.wrapperCol
          ? colSetting.wrapperCol
          : { xs: 18, sm: 18, lg: 18, xl: 18 },
      fixed: {
        cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
        labelCol:
          fixedSetting && fixedSetting.labelCol
            ? fixedSetting.labelCol
            : { xs: 2, sm: 2, lg: 2, xl: 2 },
        wrapperCol:
          fixedSetting && fixedSetting.wrapperCol
            ? fixedSetting.wrapperCol
            : { xs: 22, sm: 22, lg: 22, xl: 22 }
      }
    };
  } else if (layout && layout === '4') {
    // 4列
    return {
      cols: colSetting && colSetting.cols ? colSetting.cols : { xs: 6, sm: 6, lg: 6, xl: 6 },
      labelCol:
        colSetting && colSetting.labelCol
          ? colSetting.labelCol
          : { xs: 24, sm: 24, lg: 24, xl: 24 },
      wrapperCol:
        colSetting && colSetting.wrapperCol
          ? colSetting.wrapperCol
          : { xs: 24, sm: 24, lg: 24, xl: 24 },
      fixed: {
        cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
        labelCol:
          fixedSetting && fixedSetting.labelCol
            ? fixedSetting.labelCol
            : { xs: 24, sm: 24, lg: 24, xl: 24 },
        wrapperCol:
          fixedSetting && fixedSetting.wrapperCol
            ? fixedSetting.wrapperCol
            : { xs: 24, sm: 24, lg: 24, xl: 24 }
      }
    };
  } else {
    // 默认1列
    return {
      cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol: { xs: 4, sm: 4, lg: 4, xl: 4 },
      wrapperCol: { xs: 20, sm: 20, lg: 20, xl: 20 },
      fixed: {
        cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
        labelCol: { xs: 4, sm: 4, lg: 4, xl: 4 },
        wrapperCol: { xs: 20, sm: 20, lg: 20, xl: 20 }
      }
    };
  }
};

/**
 * 控制详情页表单列数布局--控件跨2列
 * @param layout 列布局: 1,2,3,4
 */
const flowformColLayout2 = function (layout) {
  if (layout && layout === '1') {
    // 1列
    return {
      cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol: { xs: 22, sm: 22, lg: 22, xl: 22 },
      wrapperCol: { xs: 22, sm: 22, lg: 22, xl: 22 }
    };
  } else if (layout && layout === '2') {
    // 2列
    return {
      cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol: { xs: 3, sm: 3, lg: 3, xl: 3 },
      wrapperCol: { xs: 21, sm: 21, lg: 21, xl: 21 }
    };
  } else if (layout && layout === '3') {
    // 3列
    return {
      cols: { xs: 16, sm: 16, lg: 16, xl: 16 },
      labelCol: { xs: 3, sm: 3, lg: 3, xl: 3 },
      wrapperCol: { xs: 21, sm: 21, lg: 21, xl: 21 }
    };
  } else if (layout && layout === '4') {
    // 4列
    return {
      cols: { xs: 12, sm: 12, lg: 12, xl: 12 },
      labelCol: { xs: 24, sm: 24, lg: 24, xl: 24 },
      wrapperCol: { xs: 24, sm: 24, lg: 24, xl: 24 }
    };
  } else {
    // 默认1列
    return {
      cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol: { xs: 22, sm: 22, lg: 22, xl: 22 },
      wrapperCol: { xs: 22, sm: 22, lg: 22, xl: 22 }
    };
  }
};

/**
 * 控制详情页表单列数布局--控件跨3列
 * @param layout 列布局: 1,2,3,4
 */
const flowformColLayout3 = function (layout) {
  if (layout && layout === '1') {
    // 1列
    return {
      cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol: { xs: 22, sm: 22, lg: 22, xl: 22 },
      wrapperCol: { xs: 22, sm: 22, lg: 22, xl: 22 }
    };
  } else if (layout && layout === '2') {
    // 2列
    return {
      cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol: { xs: 3, sm: 3, lg: 3, xl: 3 },
      wrapperCol: { xs: 21, sm: 21, lg: 21, xl: 21 }
    };
  } else if (layout && layout === '3') {
    // 3列
    return {
      cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol: { xs: 2, sm: 2, lg: 2, xl: 2 },
      wrapperCol: { xs: 22, sm: 22, lg: 22, xl: 22 }
    };
  } else if (layout && layout === '4') {
    // 4列
    return {
      cols: { xs: 18, sm: 18, lg: 18, xl: 18 },
      labelCol: { xs: 24, sm: 24, lg: 24, xl: 24 },
      wrapperCol: { xs: 24, sm: 24, lg: 24, xl: 24 }
    };
  } else {
    // 默认1列
    return {
      cols: { xs: 24, sm: 24, lg: 24, xl: 24 },
      labelCol: { xs: 22, sm: 22, lg: 22, xl: 22 },
      wrapperCol: { xs: 22, sm: 22, lg: 22, xl: 22 }
    };
  }
};
/**
 * 控制弹窗modal的宽高
 * @param width 弹窗modal宽度(
 *        number&&小于等于1视为屏幕百分比；
 *        number&&大于1视为固定px值；
 *        string&&以[%]结尾视为屏幕百分比；
 *        其他返回原值)
 * @param height 弹窗modal高度占屏幕比例
 * @param cutHeight 弹窗modal的body的高度比整个弹窗modal少的高度(px)
 * @param bodyOverflowY 弹窗modal的body的overflow-y属性
 * @param backgroundColor modalBody背景
 */
const modalWidthAndHeight = function (
  width = 0.7,
  height = 0.7,
  cutHeight = 55,
  bodyOverflowY = 'auto',
  backgroundColor = '#fff'
) {
  let docWidth = document.documentElement.clientWidth;
  let docHeight = document.documentElement.clientHeight;

  let modalStyleWidth = '';
  if (typeof width == 'number' && width <= 1) {
    modalStyleWidth = docWidth * width + 'px';
  } else if (typeof width == 'number' && width > 1) {
    modalStyleWidth = width + 'px';
  } else if (typeof width == 'string' && width.endsWith('%')) {
    modalStyleWidth = (docWidth * parseFloat(width)) / 100 + 'px';
  } else {
    modalStyleWidth = width;
  }
  let bodyStyleWidth = modalStyleWidth;
  let modalStyleHeight = '';
  let bodyStyleHeight = '';
  if (typeof height == 'number' && height <= 1) {
    modalStyleHeight = docHeight * height + 'px';
    bodyStyleHeight = docHeight * height - cutHeight + 'px';
  } else if (typeof height == 'number' && height > 1) {
    modalStyleHeight = height + 'px';
    bodyStyleHeight = height - cutHeight + 'px';
  } else if (typeof height == 'string' && height.endsWith('%')) {
    modalStyleHeight = (docHeight * parseFloat(height)) / 100 + 'px';
    bodyStyleHeight = (docHeight * parseFloat(height)) / 100 - cutHeight + 'px';
  } else {
    modalStyleHeight = height;
    bodyStyleHeight = 'auto';
  }
  //指定尺寸大于屏幕尺寸则默认90%
  if (
    (typeof width == 'number' && width > docWidth) ||
    (typeof height == 'number' && height > docHeight) ||
    (typeof width == 'string' && width.endsWith('px') && parseInt(width) > docWidth) ||
    (typeof height == 'string' && height.endsWith('px') && parseInt(height) > docHeight)
  ) {
    return {
      modalStyle: {
        // 弹窗宽高控制
        width: docWidth + 'px',
        height: docHeight + 'px'
      },
      bodyStyle: {
        // 弹窗body宽高控制
        width: docWidth + 'px',
        height: docHeight - cutHeight + 'px',
        overflowY: bodyOverflowY,
        backgroundColor
      },
      standardModalStyle: {
        width: parseInt(modalStyleWidth),
        height: parseInt(modalStyleHeight)
      }
    };
  }
  return {
    modalStyle: {
      // 弹窗宽高控制
      width: modalStyleWidth,
      height: modalStyleHeight
    },
    bodyStyle: {
      // 弹窗body宽高控制
      width: bodyStyleWidth,
      height: bodyStyleHeight,
      overflowY: bodyOverflowY,
      backgroundColor
    },
    standardModalStyle: {
      width: parseInt(modalStyleWidth),
      height: parseInt(modalStyleHeight)
    }
  };
};
/**
 * 根据布局列数控制弹窗modal的宽高
 * （单列： 580*420  适用于1~6项内容时、
 *   两列：960*500   适用于6~12项内容时、
 *   三列：1280*600） 适用于12项以上
 * @param layout 列数
 * @param cutHeight 弹窗modal的body的高度比整个弹窗modal少的高度
 * @param bodyOverflowY 弹窗modal的body的overflow-y属性
 */
const modalWidthAndHeightBylayout = function (
  layout = 1,
  cutHeight = 55,
  bodyOverflowY = 'auto',
  backgroundColor = '#fff'
) {
  let modalStyleWidth = null;
  let modalStyleHeight = null;
  let bodyStyleWidth = null;
  let bodyStyleHeight = null;
  let bodyStylePadding = null;
  let standardModalWidth = null;
  let standardModalHeight = null;
  let docWidth = document.documentElement.clientWidth;
  let docHeight = document.documentElement.clientHeight;
  if (layout == 1) {
    if (docWidth < 580 || docHeight < 420) {
      modalStyleWidth = docWidth + 'px';
      modalStyleHeight = docHeight + 'px';
      bodyStyleWidth = docWidth + 'px';
      bodyStyleHeight = docHeight - cutHeight + 'px';
    } else {
      modalStyleWidth = 580 + 'px';
      modalStyleHeight = 420 + 'px';
      bodyStyleWidth = 580 + 'px';
      bodyStyleHeight = 420 - cutHeight + 'px';
    }
    standardModalWidth = 580 + 'px';
    standardModalHeight = 420 + 'px';
    bodyStylePadding = '20px 60px 20px 20px';
  } else if (layout == 2) {
    if (docWidth < 960 || docHeight < 500) {
      modalStyleWidth = docWidth + 'px';
      modalStyleHeight = docHeight + 'px';
      bodyStyleWidth = docWidth + 'px';
      bodyStyleHeight = docHeight - cutHeight + 'px';
    } else {
      modalStyleWidth = 960 + 'px';
      modalStyleHeight = 500 + 'px';
      bodyStyleWidth = 960 + 'px';
      bodyStyleHeight = 500 - cutHeight + 'px';
    }
    standardModalWidth = 960 + 'px';
    standardModalHeight = 500 + 'px';
    bodyStylePadding = '20px 60px 20px 20px';
  } else if (layout == 3) {
    if (docWidth < 1280 || docHeight < 600) {
      modalStyleWidth = docWidth + 'px';
      modalStyleHeight = docHeight + 'px';
      bodyStyleWidth = docWidth + 'px';
      bodyStyleHeight = docHeight - cutHeight + 'px';
    } else {
      modalStyleWidth = 1280 + 'px';
      modalStyleHeight = 600 + 'px';
      bodyStyleWidth = 1280 + 'px';
      bodyStyleHeight = 600 - cutHeight + 'px';
    }
    standardModalWidth = 1280 + 'px';
    standardModalHeight = 600 + 'px';
    bodyStylePadding = '20px 80px 20px 20px';
  } else if (layout == 4) {
    if (docWidth < 1280 || docHeight < 600) {
      modalStyleWidth = docWidth + 'px';
      modalStyleHeight = docHeight + 'px';
      bodyStyleWidth = docWidth + 'px';
      bodyStyleHeight = docHeight - cutHeight + 'px';
    } else {
      modalStyleWidth = 1280 + 'px';
      modalStyleHeight = 600 + 'px';
      bodyStyleWidth = 1280 + 'px';
      bodyStyleHeight = 600 - cutHeight + 'px';
    }
    standardModalWidth = 1280 + 'px';
    standardModalHeight = 600 + 'px';
    bodyStylePadding = '20px 80px 20px 20px';
  }
  return {
    modalStyle: {
      // 弹窗宽高控制
      width: modalStyleWidth,
      height: modalStyleHeight
    },
    bodyStyle: {
      // 弹窗body宽高控制
      width: bodyStyleWidth,
      height: bodyStyleHeight,
      padding: bodyStylePadding,
      overflowY: bodyOverflowY,
      backgroundColor
    },
    standardModalStyle: {
      width: parseInt(standardModalWidth),
      height: parseInt(standardModalHeight)
    }
  };
};
/**
 * 获取内容高度: 传入元素的父元素高度 - 父元素除去内容之外的元素的高度
 * @param wrapId 元素的id
 * @param cutHeight 内容与父元素相差的高度,一般包括:父元素padding,按钮高度及margin,及其他
 */
const getContentHeight = function (params) {
  // 默认值auto
  let contentHeight = 'auto';
  if (params) {
    // 获取元素父元素
    let el = document.querySelector('#' + params.wrapId);
    if (el) {
      // 获取父元素高度
      let parentHeight = el.parentNode.offsetHeight;
      // 传入的元素高度设为父元素高度
      // el.style.height = parentHeight + 'px'; 为加resize改变大小,注释掉这句.
      // 内容高度为 父元素高度 减去 父元素除去内容之外的元素的高度
      if (params.cutHeight) {
        contentHeight = parentHeight - params.cutHeight + 'px';
      } else {
        contentHeight = parentHeight + 'px';
      }
    }
  }
  return contentHeight;
};

/**
 * 校验不通过,滚动到校验不通过元素位置,提示用户
 */
const getFirstCheckErrorElement = function (err) {
  for (let key in err) {
    // return document.querySelector("div[for='" + key + "']") || document.querySelector("div[for='" + key + "Alias']") || document.querySelector("div[for='" + key + "Name']");
    return document.querySelector('#' + key);
  }
};

export {
  formColLayout,
  formColLayout2,
  formColLayout3,
  flowformColLayout,
  flowformColLayout2,
  flowformColLayout3,
  modalWidthAndHeight,
  modalWidthAndHeightBylayout,
  getContentHeight,
  getFirstCheckErrorElement
};
