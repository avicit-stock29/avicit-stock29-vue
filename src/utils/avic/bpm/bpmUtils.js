import { httpAction } from '@api/manage';
import Store from '@/store/index';
import { message } from 'ant-design-vue/es';
let doc = null;
let beterScrollTop = 0;
/**
 * 流程通用工具类
 */
const bpmUtils = {
  /**
   * 根路径
   */
  baseurl: '/api/bpm/bpm',
  /**
   * 流程审批页面打开方式，可通过系统参数“PLATFORM_V6_BPMSHOWMODE”进行配置
   * default : window.open 的方式
   * dialog : top.layer.open 的方式
   * tab : tap.addTab 的方式
   * 默认不配置或配置不合法，按“default”处理
   */
  bpm_show_mode: 'default',
  /**
   * 判断非空
   *
   * @returns {Boolean}
   */
  notNull() {
    for (let i = 0; i < arguments.length; i++) {
      let obj = arguments[i];
      if (
        typeof obj === 'undefined' ||
        obj == null ||
        (typeof obj === 'string' && obj == 'undefined')
      ) {
        return false;
      }
      if (typeof obj === 'string' && obj.trim() == '') {
        return false;
      }
      if (obj == 'null') {
        return false;
      }
    }
    return true;
  },
  /**
   * 判断路由
   * @param dataJson
   */
  getConditions(dataJson) {
    return {
      // 分支
      isBranch() {
        if (dataJson.nextTask != null && dataJson.nextTask.length > 1) {
          // 分支
          return true;
        } else {
          // 非分支
          return false;
        }
      },
      // 选人方式
      isUserSelectTypeAuto(branchNo) {
        if (typeof branchNo === 'undefined') {
          branchNo = 0;
        }
        if (
          dataJson.nextTask != null &&
          dataJson.nextTask[branchNo].currentActivityAttr.userSelectType
        ) {
          if (dataJson.nextTask[branchNo].currentActivityAttr.userSelectType == 'auto') {
            return true; // 自动选人方式
          } else {
            return false;
          }
        } else {
          return false; // 手动选人方式
        }
      },
      // 是否启用工作移交
      isWorkHandUser(branchNo) {
        if (typeof branchNo === 'undefined') {
          branchNo = 0;
        }
        if (
          dataJson.nextTask != null &&
          dataJson.nextTask[branchNo].currentActivityAttr.isWorkHandUser
        ) {
          if (dataJson.nextTask[branchNo].currentActivityAttr.isWorkHandUser == 'no') {
            return false; // 启用
          } else {
            return true; // 不启用
          }
        } else {
          // 默认
          return true; // 不启用
        }
      },
      // 处理方式
      getDealType(branchNo) {
        if (typeof branchNo === 'undefined') {
          branchNo = 0;
        }
        if (dataJson.nextTask != null && dataJson.nextTask[branchNo].currentActivityAttr.dealType) {
          return dataJson.nextTask[branchNo].currentActivityAttr.dealType;
        }
        return '2';
      },
      // 处理方式
      getSingle(branchNo) {
        if (typeof branchNo === 'undefined') {
          branchNo = 0;
        }
        if (dataJson.nextTask != null && dataJson.nextTask[branchNo].currentActivityAttr.single) {
          return dataJson.nextTask[branchNo].currentActivityAttr.single;
        }
        return 'no';
      },
      // 是否必须选人
      isMustUser(branchNo) {
        if (typeof branchNo === 'undefined') {
          branchNo = 0;
        }
        if (
          dataJson.nextTask != null &&
          dataJson.nextTask[branchNo].currentActivityAttr.isMustUser
        ) {
          if (dataJson.nextTask[branchNo].currentActivityAttr.isMustUser == 'no') {
            // 必须选人
            return false;
          } else {
            return true; // 必须选人
          }
        } else {
          return true; // 默认值
        }
      },
      // 是否启用密级
      isSecret(branchNo) {
        if (typeof branchNo === 'undefined') {
          branchNo = 0;
        }
        if (dataJson.nextTask != null && dataJson.nextTask[branchNo].currentActivityAttr.isSecret) {
          if (dataJson.nextTask[branchNo].currentActivityAttr.isMustUser == 'yes') {
            // 启用
            return true;
          } else {
            return false; // 不启用
          }
        } else {
          return false; // 不启用
        }
      },
      // 意见添写方式
      getIdeaType() {
        if (dataJson.currentActivityAttr.ideaType) {
          return dataJson.currentActivityAttr.ideaType;
        }
        return '';
      },
      // 是否强制表态
      isIdeaCompelManner() {
        if (dataJson.currentActivityAttr.ideaCompelManner) {
          if (dataJson.currentActivityAttr.ideaCompelManner == 'yes') {
            // 强制
            return true;
          } else {
            return false; // 不强制
          }
        } else {
          return false; // 不强制
        }
      },
      // 退回意见是否必填
      isNeedIdea() {
        if (dataJson.currentActivityAttr.isNeedIdea) {
          if (dataJson.currentActivityAttr.isNeedIdea == 'no') {
            // 强制
            return false;
          } else {
            return true; // 不强制
          }
        } else {
          return true; // 不强制
        }
      },
      // 是否显示选人框
      isSelectUser(branchNo) {
        if (typeof branchNo === 'undefined') {
          branchNo = 0;
        }
        if (
          dataJson.nextTask != null &&
          dataJson.nextTask[branchNo].currentActivityAttr.isSelectUser
        ) {
          if (dataJson.nextTask[branchNo].currentActivityAttr.isSelectUser == 'no') {
            // 不显示
            return false;
          } else {
            return true; // 显示
          }
        } else {
          return true; // 显示
        }
      },
      /**
       * 是否自动获取用户
       */
      isAutoGetUser(branchNo) {
        if (typeof branchNo === 'undefined') {
          branchNo = 0;
        }
        if (
          dataJson.nextTask != null &&
          dataJson.nextTask[branchNo].currentActivityAttr.isAutoGetUser
        ) {
          if (dataJson.nextTask[branchNo].currentActivityAttr.isAutoGetUser == 'no') {
            // 不显示
            return false;
          } else {
            return true; // 是
          }
        } else {
          return false;
        }
      },
      // 获取下节点类型
      getNextActivityType() {
        return dataJson.activityType;
      },
      //是否是自由流程
      isUflow() {
        return dataJson.isUflow == '2';
      }
    };
  },
  //打开流程详情页面
  openFlowDetail: (url, flowDetailUrl, callback, windowName = 'NewFlowDetailWin') => {
    if (url) {
      let skipUrl = '';
      if (flowDetailUrl && url.indexOf('?') != -1) {
        url = url.split('?')[1];
        skipUrl = encodeURIComponent(flowDetailUrl.replace(RegExp('/', 'g'), '_')) + '?' + url;
      } else {
        if (url.indexOf('?') != -1) {
          let urlStr = url.split('?');
          skipUrl = encodeURIComponent(urlStr[0].replace(RegExp('/', 'g'), '_')) + '?' + urlStr[1];
        } else {
          skipUrl = encodeURIComponent(url.replace(RegExp('/', 'g'), '_'));
        }
      }

      window.open('/flowPublicDetail/' + skipUrl, windowName);
      if (callback) {
        callback();
      }
    } else {
      if (callback) {
        callback();
      }
    }
  },
  //普通流程添加打开流程页面
  addListStart(defineId, title) {
    let p = new Promise(function (resolve, reject) {
      httpAction(
        bpmUtils.baseurl + '/business/start/v1',
        {
          defineId
        },
        'post'
      )
        .then(res => {
          if (res.success) {
            resolve(res);
            bpmUtils.openFlowDetail(
              res.result.formUrl +
                '?defineId=' +
                res.result.defineId +
                '&deploymentId=' +
                res.result.deploymentId +
                '&title=' +
                title,
              '',
              null,
              'FlowAdd-' + res.result.defineId + '-' + res.result.deploymentId
            );
          } else {
            reject(res);
          }
        })
        .catch(() => {
          reject({ success: false });
        });
    });
    return p;
  },
  /**
   * 获取流程详情页面相关数据
   * id 流程id
   * afterDetail 前置事件
   */
  detail: (id, flowDetailUrl, extType) => {
    let p = new Promise(function (resolve, reject) {
      if (!bpmUtils.notNull(extType)) {
        extType = '2';
      }
      httpAction(
        bpmUtils.baseurl + '/business/getProcessDetailParameter/v1',
        {
          id,
          manager: extType
        },
        'post'
      )
        .then(res => {
          if (res.success) {
            if (res.result.length == 0) {
              message.error('没有查询到可用的任务链接，可能是数据错误，请联系管理员解决');
            } else if (res.result.length == 1) {
              const n = res.result[0];
              bpmUtils.executeTask(
                n.processInstance,
                n.executionId,
                n.dbid,
                n.businessId,
                n.formResourceName,
                n.processDefName || n.taskTitle, // 展示流程标题
                n.taskType,
                flowDetailUrl
              );
            } else {
              Store.dispatch('openFlowDetailSelect', {
                data: res.result,
                flowDetailUrl
              });
            }
            resolve(res);
          } else {
            reject(res);
          }
        })
        .catch(() => {
          reject({ success: false });
        });
    });
    return p;
  },
  executeTask: (entryId, executionId, taskId, formId, url, title, taskType, flowDetailUrl) => {
    if (url == null || url == '') {
      return;
    }
    if (taskType == '1') {
      httpAction(
        bpmUtils.baseurl + '/business/finishreader/v1',
        {
          dbid: taskId,
          entryId,
          doTask: 'false' //是否是快速办理
        },
        'post'
      ).then(() => {});
    }
    let proxyPage = 'N'; //是否做页面代理
    if (url.indexOf('proxyPage=Y') != -1) {
      //是否做页面代理
      proxyPage = 'Y';
    }
    if (proxyPage != 'Y') {
      //不明确指定用代理页面的，则通通跳转到自己页面
      if (url.indexOf('?') > 0) {
        url += '&entryId=' + entryId;
      } else {
        url += '?entryId=' + entryId;
      }
      url += '&id=' + formId;
      url += '&executionId=' + executionId;
      if (url.indexOf('taskId=') == -1) {
        url += '&taskId=' + taskId;
      }
      url += '&title=' + title;
      bpmUtils.openFlowDetail(url, flowDetailUrl, null, 'FlowDetail-' + entryId);
    } else {
      // var redirectPath = "avicit/platform6/bpmreform/bpmbusiness/approve/ProcessApprove.jsp";
      // redirectPath += "?id=" + formId;
      // redirectPath += "&entryId=" + entryId;
      // redirectPath += "&executionId=" + executionId;
      // redirectPath += "&taskId=" + taskId;
      // redirectPath += "&title=" + encodeURI(title);
      if (url.indexOf('?') > 0) {
        url += '&entryId=' + entryId;
      } else {
        url += '?entryId=' + entryId;
      }
      url += '&id=' + formId;
      url += '&executionId=' + executionId;
      if (url.indexOf('taskId=') == -1) {
        url += '&taskId=' + taskId;
      }
      url += '&title=' + title;
      //redirectPath += "&url=" + encodeURI(url);
      //redirectPath += "&url=" + encodeURIComponent(url);
      bpmUtils.openFlowDetail(url, flowDetailUrl, null, 'FlowDetail-' + entryId);
    }
  },
  //获取流程 DefineId
  getDefineIdByEntryId: async entryId => {
    let result = await httpAction(
      bpmUtils.baseurl + '/business/getDefineIdByEntryId/v1',
      {
        entryId
      },
      'post'
    );
    return new Promise(resolve => {
      if (result.success) {
        Store.commit('setDefineId', result.result);
        resolve(result);
      } else {
        resolve(result);
      }
    });
  },
  // 获取流程按钮设置
  getBpmButtonListBySelf: async (entryId, executionId, taskId, formId, userId) => {
    let result = await httpAction(
      bpmUtils.baseurl + '/business/getBpmButtonListBySelf/v1',
      {
        bpm_entryId: entryId,
        bpm_executionId: executionId,
        bpm_taskId: taskId,
        bpm_formId: formId,
        userId
      },
      'post'
    );
    return new Promise(resolve => {
      if (result.success) {
        resolve(result);
      } else {
        resolve(result);
      }
    });
  },
  //获取流程按钮
  getoperateright: async (entryId, executionId, taskId, formId) => {
    let result = await httpAction(
      bpmUtils.baseurl + '/business/getoperateright/v1',
      {
        processInstanceId: entryId,
        executionId,
        taskId,
        bpm_formId: formId
      },
      'post'
    );
    return new Promise(resolve => {
      if (result.success) {
        Store.commit('setDefineId', result.result.defineId);
        resolve(result);
      } else {
        resolve(result);
      }
    });
  },

  //请求合并后接口，获取defineId、流程按钮、流程节点、表单权限
  getOperaterightAndFormSecuritys: async (entryId, executionId, taskId, formId) => {
    let result = await httpAction(
      bpmUtils.baseurl + '/business/getOperaterightAndFormSecuritys/v1',
      {
        processInstanceId: entryId,
        executionId,
        taskId,
        bpm_formId: formId
      },
      'post'
    );
    return new Promise(resolve => {
      if (result.success) {
        Store.commit('setDefineId', result.result.defineId);
        resolve(result);
      } else {
        resolve(result);
      }
    });
  },
  //发起流程获取流程参数
  flowStart: async defineId => {
    let result = await httpAction(
      bpmUtils.baseurl + '/business/start/v1',
      {
        defineId
      },
      'post'
    );
    return new Promise(resolve => {
      if (result.success) {
        resolve(result);
      } else {
        resolve(result);
      }
    });
  },
  //获取流程页面标签页
  getBpmTabs: async () => {
    let result = await httpAction(bpmUtils.baseurl + '/business/getBpmTabs/v1', '', 'post');
    return new Promise(resolve => {
      if (result.success) {
        resolve(result);
      } else {
        resolve(result);
      }
    });
  },
  //获取流程标签类型
  getBpmNavbarType: async () => {
    let result = await httpAction(bpmUtils.baseurl + '/business/getBpmNavbarType/v1', '', 'post');
    return new Promise(resolve => {
      if (result.success) {
        resolve(result);
      } else {
        resolve(result);
      }
    });
  },
  //设置标题
  setTitle: title => {
    if (title) {
      document.title = title;
    }
  },
  //表单是否可编辑
  isEnable() {
    if (Store.state.bpmSave && Store.state.bpmSave.enable == false) {
      return true;
    } else {
      return false;
    }
  },
  //form表单的是否可操作
  formOperability: code => {
    if (bpmUtils.isEnable()) {
      return true;
    }
    if (Store.state.bpmModel.userIdentityKey == '7' && Store.state.bpmSave.enable) {
      // 管理员允许编辑表单
      return false;
    }
    // 非已办人、已阅人、读者、未知身份，判断表单字段权限
    if (
      code &&
      Store.state.bpmEditor.formSecuritys.result &&
      Store.state.bpmEditor.formSecuritys.result.length > 0 &&
      Store.state.bpmModel.userIdentityKey !== '2' &&
      Store.state.bpmModel.userIdentityKey !== '4' &&
      Store.state.bpmModel.userIdentityKey !== '5' &&
      Store.state.bpmModel.userIdentityKey !== '0'
    ) {
      let result = true;
      Store.state.bpmEditor.formSecuritys.result.map(tim => {
        if (tim.tag == code && tim.operability == '1') {
          result = false;
        }
      });
      return result;
    } else {
      return true;
    }
  },
  //form表单的是否必填
  /**
   * 增加代码生成器中，对数据库必填字段在拟稿节点的处理
   */
  formRequired: (code, options) => {
    let tips = typeof options === 'object' ? options['tip'] || '请输入' : options;
    let dbRequired = typeof options === 'object' && options['dbRequired'] ? true : false;
    let result = [];
    if (bpmUtils.isEnable()) {
      result = [];
    } else {
      if (code) {
        if (
          Store.state.bpmEditor.formSecuritys.result &&
          Store.state.bpmEditor.formSecuritys.result.length > 0
        ) {
          Store.state.bpmEditor.formSecuritys.result.map(tim => {
            if (
              tim.tag == code &&
              tim.required == '1' &&
              tim.accessibility == '1' &&
              tim.operability == '1'
            ) {
              result = [{ required: true, message: `${tips}${tim.tagName}！` }];
            }
          });
        }
      }
    }
    if (dbRequired && !result.length) {
      result = [{ required: true, message: `此项不可为空！` }];
    }
    return result;
  },
  //form表单的是否显示
  formAccessibility: code => {
    if (code) {
      if (
        Store.state.bpmEditor.formSecuritys.result &&
        Store.state.bpmEditor.formSecuritys.result.length > 0
      ) {
        let result = false;
        Store.state.bpmEditor.formSecuritys.result.map(tim => {
          if (tim.tag == code && tim.accessibility == '1') {
            result = true;
          }
        });
        return result;
      } else {
        return true;
      }
    } else {
      return true;
    }
  },
  //附件是否可编辑
  isDisabledAccessory: index => {
    if (bpmUtils.isEnable()) {
      return false;
    }
    if (
      Store.state.bpmEditor.formSecuritys.attachmentAuths &&
      Store.state.bpmEditor.formSecuritys.attachmentAuths.length >= index &&
      Store.state.bpmModel.userIdentityKey !== '2' &&
      Store.state.bpmModel.userIdentityKey !== '4' &&
      Store.state.bpmModel.userIdentityKey !== '5' &&
      Store.state.bpmModel.userIdentityKey !== '0'
    ) {
      let result = false;
      if (
        Store.state.bpmEditor.formSecuritys.attachmentAuths[index] &&
        Store.state.bpmEditor.formSecuritys.attachmentAuths[index].operability == '1'
      ) {
        result = true;
      }
      return result;
    } else {
      return false;
    }
  },
  //附件是否可修改秘籍
  isLevelAccessory: index => {
    if (bpmUtils.isEnable()) {
      return false;
    }
    if (
      Store.state.bpmEditor.formSecuritys.attachmentAuths &&
      Store.state.bpmEditor.formSecuritys.attachmentAuths.length >= index
    ) {
      let result = false;
      let attachment = Store.state.bpmEditor.formSecuritys.attachmentAuths[index];
      if (attachment && attachment.modifySecretLevel == '1' && attachment.operability == '1') {
        result = true;
      }
      return result;
    } else {
      return false;
    }
  },

  //子表状态控制
  subTablePermissions(pageSettingData, subTableName, buttonFors) {
    let userIdentityKey = Store.state.bpmModel.userIdentityKey;
    let permissions = false;
    //按钮控制
    if (buttonFors && buttonFors.length > 0) {
      if (
        Store.state.bpmEditor.formSecuritys.result &&
        Store.state.bpmEditor.formSecuritys.result.length > 0
      ) {
        Store.state.bpmEditor.formSecuritys.result.map(tim => {
          if (buttonFors.indexOf(tim.tag) != -1 && tim.accessibility == '1') {
            permissions = true;
          }
        });
      } else {
        permissions = true;
      }
    } else {
      permissions = true;
    }
    if (userIdentityKey !== '1' && userIdentityKey !== '6') {
      permissions = false;
    }
    //已办 已阅 读者 未知 状态的隐藏子表按钮 禁用子表字段
    if (
      userIdentityKey == '2' ||
      userIdentityKey == '4' ||
      userIdentityKey == '5' ||
      userIdentityKey == '0'
    ) {
      //子表不可编辑
      pageSettingData.map(tim => {
        tim.isCanEdit = false;
      });
      return { pageSettingData, permissions };
    } else {
      if (bpmUtils.isEnable()) {
        //子表不可编辑
        pageSettingData.map(tim => {
          tim.isCanEdit = false;
        });
        return { pageSettingData, permissions };
      }
      if (
        Store.state.bpmEditor.formSecuritys.result &&
        Store.state.bpmEditor.formSecuritys.result.length > 0
      ) {
        Store.state.bpmEditor.formSecuritys.result.map(tim => {
          pageSettingData.map((table, index) => {
            if (subTableName + '__' + table.dataIndex == tim.tag) {
              //表格是否显示
              if (tim.accessibility != '1') {
                pageSettingData.splice(index, 1);
              } else {
                //表单是否可编辑
                if (tim.operability == '1') {
                  table.isCanEdit = true;
                } else {
                  table.isCanEdit = false;
                }
                //是否必填
                if (tim.required == '1' && tim.accessibility == '1' && tim.operability == '1') {
                  table.headerClassName = 'required-table-title';
                }
              }
            }
          });
        });
        return { pageSettingData, permissions };
      } else {
        return { pageSettingData, permissions };
      }
    }
  },
  //附件是否必填
  isQequiredAccessory: index => {
    if (bpmUtils.isEnable()) {
      return [];
    }
    if (
      Store.state.bpmEditor.formSecuritys.attachmentAuths &&
      Store.state.bpmEditor.formSecuritys.attachmentAuths.length >= index
    ) {
      let result = [];
      let attachment = Store.state.bpmEditor.formSecuritys.attachmentAuths[index];
      if (attachment && attachment.required == '1' && attachment.operability == '1') {
        result = [{ required: true, message: '请上传附件！' }];
      }
      return result;
    } else {
      return [];
    }
  },
  /**
   * 创建空的xmlDocument
   * @param {*} arguments
   */
  createXmlDocument: () => {
    if (document.implementation && document.implementation.createDocument) {
      doc = document.implementation.createDocument('', '', null);
    } else if (window.ActiveXObject) {
      doc = new ActiveXObject('Microsoft.XMLDOM');
    }
    return doc;
  },
  /**
   * 构建xml元素
   * @param {*} value
   */
  createElement: value => {
    if (doc == null) {
      doc = bpmUtils.createXmlDocument();
    }
    return doc.createElement(value);
  },

  createTextNode: value => {
    if (doc == null) {
      doc = bpmUtils.createXmlDocument();
    }
    if (value) {
      value = value.replace(/(^\s+)|(\s+$)/g, '');
    }
    return doc.createTextNode(value);
  },
  /**
   * 关闭当前窗口
   */
  closeWindow() {
    if (window.opener == null) {
      document.title = '页面已过期';
      location.replace('about:blank');
    } else {
      window.close();
    }
    this.refreshBack();
  },
  /**
   * 刷新父页面，bpm_operator_refresh
   */
  refreshBack() {
    try {
      if (window.opener && !window.opener.closed) {
        if (bpmUtils.notNull(window.opener.bpmOperatorRefresh)) {
          window.opener.bpmOperatorRefresh();
        }
      }
    } catch (e) {
      // console.log(e);
    }
  },
  /**
   * 校验不通过,滚动到校验不通过元素位置,提示用户
   */
  getFirstCheckErrorElement(err) {
    for (let key in err) {
      // return document.querySelector("div[for='" + key + "']") || document.querySelector("div[for='" + key + "Alias']") || document.querySelector("div[for='" + key + "Name']");
      return document.querySelector('#' + key);
    }
  },
  /**
   * 流程详情页面滚动事件
   */
  handleScroll() {
    let scrollTop =
      window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
    let headTop,
      tabsTop = '0';
    if (scrollTop != 0 && scrollTop - beterScrollTop > 0) {
      headTop = '-40px';
      tabsTop = '0px';
    } else {
      headTop = '0px';
      tabsTop = '40px';
    }
    beterScrollTop = scrollTop;
    return { headTop, tabsTop };
  }
};

export { bpmUtils };
