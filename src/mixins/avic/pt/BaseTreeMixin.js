import '@assets/styles/common/base-surface-panel.less';
import '@/assets/styles/common/base-surface-toolbar.less';
import '@/assets/styles/common/base-form.less';
import {
  Modal,
  Col,
  Button,
  Form,
  Input,
  DatePicker,
  Radio,
  Checkbox,
  Select,
  InputNumber
} from 'ant-design-vue';
import AvicResizeLayout from '@comp/avic/pt/common/AvicResizeLayout';
import AvicResizeLayoutPanel from '@comp/avic/pt/common/AvicResizeLayout/AvicResizeLayoutPanel';
import AvicTree from '@/components/avic/pt/common/AvicTree';
import AvicInitTreeRootNode from '@/components/avic/pt/common/AvicTree/AvicInitTreeRootNode';
import AvicCommonSelect from '@/components/avic/pt/org/AvicCommonSelect';
import AvicUploader from '@comp/avic/pt/common/AvicUploader';
import VueScroll from 'vuescroll/dist/vuescroll-native';
import { httpAction } from '@/api/manage';
import moment from 'moment';
import { formDataPretreatment } from '@utils/avic/common/formDataPretreatment';
export const baseTreeMixin = {
  components: {
    AModal: Modal,
    ACol: Col,
    AButton: Button,
    AForm: Form,
    AFormItem: Form.Item,
    AInput: Input,
    ADatePicker: DatePicker,
    ARadio: Radio,
    ARadioGroup: Radio.Group,
    ACheckbox: Checkbox,
    ACheckboxGroup: Checkbox.Group,
    ASelect: Select,
    ASelectOption: Select.Option,
    AInputNumber: InputNumber,
    Atextarea: Input.TextArea,
    AvicResizeLayout,
    AvicResizeLayoutPanel,
    AvicTree,
    AvicInitTreeRootNode,
    AvicCommonSelect,
    AvicUploader,
    VueScroll
  },
  data() {
    return {
      addParentId: '',
      menuVisible: false, // 右键树菜单
      surfaceTitle: '详情', // form表单内容说明
      parentTitle: ''
    };
  },
  methods: {
    //点击右键菜单操作
    onMenuSelect(obj) {
      let e = obj.key;
      if (obj.selectTreeNode) {
        this.selectTree = obj.selectTreeNode;
        this.onSelected = this.selectTree.id;
        this.selectedKeys = [this.selectTree.id]; //选中的节点编号
        this.searchAndForm(this.selectTree.id);
        if (e == '1') {
          // 添加平级节点
          this.addParentId = this.selectTree.parentId;
          this.parentTitle = '';
          this.handleAdd(); //添加平级节点
        } else if (e == '2') {
          // 添加子节点
          this.addParentId = this.selectTree.id;
          this.parentTitle = this.selectTree.title;
          this.handleAddSub(); //添加子节点
        } else if (e == '3') {
          // 删除节点
          if (
            this.selectTree.isLeaf &&
            (this.selectTree.children == null || this.selectTree.children.length == 0)
          ) {
            this.handleDelete();
          } else {
            this.$message.warning('当前选中节点含有子节点，请先删除子节点！');
          }
        }
      }
    },
    //添加平级节点
    handleAdd() {
      if (this.onSelected.length == '0') {
        this.$message.warning('请选择要添加的节点！');
        return;
      }
      if (this.parentId == '-1') {
        this.$message.warning('根节点不能添加平级节点！');
        return;
      }
      this.isChildNode = false;
      this.showAddModal = true;
    },
    //添加子节点
    handleAddSub() {
      if (this.onSelected.length == '0') {
        this.$message.warning('请选择要添加的节点！');
        return;
      }
      this.isChildNode = true;
      this.showAddModal = true;
    },
    //清空表单数据
    clearData() {
      this.formId = '';
      let clearArr = [];
      Object.assign(clearArr, this.formArray);
      let index = clearArr.indexOf('parentId');
      if (index > -1) {
        clearArr.splice(index, 1);
      }
      this.form.resetFields(clearArr);
    },
    //保存
    saveForm() {
      let This = this;
      //获取当前表单的id
      let id = this.form.getFieldValue('id');
      if (id) {
        //编辑
        this.form.validateFieldsAndScroll((err, values) => {
          if (!err) {
            // 提交前数据处理
            let newValues = formDataPretreatment(values, this.tagTypeObj);
            // 提交
            httpAction(This.addSaveFormParam.url, newValues, This.addSaveFormParam.method).then(
              res => {
                if (res.success) {
                  if (This.attachmentRefName) {
                    //表单有附件
                    This.formType = 'add';
                    This.$refs[This.attachmentRefName].upload(This.formId); // 附件上传
                    This.isLoadingRootNode = false;
                    This.reloadTreeData(res.result.parentId);
                    This.searchAndForm(res.result.id);
                  } else {
                    this.$message.success('编辑成功！');
                    This.isLoadingRootNode = false;
                    This.reloadTreeData(res.result.parentId);
                    This.searchAndForm(res.result.id);
                  }
                }
              }
            );
          }
        });
      }
    },
    reloadData(nodeInfo) {
      this.formId = nodeInfo.id;
      this.parentId = nodeInfo.parentId;
      this.isLoadingRootNode = false;
      this.reloadTreeData(nodeInfo.parentId);
      //选中新增的节点
      this.selectedKeys = [nodeInfo.id];
      //修改当前选中的id
      this.onSelected = nodeInfo.id;
      this.surfaceTitle = nodeInfo.title;
      this.searchAndForm(nodeInfo.id);
    },
    // 删除树节点
    handleDelete() {
      if (this.onSelected.length == '0') {
        this.$message.warning('请选择要删除的节点！');
        return;
      }
      let This = this;
      this.$confirm({
        title: '您确定要删除选中的节点吗？',
        content: '',
        okText: '确定',
        cancelText: '取消',
        onOk() {
          if (This.onSelected) {
            httpAction(
              This.handleDeleteParam.url,
              This.onSelected,
              This.handleDeleteParam.method
            ).then(res => {
              if (res.success) {
                This.$message.success('删除成功！');
                This.isRootNode = false;
                This.$refs[This.treeRefName].handleTreeNodeDelete(This.onSelected);
                let nowParentId = This.parentId;
                This.searchAndForm(nowParentId);
                This.selectedKeys = [nowParentId];
                This.onSelected = nowParentId;
              }
            });
          }
        },
        onCancel() {}
      });
    },
    // 树列表加载数据之后
    afterLoadData(treeData) {
      // 判断是否显示初始化根节点
      if (this.isRootNode) {
        if (
          treeData.success &&
          (!treeData.result || treeData.result.length == 0) &&
          this.$refs[this.treeRefName].treeNodeData.length == 0
        ) {
          this.initTreeDataVisible = true;
        }
      }
      //设置根节点默认选中
      if (this.isLoadingRootNode) {
        if (treeData.result && treeData.result.length > 0) {
          let rootId = treeData.result[0].id;
          this.selectedKeys = [rootId];
          this.searchAndForm(rootId);
          this.onSelected = rootId;
          this.surfaceTitle = treeData.result[0].title;
        }
      }
    },
    // 加载初始化节点完成,点击完成的时候
    confirmInitTreeData() {
      this.initTreeDataVisible = false;
      this.$refs[this.treeRefName].refresh();
    },
    // 关闭初始化加载节点框
    closeInitTreeData() {
      this.initTreeDataVisible = false;
    },
    // 重新加载刷新树
    refresh() {
      this.$refs[this.treeRefName].refresh();
    },
    // 选中的树
    onSelect(e) {
      if (e && e.length > 0) {
        this.onSelected = e[0];
        this.selectedKeys = e;
        this.searchAndForm(e[0]);
      }
    },
    //根据节点id查询该节点的数据，并赋值给form表单
    searchAndForm(id) {
      this.loadLookUp(this.lookupParams);
      let This = this;
      //根据id去后台查询当前节点的数据
      httpAction(This.searchTreeByIdParam.url + id, '', This.searchTreeByIdParam.method)
        .then(res => {
          if (res.success) {
            This.parentId = res.result.parentId;
            // 从返回结果中抽取本页面表单字段
            let data = new Object();
            for (let i = 0; i < This.formArray.length; i++) {
              data[This.formArray[i]] = res.result[This.formArray[i]];
            }
            // 从本页面表单字段抽取值不为null的字段,对时间、多选框等前后台数据格式不一致的字段进行数据处理
            let newData = new Object();
            for (let key in data) {
              if (data[key] !== null && data[key] !== '') {
                newData[key] = data[key];
              }
            }
            // 对时间、多选框等前后台数据格式不一致的字段进行数据处理
            for (let key in newData) {
              for (let i = 0; i < This.tagTypeObj.length; i++) {
                if (
                  (This.tagTypeObj[i].dataIndex === key && This.tagTypeObj[i].type === 'date') ||
                  (This.tagTypeObj[i].dataIndex === key && This.tagTypeObj[i].type === 'datetime')
                ) {
                  newData[key] = moment(data[key]);
                } else if (
                  This.tagTypeObj[i].dataIndex === key &&
                  This.tagTypeObj[i].type === 'checkbox'
                ) {
                  newData[key] = data[key].split(';');
                } else if (
                  This.tagTypeObj[i].dataIndex === key &&
                  This.tagTypeObj[i].type === 'commonselect'
                ) {
                  newData[key] = { ids: data[key], names: res.result[key + 'Alias'] };
                }
              }
            }

            //清空表单数据
            This.clearData();
            if (this.formId) {
              this.formId = '';
            }
            //为部门树右侧的form表单赋值
            This.form.setFieldsValue(newData);
            //为右侧表单附件赋值formId
            This.formId = res.result.id;
            //为顶部title赋值
            This.setTitle();
          }
        })
        .catch(response => {
          This.treeLoading = false;
        });
    },
    //树搜索方法
    handleSuperQuery(e) {
      if (!e || !e.trim().length) {
        this.treeParam.url = this.getTreeUrl + this.defaultLevel + '/' + this.defaultParentId;
      } else {
        this.searchTreeParam.parameter = { searchText: e.trim() };
      }
    }
  }
};
