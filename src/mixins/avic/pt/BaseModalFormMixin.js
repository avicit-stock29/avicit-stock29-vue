/**
 * 模态框表单基础混入对象
 * ---定义模态框组件及组件属性
 * ---定义表单组件、表单控件、表单布局、表单校验方法
 * ---定义打开、关闭、保存等默认交互
 */
import '@assets/styles/common/base-modal.less';
import {
  Modal,
  Button,
  Form,
  Col,
  Input,
  Radio,
  Checkbox,
  Select,
  InputNumber,
  DatePicker
} from 'ant-design-vue';
import AvicModal from '@comp/avic/pt/common/AvicModal';
import AvicUploader from '@comp/avic/pt/common/AvicUploader';
import AvicBpmUploader from '@comp/avic/bpm/AvicBpmUploader';
import AvicCommonSelect from '@comp/avic/pt/org/AvicCommonSelect';
import AvicTreeSelect from '@comp/avic/pt/common/AvicTree/AvicTreeSelect';
import AvicTextareaCount from '@comp/avic/pt/common/AvicTextarea/Count';
import { validateCheckboxMaxlength, validateInputMaxlength } from '@utils/avic/common/validate';
import { formatDate, formatNumber, formatDisabledDate } from '@utils/avic/common/format';
import {
  formColLayout,
  formColLayout2,
  formColLayout3,
  modalWidthAndHeightBylayout,
  getFirstCheckErrorElement
} from '@utils/avic/layout/baseMouldStyles';
import { loadLookUp } from '@utils/avic/http/loadLookUp';
import { httpAction } from '@api/manage';
import { mapGetters } from 'vuex';
import moment from 'moment';
export const baseModalFormMixin = {
  components: {
    AButton: Button,
    AForm: Form,
    ACol: Col,
    AFormItem: Form.Item,
    AInput: Input,
    ARadioGroup: Radio.Group,
    ARadio: Radio,
    ACheckboxGroup: Checkbox.Group,
    ACheckbox: Checkbox,
    ASelect: Select,
    ASelectOption: Select.Option,
    AInputNumber: InputNumber,
    ADatePicker: DatePicker,
    ATextarea: Input.TextArea,
    AvicTextareaCount,
    AvicUploader,
    AvicBpmUploader,
    AvicCommonSelect,
    AvicTreeSelect,
    AvicModal
  },
  props: {
    layout: {
      // 一行几列布局参数
      type: String,
      default: '1'
    },
    requestPublicUrl: {
      // 请求地址路径的公共部分
      type: String,
      required: true
    },
    modalFormLookupParams: {
      // 通用代码
      type: Array,
      default: () => []
    }
  },
  data() {
    return {
      Modal, // 函数式模态框对象句柄
      visible: false, // 弹窗modal显隐
      formColLayout: formColLayout(this.layout), // 调用布局公共方法
      formColLayout2: formColLayout2(this.layout), // 调用布局公共方法--指定控件跨两列
      formColLayout3: formColLayout3(this.layout), // 调用布局公共方法--指定控件跨三列
      modalWidthAndHeight: modalWidthAndHeightBylayout(this.layout, 96), // 弹窗modal宽高控制
      modalLoading: false, // 弹窗保存按钮loading状态
      disabled: false, //返回按钮可用
      form: this.$form.createForm(this, {
        onValuesChange: (props, values) => {
          this.isDataChange = true;
        }
      }), // 表单
      formId: '', // 附件上传formId
      formSubmitTimes: 0, // 记录的表单提交次数
      isDataChange: false //标记表单数据是否变化
    };
  },
  methods: {
    ...mapGetters(['userInfo']),
    /**
     * 表单数据格式化、表单数据校验方法
     */
    validateCheckboxMaxlength,
    validateInputMaxlength,
    formatDate,
    formatNumber,
    formatDisabledDate,
    moment,
    getFirstCheckErrorElement,
    /**
     * 获取表单通用代码
     * @param {Array<Object>} params 通用代码设置的params,由manage页面通过props传递过来
     */
    loadLookUp(params) {
      if (params) {
        loadLookUp(params, data => {
          for (let i = 0; i < data.length; i++) {
            for (let j = 0; j < params.length; j++) {
              if (data[i].fileName === params[j].fileName) {
                if (data[i].fileName !== 'secretLevel') {
                  this[params[j].fileName + 'List'] = data[i].value; // 拿到通用代码,赋给相应的通用代码数组
                } else {
                  httpAction(
                    '/api/appsys/lookup/LookupRest/getUserSecretWordList/v1/' + this.userInfo().id,
                    '',
                    'get'
                  ) // 提交
                    .then(res => {
                      if (res.success) {
                        this.secretLevelList = res.result;
                      } else {
                        this.$message.error('读取文档密级选项失败！');
                      }
                    })
                    .catch(e => {
                      console.error(e);
                    });
                }
              }
            }
          }
        });
      }
    }
  }
};
