export const internationalMixin = {
  computed: {
    // 页面公共国际化变量
    modalTitleAdd() {
      // 添加弹窗标题: 添加
      return this.$t('basemould.add');
    },
    modalTitleEdit() {
      // 编辑弹窗标题: 编辑
      return this.$t('basemould.edit');
    },
    modalTitleDetail() {
      // 详情弹窗标题: 详情
      return this.$t('basemould.detail');
    },
    from() {
      // 分页: 第
      return this.$t('basemould.from');
    },
    to() {
      // 分页: 到第
      return this.$t('basemould.to');
    },
    total() {
      // 分页: 共
      return this.$t('basemould.total');
    },
    items() {
      // 分页: 条
      return this.$t('basemould.items');
    },
    saveWarning() {
      // 保存提示: 请先修改数据！
      return this.$t('basemould.saveWarning');
    },
    saveSuccess() {
      // 保存成功
      return this.$t('basemould.saveSuccess');
    },
    delWarning() {
      // 删除提示: 请确保上一条数据修改完毕！
      return this.$t('basemould.delWarning');
    },
    delEnquire() {
      // 删除提示: 确认要删除选中的数据吗？
      return this.$t('basemould.delEnquire');
    },
    delSuccess() {
      // 删除成功
      return this.$t('basemould.delSuccess');
    },
    requiredWarning() {
      // 必填提示: 为必填字段！
      return this.$t('basemould.requiredWarning');
    },
    scriptWarning() {
      // 非法脚本提示: 非法含有JS脚本完整标签！
      return this.$t('basemould.scriptWarning');
    },
    maxLengthWarning() {
      // 最大长度提示: 不能超过
      return this.$t('basemould.maxLengthWarning');
    },
    characters() {
      // 个字符
      return this.$t('basemould.characters');
    }
  }
};
