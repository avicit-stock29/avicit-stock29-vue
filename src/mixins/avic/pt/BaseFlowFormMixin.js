/**
 * 流程详情页表单基础混入对象
 * ---定义流程相关基本属性
 * ---定义流程组件、流程表单校验方法
 * ---定义父子窗口刷新交互公共方法
 */

import {
  Modal,
  Col,
  Button,
  Form,
  Input,
  DatePicker,
  Checkbox,
  Radio,
  Select,
  InputNumber
} from 'ant-design-vue';
import AvicCommonSelect from '@comp/avic/pt/org/AvicCommonSelect';
import AvicBpmFormSpan from '@comp/avic/bpm/AvicBpmFormSpan';
import AvicTextareaCount from '@comp/avic/pt/common/AvicTextarea/Count';
import { loadLookUp } from '@utils/avic/http/loadLookUp';
import moment from 'moment';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
import { validateCheckboxMaxlength, validateInputMaxlength } from '@utils/avic/common/validate';
import {
  flowformColLayout,
  flowformColLayout2,
  flowformColLayout3,
  getFirstCheckErrorElement
} from '@utils/avic/layout/baseMouldStyles';
import { formDataPretreatment } from '@utils/avic/common/formDataPretreatment';

import { formatDate, formatNumber, formatDisabledDate } from '@utils/avic/common/format';
import { httpAction } from '@api/manage';
import { mapActions, mapGetters } from 'vuex';
export const baseFlowFormMixin = {
  components: {
    AButton: Button,
    AForm: Form,
    ACol: Col,
    AFormItem: Form.Item,
    AInput: Input,
    ARadioGroup: Radio.Group,
    ARadio: Radio,
    ACheckboxGroup: Checkbox.Group,
    ACheckbox: Checkbox,
    ASelect: Select,
    ASelectOption: Select.Option,
    AInputNumber: InputNumber,
    ADatePicker: DatePicker,
    ATextarea: Input.TextArea,
    AvicTextareaCount,
    AvicBpmFormSpan,
    AvicCommonSelect,
    AvicBpmUploader: () => import('@comp/avic/bpm/AvicBpmUploader')
  },
  data() {
    return {
      Modal,
      noDataTip: '无', // 展示详情状态无数据的默认显示信息
      openType: 'add', // 表单是添加还是编辑状态 add or edit
      formDetailData: {}, //非编辑状态下详情信息保存
      formColLayout: flowformColLayout(this.layout), // 调用布局公共方法
      formColLayout2: flowformColLayout2(this.layout), // 调用布局公共方法
      formColLayout3: flowformColLayout3(this.layout), // 调用布局公共方法
      form: this.$form.createForm(this), // 表单
      formId: '', // 附件上传formId
      formData: {}, //formData 数据
      bpmTabs: [], //显示区域数据
      bpmButtonParams: {}, //提交按钮传递的参数
      formShowed: false // 表单信息是否以展示
    };
  },
  //注册监听tab数据变化事件
  computed: {
    ...mapGetters(['getTabs'])
  },
  //监听Tab参数变化
  watch: {
    getTabs(tabs) {
      if (this.bpmTabs.length == 0 && tabs.length > 0 && this.openType == 'edit') {
        //加载流程数据
        // this.modalHandleDetailOrEdit(this.formData.id);
        // this.formShowed = true;
      }
      this.bpmTabs = tabs;
    }
  },
  /**
   * 页面创建时候设置初始化数据,区分是是否启动流程页面操作
   */
  created() {
    this.formColLayout = flowformColLayout(this.layout);
    this.formColLayout2 = flowformColLayout2(this.layout); // 调用布局公共方法
    this.formColLayout3 = flowformColLayout3(this.layout); // 调用布局公共方法
    if (this.$route.query) {
      this.openType = this.$route.query.id ? 'edit' : 'add';
      this.setFlowData();
    }
  },
  mounted() {
    this.bpmTabs = this.$store.state.bpmTabs.tabs;
  },
  methods: {
    ...mapGetters(['userInfo']),
    ...mapActions(['closeLoading', 'setIsButtonLoading']),
    /**
     * 表单数据格式化、表单数据校验方法
     */
    validateCheckboxMaxlength,
    validateInputMaxlength,
    formatDate,
    formatNumber,
    formatDisabledDate,
    moment,
    getFirstCheckErrorElement,
    formDataPretreatment,
    //设置流程数据
    setFlowData() {
      if (this.openType == 'add') {
        this.saveAndStartParam.parameter = {
          processDefId: this.$route.query.defineId,
          formCode: this.formCode,
          bean: {}
        };
        this.$nextTick(() => {
          this.closeLoading();
        });
      } else if (this.openType == 'edit') {
        this.formData = this.$route.query;
        //加载流程数据
        this.modalHandleDetailOrEdit(this.formData.id);
      }
    },
    /**
     * 获取表单通用代码
     * @param {Array<Object>} params 通用代码设置的params,由manage页面通过props传递过来
     */
    loadLookUp(params) {
      if (params) {
        loadLookUp(params, data => {
          for (let i = 0; i < data.length; i++) {
            for (let j = 0; j < params.length; j++) {
              if (data[i].fileName === params[j].fileName) {
                if (data[i].fileName !== 'secretLevel') {
                  this[params[j].fileName + 'List'] = data[i].value; // 拿到通用代码,赋给相应的通用代码数组
                } else {
                  httpAction(
                    '/api/appsys/lookup/LookupRest/getUserSecretWordList/v1/' + this.userInfo().id,
                    '',
                    'get'
                  ) // 提交
                    .then(res => {
                      if (res.success) {
                        this.secretLevelList = res.result;
                      } else {
                        this.$message.error('读取文档密级选项失败！');
                      }
                    })
                    .catch(e => {
                      console.error(e);
                    });
                }
              }
            }
          }
        });
      }
    },
    //加载form
    reloadForm() {
      if (this.attachmentRefName) {
        // 表单含有附件
        if (this.$refs[this.attachmentRefName]) {
          if (this.$refs[this.attachmentRefName].length > 0) {
            this.$refs[this.attachmentRefName][0].upload(this.formId); // 附件上传
          } else {
            this.$refs[this.attachmentRefName].upload(this.formId); // 附件上传
          }
        } else {
          this.reloadList();
        }
      } else {
        this.reloadList();
      }
    },
    /**
     * 附件afterUpload执行函数
     */
    uploadCompleteFile(successFile, errorFile) {
      this.reloadList();
    },
    //点击按钮的前置事件
    beforeBpmButtons(data) {},
    //按钮点击后置事件
    afterBpmButtons(data) {},
    //设置是否可编辑
    isDisabledFlow(code) {
      return bpmUtils.formOperability(code);
    },
    //设置是否必填
    findRulesFlow(code, options) {
      return bpmUtils.formRequired(code, options);
    },
    //字段是否显隐
    formAccessibility(code) {
      let res = bpmUtils.formAccessibility(code);
      return res;
    },
    //设置详情展示时是否是必填
    findDetailRulesFlow(code) {
      let res = bpmUtils.formDetailRequired(code);
      return res;
    },
    //附件是否可编辑
    isDisabledAccessory(index) {
      let res = bpmUtils.isDisabledAccessory(index);
      return res;
    },
    //设置子表权限是否可编辑
    subTablePermission(buttonFors) {
      let result = bpmUtils.subTablePermissions(
        this.pageSettingData,
        this.subTableName,
        buttonFors
      );
      this.pageSettingData = result.pageSettingData;
      return result.permissions;
    },
    //附件是否必填
    isQequiredAccessory(index) {
      let res = bpmUtils.isQequiredAccessory(index);
      return res;
    }
  }
};
