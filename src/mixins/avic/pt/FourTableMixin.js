// import { Modal } from 'ant-design-vue';
// import { httpAction } from '@/api/manage';
export const fourTableMixin = {
  data() {
    return {
      selectedRowKeys: [],
      sidx: '', // 排序字段
      sord: '', // 排序方式: desc降序 asc升序
      dataRecord: [] // 记录每次刷新表格的数据
    };
  },
  methods: {
    reloadDataMain(current, refName, type) {
      let that = this;
      if (current) {
        this.page = 1;
      }
      if (type && (type == 'delete' || type == 'add')) {
        this.$refs[refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            if (that.data.length > 0) {
              that.selectedRowKeys = [that.data[0][that.rowKey]];
              that.setChangeInfoId(that.selectedRowKeys);
            }
            // that.selectedRowKeys = [that.data[0][that.rowKey]];
            // that.$refs.mpmProjectSubInfoManage.reloadDataSub(true, "SubTable", that.selectedRowKeys[0]);
            // that.setFormId(that.selectedRowKeys[0]);
          }
        );
      } else {
        this.$refs[refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            // this.selectedRowKeys = [];
          }
        );
      }
    },
    reloadDataSub(current, refName, type) {
      let that = this;
      if (current) {
        this.page = 1;
      }
      if (type && (type == 'delete' || type == 'add')) {
        this.$refs[refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            if (that.data.length > 0) {
              that.selectedRowKeys = [that.data[0][that.rowKey]];
              that.setMaterInfoId(that.selectedRowKeys);
              that.setRecordInfo({ data: that.data[0], type: 'materInfo' });
            } else {
              that.setRecordInfo({ data: null, type: 'materInfo' });
            }
            // that.selectedRowKeys = [that.data[0][that.rowKey]];
            // that.$refs.mpmProjectSubInfoManage.reloadData(true,"SubTable", that.selectedRowKeys[0]);
            // that.setFormId(that.selectedRowKeys[0]);
          }
        );
      } else {
        this.$refs[refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            // this.selectedRowKeys = [];
          }
        );
      }
    },
    reloadDataMaterDetail(current, refName, type) {
      let that = this;
      if (current) {
        this.page = 1;
      }
      if (type && (type == 'delete' || type == 'add')) {
        this.$refs[refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            if (that.data.length > 0) {
              that.selectedRowKeys = [that.data[0][that.rowKey]];
              that.setMaterDetailId(that.selectedRowKeys);
              that.setRecordInfo({ data: that.data[0], type: 'materDetail' });
            }
            // that.selectedRowKeys = [that.data[0][that.rowKey]];
            // that.$refs.mpmProjectSubInfoManage.reloadData(true,"SubTable", that.selectedRowKeys[0]);
            // that.setFormId(that.selectedRowKeys[0]);
          }
        );
      } else {
        this.$refs[refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            // this.selectedRowKeys = [];
          }
        );
      }
    }
  }
};
