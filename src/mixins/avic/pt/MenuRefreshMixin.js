/**
 * @description MainLayout下，单tab标签页刷新功能使用此部分内容
 * @remark 使用方法
 *    1、混入MenuRefreshMixin
 *    2、标签页根组件methods中实现{{ menuRefresh }}同名方法，根据各模块的功能需求自定义
 */
import { mapGetters } from 'vuex';
// import html2canvas from 'html2canvas'
export const menuRefreshMixin = {
  data() {
    return {
      isInstanceActive: true,
      previewDataLength: 0
    };
  },
  computed: {
    ...mapGetters(['getOpenedViewList', 'getRefreshMenuName', 'getMenuRefresh'])
  },
  created() {
    this.$store.commit('ADD_MENU', this);
  },
  mounted() {
    // 暂时关闭该功能 By Cj
    // this.updatePreview()
  },
  activated() {
    this.isInstanceActive = true;

    // 暂时关闭该功能 By Cj
    // if (this.previewDataLength === 0) {
    //   this.updatePreview()
    // }
  },
  deactivated() {
    this.isInstanceActive = false;
  },
  updated() {
    // 暂时关闭该功能 By Cj
    // setTimeout(this.updatePreview, 0)
  },
  watch: {
    getMenuRefresh(newVal) {
      if (newVal) {
        this.getOpenedViewList.map(item => {
          if (item.$options.name === this.getRefreshMenuName) {
            if (item.menuRefresh) {
              item.menuRefresh();
            }
          }
        });
      }
      this.$store.commit('SET_MENU_REFRESH', false);
    }
  },
  beforeDestroy() {
    this.$store.commit('REMOVE_MENU_BY_OBJ', this);
    if (this.updatePreviewSto) {
      clearTimeout(this.updatePreviewSto);
      this.updatePreviewSto = null;
    }
  },
  methods: {
    // updatePreview() {
    //   if (this.isInstanceActive) {
    //     if (this.updatePreviewSto) {
    //       clearTimeout(this.updatePreviewSto)
    //     }
    //     this.updatePreviewSto = setTimeout(this.makePreview, 3000)
    //   }
    // },
    // makePreview() {
    //   if (this.isInstanceActive) {
    //     let options = {
    //       scale: 1,
    //       width: this.$el.offsetWidth,
    //       height: this.$el.offsetHeight
    //     }
    //     html2canvas(this.$el, options).then((previewCanvas) => {
    //       this.previewDataLength = previewCanvas.toDataURL().length
    //       this.$store.commit('UPDATE_MENU_PREVIEW', {
    //         vm: this,
    //         previewCanvas: previewCanvas.toDataURL()
    //       })
    //       this.updatePreviewSto = null
    //     })
    //   } else {
    //     this.updatePreviewSto = null
    //   }
    // }
  }
};
