import { Modal } from 'ant-design-vue';
import { httpAction } from '@/api/manage';
import { formDataPretreatment } from '@utils/avic/common/formDataPretreatment';
// import '@assets/styles/css/main-sub-table.css';
export const mainSubTableMixin = {
  data() {
    return {
      selectedRowKeys: [],
      sidx: '', // 排序字段
      sord: '', // 排序方式: desc降序 asc升序
      dataRecord: [] // 记录每次刷新表格的数据
    };
  },
  methods: {
    handleRowSelectionMain(obj) {
      if (obj.selectedRowKeys) {
        this.selectedRowKeys = obj.selectedRowKeys;
        this.setMainId(obj.selectedRowKeys);
      }
    },

    reloadList() {
      // 提示成功
      this.$message.success('保存成功！');
      // 刷新表格
      this.$emit('reloadData', true);
      this.modalLoading = false;
      this.visible = false;
    },

    mainHandleDelete(params) {
      // 点击删除按钮
      Modal.confirm({
        title: '确定删除吗？',
        onOk: () => {
          this.delLoading = true;
          httpAction(this.delParam.url, params.param.ids.join(','), this.delParam.method)
            .then(res => {
              if (res.success) {
                // 提示成功
                this.$message.success('删除成功！');
                this.reloadDataMain(false, params.refName, params.param.type);

                // 清空表格选中项
                // this.selectedRowKeys = [];
                // 刷新表格
                // this.reloadData(false, params.refName, params.type);
              }
              this.delLoading = false;
            })
            .catch(() => {
              this.delLoading = false;
            });
        }
      });
    },

    mainSaveAddForm(e) {
      // 点击保存按钮
      e.preventDefault();
      let that = this;
      // 校验表单并处理
      this.form.validateFields((err, values) => {
        if (!err) {
          this.modalLoading = true;
          // 提交前数据处理
          let newValues = formDataPretreatment(values, this.modalFormTagTypeObj);
          // 提交
          httpAction(this.addParam.url, newValues, this.addParam.method)
            .then(res => {
              if (res.success) {
                that.$message.warning('保存成功！');
                that.$emit('reloadDataMain', true);
                that.modalLoading = false;
                that.visible = false;
                // that.setMainId([res.result]);
                // if(that.attachmentRefName){
                //    //附件上传formId
                //    that.formId = res.result;
                //   //附件上传
                //   that.$refs[that.attachmentRefName].upload(res.result);
                // }else{
                //   that.reloadList()
                // }
              } else {
                that.modalLoading = false;
                that.visible = false;
              }
            })
            .catch(() => {
              this.modalLoading = false;
              this.visible = false;
            });
        }
      });
    },
    mainSaveEditForm(e) {
      // 点击保存按钮
      e.preventDefault();
      let that = this;
      // 校验表单并处理
      this.form.validateFields((err, values) => {
        if (!err) {
          this.modalLoading = true;
          // 提交前数据处理
          let newValues = formDataPretreatment(values, this.modalFormTagTypeObj);
          // 提交
          httpAction(this.editParam.url, newValues, this.editParam.method)
            .then(res => {
              if (res.success) {
                that.$message.warning('保存成功！');
                that.$emit('reloadDataMain', true);
                that.setMainId([res.result]);
                // if(that.attachmentRefName){
                //   //附件上传
                //   that.$refs[that.attachmentRefName].upload(res.result);
                // }else{
                //   that.reloadList()
                // }
              } else {
                that.modalLoading = false;
                that.visible = false;
              }
            })
            .catch(() => {
              this.modalLoading = false;
              this.visible = false;
            });
        }
      });
    },

    searchByKeyWord(params) {
      // 普通搜索,根据搜索内容刷新表格,从第一页开始
      let that = this;
      if (params && params.param.type == 'main') {
        this.$refs[params.refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            if (that.data.length > 0) {
              that.selectedRowKeys = [that.data[0][that.rowKey]];
              that.setMainId(that.selectedRowKeys);
            }
          }
        );
      } else {
        this.$refs[params.refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            if (that.data.length > 0) {
              that.selectedRowKeys = [that.data[0][that.rowKey]];
            }
          }
        );
      }
    },

    reloadDataMain(current, refName, type) {
      let that = this;
      if (current) {
        this.page = 1;
      }
      if (type && (type == 'delete' || type == 'add')) {
        this.$refs[refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            if (that.data.length > 0) {
              that.selectedRowKeys = [that.data[0][that.rowKey]];
              that.setMainId(that.selectedRowKeys);
            }
          }
        );
      } else {
        this.$refs[refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {}
        );
      }
    },
    reloadDataSub(current, refName, type) {
      let that = this;
      if (current) {
        this.page = 1;
      }
      if (type && (type == 'delete' || type == 'add')) {
        this.$refs[refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            if (that.data.length > 0) {
              that.selectedRowKeys = [that.data[0][that.rowKey]];
            }
          }
        );
      } else {
        this.$refs[refName].loadData(
          {
            url: this.queryParam.url,
            parameter: this.queryParam.parameter,
            method: this.queryParam.method
          },
          function () {
            if (that.data.length > 0) {
              that.selectedRowKeys = [that.data[0][that.rowKey]];
            }
          }
        );
      }
    },
    /**
     * 子表
     */
    subHandleAdd(params) {
      // 点击添加按钮,调用添加弹窗方法
      if (this.mainId.length != 1 || this.mainId[0] == '-1') {
        this.$message.warning('请选择一条主表数据！');
        return;
      } else {
        this.$refs[params.refName].addModalHandleAdd();
      }
    },
    addModalHandleAdd() {
      // 父组件调用的添加方法
      // 清空添加弹窗的表单
      this.form.resetFields();
      // 打开添加弹窗
      this.visible = true;
      // 获取通用代码
      this.loadLookUp(this.modalFormLookupParams);
    },

    subSaveAddForm(e) {
      // 点击保存按钮
      e.preventDefault();
      let that = this;
      // 校验表单并处理
      this.form.setFieldsValue({ projectClassId: this.mainId[0] });
      this.form.validateFields((err, values) => {
        if (!err) {
          this.modalLoading = true;
          // 提交前数据处理
          let newValues = formDataPretreatment(values, this.modalFormTagTypeObj);
          // 提交
          httpAction(this.addParam.url, newValues, this.addParam.method)
            .then(res => {
              if (res.success) {
                that.$message.warning('保存成功！');
                that.$emit('reloadDataSub', true);
                that.modalLoading = false;
                that.visible = false;

                // that.setMainId([res.result]);
                // if(that.attachmentRefName){
                //    //附件上传formId
                //    that.formId = res.result;
                //   //附件上传
                //   that.$refs[that.attachmentRefName].upload(res.result);
                // }else{
                //   that.reloadList()
                // }
              } else {
                that.modalLoading = false;
                that.visible = false;
              }
            })
            .catch(() => {
              this.modalLoading = false;
              this.visible = false;
            });
        }
      });
    },
    subSaveEditForm(e) {
      // 点击保存按钮
      e.preventDefault();
      let that = this;
      // 校验表单并处理
      this.form.setFieldsValue({ projectClassId: this.mainId[0] });
      this.form.validateFields((err, values) => {
        if (!err) {
          this.modalLoading = true;
          // 提交前数据处理
          let newValues = formDataPretreatment(values, this.modalFormTagTypeObj);
          // 提交
          httpAction(this.editParam.url, newValues, this.editParam.method)
            .then(res => {
              if (res.success) {
                that.$message.warning('保存成功！');
                that.$emit('reloadDataSub', true);
                // that.setMainId([res.result]);
                // if(that.attachmentRefName){
                //   //附件上传
                //   that.$refs[that.attachmentRefName].upload(res.result);
                // }else{
                //   that.reloadList()
                // }
              } else {
                that.modalLoading = false;
                that.visible = false;
              }
            })
            .catch(() => {
              this.modalLoading = false;
              this.visible = false;
            });
        }
      });
    }
  }
};
