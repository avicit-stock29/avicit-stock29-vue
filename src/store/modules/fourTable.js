const fourTable = {
  state: {
    changeInfoId: '',
    materInfoId: '',
    materDetailId: '',
    changeInfo: null,
    materInfo: null,
    materDetail: null,
    recordInfo: null,
    ede: {}
  },
  mutations: {
    SET_CHANGE_INFO_ID(state, changeInfoId) {
      state.changeInfoId = changeInfoId;
    },
    SET_MATER_INFO_ID(state, materInfoId) {
      state.materInfoId = materInfoId;
    },
    SET_MATER_DETAIL_ID(state, materDetailId) {
      state.materDetailId = materDetailId;
    },
    SET_CHANGE_INFO(state, changeInfo) {
      state.changeInfo = changeInfo;
    },
    SET_MATER_IFNO(state, materInfo) {
      state.materInfo = materInfo;
    },
    SET_MATER_DETAIL(state, materDetail) {
      state.materDetail = materDetail;
    },
    SET_RECORD_INFO(state, recordInfo) {
      state.recordInfo = recordInfo;
    }
  },
  actions: {
    setRecordInfo: ({ commit, state }, recordInfo) => {
      let data = recordInfo.data;
      let type = recordInfo.type;
      let dataAll = state.recordInfo;
      let resData = {};
      if (dataAll && dataAll.length > 0) {
        resData = dataAll[0];
      }
      if (null != data) {
        if (type == 'changeInfo') {
          resData.bomType = data.bomType;
          resData.changeCate = data.changeCate;
          resData.changeDate = data.changeDate;
          resData.changeStatus = data.changeStatus;
        }
        if (type == 'materInfo') {
          resData.materialsCode = data.materialsCode;
        }
        if (type == 'materDetail') {
          resData.everyNum = data.everyNum;
          resData.workRoute = data.workRoute;
          resData.materOrder = data.materOrder;
          resData.startTime = data.startTime;
          resData.endTime = data.endTime;
        }
      } else {
        if (type == 'changeInfo') {
          resData.bomType = '';
          resData.changeCate = '';
          resData.changeDate = '';
          resData.changeStatus = '';
        }
        if (type == 'materInfo') {
          resData.materialsCode = '';
        }
        if (type == 'materDetail') {
          resData.everyNum = '';
          resData.workRoute = '';
          resData.materOrder = '';
          resData.startTime = '';
          resData.endTime = '';
        }
      }
      commit('SET_RECORD_INFO', [resData]);
    },
    setChangeInfoId: ({ commit }, changeInfoId) => {
      commit('SET_CHANGE_INFO_ID', changeInfoId);
    },
    setMaterInfoId: ({ commit }, materInfoId) => {
      commit('SET_MATER_INFO_ID', materInfoId);
    },
    setMaterDetailId: ({ commit }, materDetailId) => {
      commit('SET_MATER_DETAIL_ID', materDetailId);
    },
    setChangeInfo: ({ commit }, changeInfo) => {
      commit('SET_CHANGE_INFO', changeInfo);
    },
    setMaterInfo: ({ commit }, materInfo) => {
      commit('SET_MATER_IFNO', materInfo);
    },
    setMaterDetail: ({ commit }, materDetail) => {
      commit('SET_MATER_DETAIL', materDetail);
    }
  },
  getters: {
    /** 返回已打开标签页组件集合 */
    getChangeInfoId: state => {
      return state.changeInfoId;
    },
    getMaterInfoId: state => {
      return state.materInfoId;
    },
    getMaterDetailId: state => {
      return state.materDetailId;
    },
    getChangeInfo: state => {
      return state.changeInfo;
    },
    getMaterInfo: state => {
      return state.materInfo;
    },
    getMaterDetail: state => {
      return state.materDetail;
    },
    getRecordInfo: state => {
      return state.recordInfo;
    }
  }
};
export default fourTable;
