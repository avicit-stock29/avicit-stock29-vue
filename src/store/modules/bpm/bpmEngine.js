/**
 * 流程操作
 */
const bpmEngine = {
  state: {
    /**
     * 以下变量仅代表流程按钮相关VUEX模块是否被动态注册过
     * 注意：TRUE只表示已发起注册，并不代表模块已被注册完成
     * */
    installingBpmSave: false, // 已发起注册异步模块
    installedBpmSave: false, // 异步模块注册完成
    uninstalledBpmSave: false, // 模块已卸载
    /* 提交 */
    installingBpmSubmit: false,
    installedBpmSubmit: false,
    uninstalledBpmSubmit: false,
    /* 已阅 */
    installingBpmTransmitSubmit: false,
    installedBpmTransmitSubmit: false,
    uninstalledBpmTransmitSubmit: false,
    /* 拿回 */
    installingBpmWithdraw: false,
    installedBpmWithdraw: false,
    uninstalledBpmWithdraw: false,
    /* 补发 */
    installingBpmSupplement: false,
    installedBpmSupplement: false,
    uninstalledBpmSupplement: false,
    /* 发送阅知、流程转发 */
    installingBpmTransmit: false,
    installedBpmTransmit: false,
    uninstalledBpmTransmit: false,
    /* 加签 */
    installingBpmAdduser: false,
    installedBpmAdduser: false,
    uninstalledBpmAdduser: false,
    /* 减签 */
    installingBpmWithdrawassignee: false,
    installedBpmWithdrawassignee: false,
    uninstalledBpmWithdrawassignee: false,
    /* 流程转办 */
    installingBpmSupersede: false,
    installedBpmSupersede: false,
    uninstalledBpmSupersede: false,
    /* 发送催办 */
    installingBpmPresstodo: false,
    installedBpmPresstodo: false,
    uninstalledBpmPresstodo: false,
    /* 自定义选人 */
    installingBpmStepuserdefined: false,
    installedBpmStepuserdefined: false,
    uninstalledBpmStepuserdefined: false,
    /* 增加读者 */
    installingBpmTaskreader: false,
    installedBpmTaskreader: false,
    uninstalledBpmTaskreader: false,
    /* 发起子流程 */
    installingBpmStartsubflow: false,
    installedBpmStartsubflow: false,
    uninstalledBpmStartsubflow: false,
    /* 意见修改 */
    installingBpmGlobalidea: false,
    installedBpmGlobalidea: false,
    uninstalledBpmGlobalidea: false,
    /* 流程跳转 */
    installingBpmGlobaljump: false,
    installedBpmGlobaljump: false,
    uninstalledBpmGlobaljump: false,
    /* 流程暂停 */
    installingBpmGlobalsuspend: false,
    installedBpmGlobalsuspend: false,
    uninstalledBpmGlobalsuspend: false,
    /* 流程恢复 */
    installingBpmGlobalresume: false,
    installedBpmGlobalresume: false,
    uninstalledBpmGlobalresume: false,
    /* 流程结束 */
    installingBpmGlobalend: false,
    installedBpmGlobalend: false,
    uninstalledBpmGlobalend: false,
    /* 关注工作 */
    installingBpmFocus: false,
    installedBpmFocus: false,
    uninstalledBpmFocus: false,
    /* 流程跟踪 */
    installingBpmButTrack: false,
    installedBpmButTrack: false,
    uninstalledBpmButTrack: false,
    /* 流程退回 */
    installingBpmRetreat: false,
    installedBpmRetreat: false,
    uninstalledBpmRetreat: false,
    /* 帮助 */
    installingBpmButHelp: false,
    installedBpmButHelp: false,
    uninstalledBpmButHelp: false,
    /* 流程选人 */
    installingBpmCommonSelect: false,
    installedBpmCommonSelect: false,
    uninstalledBpmCommonSelect: false,
    /* bpmEditor */
    installingBpmEditor: false,
    installedBpmEditor: false,
    uninstalledBpmEditor: false,
    /* bpmModel */
    installingBpmModel: false,
    installedBpmModel: false,
    uninstalledBpmModel: false,
    /* 流程节点选择 */
    installingBpmNodeSelect: false,
    installedBpmNodeSelect: false,
    uninstalledBpmNodeSelect: false
  },
  mutations: {
    installModule: (state, moduleName) => {
      let mName = moduleName.substring(0, 1).toUpperCase() + moduleName.substring(1);
      state['installing' + mName] = true;
      state['installed' + mName] = false;
      state['uninstalled' + mName] = false;
    },
    installedModule: (state, moduleName) => {
      let mName = moduleName.substring(0, 1).toUpperCase() + moduleName.substring(1);
      state['installed' + mName] = true;
      state['installing' + mName] = false;
    },
    uninstallModule: (state, moduleName) => {
      let mName = moduleName.substring(0, 1).toUpperCase() + moduleName.substring(1);
      state['uninstalled' + mName] = true;
      state['installed' + mName] = false;
    }
  },
  actions: {},
  getters: {}
};
export default bpmEngine;
