import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 意见修改
 */
const bpmGlobalidea = {
  state: {
    data: {},
    enable: false,
    globalideaVisible: false
  },
  mutations: {
    /**
     * 控制意见修改弹窗显隐
     */
    globalideaIsVisible(state, globalideaVisible) {
      state.globalideaVisible = globalideaVisible;
    }
  },
  actions: {
    /**
     * 初始化
     */
    BpmGlobalideaInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     */
    BpmGlobalideaExecute: ({ dispatch, commit, state }) => {
      commit('globalideaIsVisible', true);
    },
    /**
     * 关闭意见修改弹窗
     */
    closeGlobalideaModal: ({ dispatch, commit, state }) => {
      commit('globalideaIsVisible', false);
    }
  },
  getters: {
    /**
     * 获取意见修改弹窗显隐
     */
    getGlobalideaVisible: state => {
      return state.globalideaVisible;
    }
  }
};
export default bpmGlobalidea;
