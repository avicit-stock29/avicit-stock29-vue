import { message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 发起子流程
 */
const bpmStartsubflow = {
  state: {
    data: {},
    enable: false,
    startsubflowVisible: false,
    isShowAllFlowModel: false,
    allFlowModelData: [],
    selectedFlowModelData: []
  },
  mutations: {
    /**
     * 控制发起子流程弹窗显隐
     */
    startsubflowIsVisible(state, startsubflowVisible) {
      state.startsubflowVisible = startsubflowVisible;
    }
  },
  actions: {
    /**
     * 初始化
     */
    BpmStartsubflowInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     */
    BpmStartsubflowExecute: ({ dispatch, commit, state }) => {
      httpAction(
        bpmUtils.baseurl + '/business/getSubPdId/v1',
        {
          procinstDbid: state.data.procinstDbid,
          executionId: state.data.executionId
        },
        'post'
      ).then(res => {
        if (res.success) {
          if (res.result.length === 0) {
            //默认展示所有流程模板
            dispatch('showAllFlowModel');
          } else if (res.result.length === 1) {
            // 直接跳选人
            dispatch('executeUserInfo', res.result[0].id);
          } else {
            // 展示配置的模板
            dispatch('executeSelectId', res.result);
          }
        }
      });
    },
    showAllFlowModel: ({ dispatch, commit, state }) => {
      // 所有的模板
      state.isShowAllFlowModel = true;
      httpAction(bpmUtils.baseurl + '/business/processStartView/v1', {}, 'get').then(res => {
        if (res.success) {
          state.allFlowModelData = res.result;
        }
      });
      commit('startsubflowIsVisible', true);
    },
    executeSelectId: ({ dispatch, commit, state }, ids) => {
      // 配置的模板
      state.isShowAllFlowModel = false;
      state.selectedFlowModelData = ids;
      commit('startsubflowIsVisible', true);
    },
    executeUserInfo: ({ dispatch, commit, state }, id) => {
      // 如果有模板弹窗,则关闭
      if (state.startsubflowVisible) {
        commit('startsubflowIsVisible', false);
      }
      state.data.deployId = id;
      dispatch('openBpmCommonSelect', {
        buttonData: state.data,
        submitFuncName: 'BpmStartsubflowSubmit'
      });
    },
    /**
     * 选人回调
     * @param params
     * {
     *    buttonData
     *    users
     * }
     */
    BpmStartsubflowSubmit: ({ dispatch, commit, state }, params) => {
      if (!bpmUtils.notNull(params.users) || params.users.length === 0) {
        message.warning('选人错误');
        return;
      }
      httpAction(
        bpmUtils.baseurl + '/business/dostartsubprocess/v1',
        {
          procinstDbid: params.buttonData.procinstDbid,
          executionId: params.buttonData.executionId,
          userJson: params.users,
          id: params.buttonData.deployId
        },
        'post'
      ).then(res => {
        if (res.success) {
          dispatch('closeBpmCommonSelect');
          message.success('操作成功！');
          dispatch('createButtons');
          bpmUtils.refreshBack();
        }
      });
    },
    /**
     * 关闭发起子流程弹窗
     */
    closeStartsubflowModal: ({ dispatch, commit, state }) => {
      commit('startsubflowIsVisible', false);
    }
  },
  getters: {
    /**
     * 获取发起子流程弹窗显隐
     */
    getStartsubflowVisible: state => {
      return state.startsubflowVisible;
    },
    /**
     * 获取是否展示所有模板
     */
    getIsShowAllFlowModel: state => {
      return state.isShowAllFlowModel;
    },
    /**
     * 获取所有的模板数据
     */
    getAllFlowModelData: state => {
      return state.allFlowModelData;
    },
    /**
     * 获取配置的模板数据
     */
    getSelectedFlowModelData: state => {
      return state.selectedFlowModelData;
    }
  }
};
export default bpmStartsubflow;
