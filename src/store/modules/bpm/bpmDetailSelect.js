/**
 * 跳转流程详情页面多个任务选择打开的任务操作
 * TODO 此模块的使用位于FlowDetailList组件(流程相关模块的必要组件)，因此使用VUEX保存数据存在多视图切换的局限
 */
const bpmDetailSelect = {
  state: {
    data: [],
    flowDetailUrl: ''
  },
  mutations: {
    setFlowDetailSelectData: (state, data) => {
      state.data = data;
    },
    setFlowDetailUrl: (state, flowDetailUrl) => {
      state.flowDetailUrl = flowDetailUrl;
    }
  },
  actions: {
    /**
     * 打开流程详情跳转选择
     */
    openFlowDetailSelect: ({ dispatch, commit, state }, data) => {
      commit('setFlowDetailSelectData', data.data);
      commit('setFlowDetailUrl', data.flowDetailUrl);
    }
  },
  getters: {
    getFlowDetailSelectData: state => {
      return state.data;
    },
    getFlowDetailUrl: state => {
      return state.flowDetailUrl;
    }
  }
};
export default bpmDetailSelect;
