import { message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 流程跳转
 */
const bpmGlobaljump = {
  state: {
    data: {},
    enable: false
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmGlobaljumpInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     */
    BpmGlobaljumpExecute: ({ dispatch, commit, state }) => {
      dispatch('OpenFlowNodeSelectModal', { type: state.data.event, title: '跳转节点选择' });
    },
    /**
     * 选择流程节点回调
     * @param params
     * {
     *  executionId
     *  activityName
     * }
     */
    BpmGlobaljumpCallback: ({ dispatch, commit, state }, params) => {
      dispatch('closeFlowNodeSelectModal');
      let data = JSON.parse(JSON.stringify(state.data));
      if (bpmUtils.notNull(params.executionId)) {
        data.executionId = params.executionId;
      }
      data.targetActivityName = params.activityName;
      dispatch('openBpmCommonSelect', { buttonData: data, submitFuncName: 'BpmGlobaljumpSubmit' });
    },
    /**
     * 选人回调
     * @param params
     * {
     *    buttonData
     *    users
     * }
     */
    BpmGlobaljumpSubmit: ({ dispatch, commit, state }, params) => {
      if (!bpmUtils.notNull(params.users) || params.users.length == 0) {
        message.warning('选人错误');
        return;
      }
      httpAction(
        bpmUtils.baseurl + '/business/dojump/v1',
        {
          procinstDbid: params.buttonData.procinstDbid,
          executionId: params.buttonData.executionId,
          userJson: params.users,
          activityName: params.buttonData.targetActivityName
        },
        'post'
      ).then(res => {
        if (res.success) {
          dispatch('closeBpmCommonSelect');
          message.success(
            '跳转成功！表单将自动关闭',
            2,
            function () {
              bpmUtils.closeWindow();
              setTimeout(function () {
                dispatch('createButtons');
              }, 500);
            },
            true
          );
          try {
            //按钮点击后置事件
            dispatch('afterButtons', { event: params.buttonData.event });
          } catch (e) {}
        }
      });
    }
  },
  getters: {}
};
export default bpmGlobaljump;
