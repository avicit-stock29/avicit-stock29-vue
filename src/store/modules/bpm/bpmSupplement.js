import { message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 补发
 */
const bpmSupplement = {
  state: {
    data: {},
    enable: false
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmSupplementInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     */
    BpmSupplementExecute: ({ dispatch, commit, state }) => {
      httpAction(
        bpmUtils.baseurl + '/clientbpmdisplayaction/getcoordinate/v1',
        {
          processInstanceId: state.data.procinstDbid
        },
        'post'
      ).then(res => {
        if (res.success) {
          if (bpmUtils.notNull(res.result)) {
            for (let key in res.result) {
              let activity = res.result[key];
              let activityName = activity.activityName;
              let isCurrent = activity.isCurrent;
              let executionId = activity.executionId;
              let isAlone = activity.executionAlone;
              // 只有一个当前节点时候补发操作和拿回操作自动处理
              if (isAlone && isCurrent == 'true') {
                dispatch('BpmSupplementCallback', {
                  executionId,
                  activityName
                });
                return;
              }
            }
            dispatch('OpenFlowNodeSelectModal', { type: state.data.event, title: '补发节点选择' });
          }
        }
      });
    },
    /**
     * 选择流程节点回调
     * @param params
     * {
     *    executionId
     *    activityName
     * }
     */
    BpmSupplementCallback: ({ dispatch, commit, state }, params) => {
      dispatch('closeFlowNodeSelectModal');
      let data = JSON.parse(JSON.stringify(state.data));
      data.executionId = params.executionId;
      data.targetActivityName = params.activityName;
      dispatch('openBpmCommonSelect', { buttonData: data, submitFuncName: 'BpmSupplementSubmit' });
    },
    /**
     * 选人回调
     * @param params
     * {
     *    buttonData
     *    users
     * }
     */
    BpmSupplementSubmit: ({ dispatch, commit, state }, params) => {
      if (!bpmUtils.notNull(params.users) || params.users.length === 0) {
        message.warning('选人错误');
        return;
      }
      httpAction(
        bpmUtils.baseurl + '/business/dosupplementassignee/v1',
        {
          procinstDbid: params.buttonData.procinstDbid,
          executionId: params.buttonData.executionId,
          userJson: params.users,
          opType: 'dosupplement'
        },
        'post'
      ).then(res => {
        if (res.success) {
          dispatch('closeBpmCommonSelect');
          message.success('补发成功');
          try {
            //按钮点击后置事件
            dispatch('afterButtons', { event: params.buttonData.event });
          } catch (e) {}
          dispatch('createButtons');
        }
      });
    }
  },
  getters: {}
};
export default bpmSupplement;
