import { message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 自定义选人
 */
const bpmStepuserdefined = {
  state: {
    data: {},
    enable: false
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmStepuserdefinedInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     */
    BpmStepuserdefinedExecute: ({ dispatch, commit, state }) => {
      dispatch('OpenFlowNodeSelectModal', { type: state.data.event, title: '自定义审批人' });
    },
    /**
     * 选择流程节点回调
     * @param params
     * {
     *  executionId
     *  activityName
     * }
     */
    BpmStepuserdefinedCallback: ({ dispatch, commit, state }, params) => {
      dispatch('closeFlowNodeSelectModal');
      // httpAction(bpmUtils.baseurl + '/business/getBpmStepPerson/v1', {
      //   entryId: params.procinstDbid,
      //   activityName: params.activityName,
      // }, 'post')
      //   .then((res) => {
      //     if (res.success) {
      //       var data = JSON.parse(JSON.stringify(state.data));
      //       data.activityName = params.activityName;
      //       data.targetActivityName = params.activityName;
      //       dispatch('openBpmCommonSelect', { buttonData: data, submitFuncName: 'BpmStepuserdefinedSubmit' });
      //     }
      //   })
      let data = JSON.parse(JSON.stringify(state.data));
      data.activityName = params.activityName;
      data.targetActivityName = params.activityName;
      dispatch('openBpmCommonSelect', {
        buttonData: data,
        submitFuncName: 'BpmStepuserdefinedSubmit'
      });
    },
    /**
     * 选人回调
     * @param params
     * {
     *    buttonData
     *    users
     * }
     */
    BpmStepuserdefinedSubmit: ({ dispatch, commit, state }, params) => {
      if (!bpmUtils.notNull(params.users) || params.users.length === 0) {
        message.warning('选人错误');
        return;
      }
      console.log(params, '---params------');
      httpAction(
        bpmUtils.baseurl + '/business/douserdefined/v1',
        {
          procinstDbid: params.buttonData.procinstDbid,
          userJson: params.users,
          //selectUserIds: params.users,
          activityName: params.buttonData.activityName
        },
        'post'
      ).then(res => {
        if (res.success) {
          dispatch('closeBpmCommonSelect');
          message.success('操作成功');
          try {
            //按钮点击后置事件
            dispatch('afterButtons', { event: params.buttonData.event });
          } catch (e) {}
        }
      });
    }
  },
  getters: {}
};
export default bpmStepuserdefined;
