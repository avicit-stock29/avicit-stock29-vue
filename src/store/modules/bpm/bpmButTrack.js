/**
 * 流程跟踪
 */
const bpmButTrack = {
  state: {
    butTrackVisible: false
  },
  mutations: {
    /**
     * 控制流程跟踪弹窗显隐
     */
    butTrackIsVisible(state, butTrackVisible) {
      state.butTrackVisible = butTrackVisible;
    }
  },
  actions: {
    /**
     * 执行
     */
    BpmButTrackExecute: ({ dispatch, commit, state }) => {
      commit('butTrackIsVisible', true);
    },
    /**
     * 关闭流程跟踪弹窗
     */
    closeButTrackModal: ({ dispatch, commit, state }) => {
      commit('butTrackIsVisible', false);
    }
  },
  getters: {
    /**
     * 获取流程跟踪弹窗显隐
     */
    getButTrackVisible: state => {
      return state.butTrackVisible;
    }
  }
};
export default bpmButTrack;
