import Store from '@/store/index';
import { message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 关注工作
 */
const bpmFocus = {
  state: {
    data: {},
    enable: false
  },
  mutations: {},
  actions: {
    /**
     * 初始化
     */
    BpmFocusInitFunc: ({ dispatch, commit, state }, params) => {
      state.data = params.data;
      if (bpmUtils.notNull(state.data)) {
        state.enable = true;
      }
    },
    /**
     * 执行
     */
    BpmFocusExecute: ({ dispatch, commit, state }) => {
      httpAction(
        bpmUtils.baseurl + '/business/setFocusedTask/v1',
        {
          processInstanceId: Store.state.bpmModel.entryId,
          dbid: Store.state.bpmModel.taskId
        },
        'post'
      ).then(res => {
        if (res.success) {
          if (res.result == false) {
            message.success('该任务此前已被关注');
          } else {
            message.success('已成功关注该任务');
          }
        }
      });
    }
  },
  getters: {}
};
export default bpmFocus;
