import Store from '@/store/index';
import { Modal, message } from 'ant-design-vue';
import { httpAction } from '@api/manage';
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';
/**
 * 流程操作
 */
const bpmditor = {
  state: {
    flowIdea: '',
    isStart: false, // 是否新建
    isStartPage: false, // 是否是发起页面
    afterInit: false, // 自定义的加载后事件是否已经执行
    needRefreshPic: false, // 刷新是否被挂起
    needRefreshIdea: false, // 刷新是否被挂起
    leadButCodeList: [], // 引导按钮
    flowButtons: [], // 权限按钮
    formSecuritys: [], //表单权限,
    loadingState: true, //表单加载蒙层状态
    isButtonLoading: false, // 流程按钮加载蒙层状态
    loadingSaveState: false // 表单提交蒙层状态
  },
  mutations: {
    setFlowIdea: (state, flowIdea) => {
      state.flowIdea = flowIdea;
    },
    setLoadingState: (state, loadingState) => {
      state.loadingState = loadingState;
    },
    setIsButtonLoading: (state, isButtonLoading) => {
      state.isButtonLoading = isButtonLoading;
    },
    setLoadingSaveState: (state, loadingSaveState) => {
      state.loadingSaveState = loadingSaveState;
    }
  },
  actions: {
    /**
     * 初始化
     * @param params
     * {
     *    _bpm_defineId
     *    _bpm_deploymentId
     *    _bpm_entryId
     *    _bpm_executionId
     *    _bpm_taskId
     *    _bpm_formId
     *    _bpm_firstTaskName
     *    _bpm_firstTaskAlias
     * }
     */
    flowEditorInitFunc: async ({ dispatch, commit, state }, params) => {
      console.log('flowEditorInitFunc');
      commit('flowModelInitFunc', {
        defineId: params._bpm_defineId,
        deploymentId: params._bpm_deploymentId,
        entryId: params._bpm_entryId,
        executionId: params._bpm_executionId,
        taskId: params._bpm_taskId,
        formId: params._bpm_formId
      });
      //流程意见扩展，使用名称FlowIdeaBySelf的function
      // if (typeof FlowIdeaBySelf == "function") {
      //   state.flowIdea = eval("new FlowIdeaBySelf(this)");
      // } else {
      //   state.flowIdea = new FlowIdea(this);
      // }
      // 初始化参数
      if (bpmUtils.notNull(params._bpm_defineId)) {
        state.isStart = true;
        state.isStartPage = true;
        let res = await bpmUtils.flowStart(params._bpm_defineId);
        if (res.success) {
          commit('setActivityname', res.result.firstTaskName);
          commit('setActivitylabel', res.result.firstTaskAlias);
          commit('setDeploymentId', res.result.deploymentId);
          commit('setUserIdentityKey', '6');
          commit('setUserIdentity', '拟稿人');
          dispatch('closeLoading');
        } else {
          Modal.warning({
            title:
              '流程参数错误！无法初始化权限按钮！您可能是通过非法的途径进入了当前页面，例如重复刷新页面，拷贝链接到浏览器地址栏等操作。点击确定关闭表单',
            onOk: () => {
              window.close();
            }
          });
          return false;
        }
      } else if (
        bpmUtils.notNull(
          params._bpm_entryId,
          params._bpm_executionId,
          params._bpm_taskId,
          params._bpm_formId
        )
      ) {
        // state.defaultForm.setId(params._bpm_formId);
        // state.defaultForm.initFormData();
        // let res = await bpmUtils.getDefineIdByEntryId(params._bpm_entryId);
      } else {
        Modal.warning({
          title:
            '流程参数错误！无法初始化权限按钮！您可能是通过非法的途径进入了当前页面，例如重复刷新页面，拷贝链接到浏览器地址栏等操作。点击确定关闭表单',
          onOk: () => {
            window.close();
          }
        });
        return false;
      }
      // 权限按钮
      dispatch('createButtons');
    },
    /**
     * 权限按钮重置
     * @param params
     * {
     *    flg
     *    outcome
     * }
     */
    createButtons: async ({ dispatch, commit, state }, params) => {
      // var _self = this;
      // state.leadButCodeList = state.defaultForm.getLeadButCodeList();
      // 隐藏所有按钮
      // state.hideButtons();
      if (state.isStart) {
        // // 默认提交
        // state.bpmSubmit = new bpmSubmit(this, state.defaultForm, new Object());
        // // 默认保存
        // state.bpmSave = new bpmSave(this, state.defaultForm, new Object());
        // // 帮助
        // state.bpmButHelp = new bpmButHelp(this, state.defaultForm, new Object());
        // //引用文档
        // state.bpmButFile = new BpmButFile(this, state.defaultForm, new Object());
        // //流程跟踪
        // state.bpmButTrack = new bpmButTrack(this, state.defaultForm, new Object());

        // state.setRoles();
        // //当前节点描述
        // state.hoverRemark();
        // // 控制表单元素
        // state.flowForm.controlFormInput();
        // // 流程图
        // state.refreshPic();
        // // 流程意见
        // state.refreshIdea();
        // //刷新权限按钮后事件
        // state.defaultForm.afterCreateButtons();

        // $(window).resize();

        //自定义的加载事件
        if (!state.afterInit) {
          // state.defaultForm.afterInit();
          state.afterInit = true;
        }
        let buttonListDefault = [
          {
            id: 'doformsave',
            lable: '暂存',
            event: 'doformsave',
            icon: 'icon iconfont icon-customicon-temporary-storage'
          },
          {
            id: 'dostart',
            lable: '提交',
            event: 'dostart',
            icon: 'icon iconfont icon-customicon-submit'
          },
          // {
          //   id: 'bpm_but_track',
          //   lable: '流程跟踪',
          //   event: 'bpm_but_track',
          //   icon: 'icon iconfont icon-customicon-process-tracking'
          // },
          {
            id: 'bpm_but_help',
            lable: '帮助',
            event: 'bpm_but_help',
            icon: 'icon iconfont icon-question-circle'
          }
        ];
        dispatch('controlFormInput');
        dispatch('drawDefaultButtons', {
          buttonListDefault
        });
      } else {
        // // 流程图
        // state.refreshPic();
        // // 流程意见
        // state.refreshIdea();
        let res = await bpmUtils.getOperaterightAndFormSecuritys(
          Store.state.bpmModel.entryId,
          Store.state.bpmModel.executionId,
          Store.state.bpmModel.taskId,
          Store.state.bpmModel.formId
        );
        if (res.success) {
          commit('setActivityname', res.result.bpmContent.processActivityName);
          commit('setActivitylabel', res.result.bpmContent.processActivityLabel);
          commit('setUserIdentityKey', res.result.bpmContent.userIdentityKey);
          commit('setUserIdentity', res.result.bpmContent.userIdentity);
          // eslint-disable-next-line require-atomic-updates
          state.formSecuritys = res.result.formSecuritys;

          dispatch('drawButtons', {
            operateRights: res.result
          });
          // 默认点击提交
          if (params && params.flg) {
            dispatch('clickBut', params.outcome);
          }
          // //刷新权限按钮后事件
          // state.defaultForm.afterCreateButtons();

          // $(window).resize();

          //自定义的加载事件
          if (!state.afterInit) {
            // state.defaultForm.afterInit();
            // eslint-disable-next-line require-atomic-updates
            state.afterInit = true;
          }
        }
      }
      // dispatch('controlFormInput');
      dispatch('flowRefresh');
      // commit('setLoadingState', false)
    },
    closeLoading: ({ dispatch, commit, state }, params) => {
      commit('setLoadingState', false);
    },
    /**
     * 画按钮,流程未启动时
     * buttonListDefault
     */
    drawDefaultButtons: ({ dispatch, commit, state }, params) => {
      state.flowButtons = [...params.buttonListDefault];
      for (let i = 0; i < state.flowButtons.length; i++) {
        let item = state.flowButtons[i];
        if (item.event === 'doformsave') {
          // 暂存
          if (Store.state.bpmEngine.installedBpmSave) {
            dispatch('BpmSaveInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmSave) {
            commit('installModule', 'bpmSave');
            import('@/store/modules/bpm/bpmSave').then(res => {
              Store.registerModule(['bpmSave'], res.default);
              commit('installedModule', 'bpmSave');
              dispatch('BpmSaveInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'dosubmit' || item.event === 'dostart') {
          // 提交
          if (Store.state.bpmEngine.installedBpmSubmit) {
            dispatch('BpmSubmitInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmSubmit) {
            commit('installModule', 'bpmSubmit');
            import('@/store/modules/bpm/bpmSubmit').then(res => {
              Store.registerModule(['bpmSubmit'], res.default);
              commit('installedModule', 'bpmSubmit');
              for (let i = 0; i < state.flowButtons.length; i++) {
                let btn = state.flowButtons[i];
                if (btn.event === 'dosubmit' || btn.event === 'dostart') {
                  dispatch('BpmSubmitInitFunc', {
                    data: btn
                  });
                }
              }
            });
          }
        }
      }
    },
    /**
     * 画按钮
     * operateRights
     */
    drawButtons: ({ dispatch, commit, state }, params) => {
      // 节点内容处理
      let bpmContent = params.operateRights.bpmContent;
      //如果曾经在当前节点办理过，提示用户
      if (bpmUtils.notNull(bpmContent.messageForEver)) {
        message.info(bpmContent.messageForEver);
      }
      // 按钮处理
      // 权限按钮
      let buttonListRight = params.operateRights.operateRight;
      if (buttonListRight == null) {
        return;
      }
      // 默认按钮
      let defaultButtonArray = [
        // {
        //   id: 'bpm_but_track',
        //   lable: '流程跟踪',
        //   event: 'bpm_but_track'
        // },
        {
          id: 'bpm_but_help',
          lable: '帮助',
          event: 'bpm_but_help',
          icon: 'icon iconfont icon-question-circle'
        }
      ];
      // 获取权限按钮中的退回按钮集合
      let retreatButtonsArr = []; // 退回按钮集合
      for (let i = 0; i < buttonListRight.length; i++) {
        let buttonItem = buttonListRight[i];
        if (buttonItem.event !== 'dosubmit' && buttonItem.event !== 'doretreattoactivity') {
          // 提交、跨节点退回不用默认名称，其他用默认名称
          buttonItem.lable = buttonItem.customName || buttonItem.dName || buttonItem.lable;
        }
        if (
          buttonItem.event === 'doretreattodraft' ||
          buttonItem.event === 'doretreattoprev' ||
          buttonItem.event === 'doretreattowant'
        ) {
          // 退回上一步 退回拟稿人 任意退回
          retreatButtonsArr.push(buttonItem);
        } else if (buttonItem.event === 'doretreattoactivity') {
          // 跨节点退回
          let doretreattoactivityArr = [];
          let activityArr = buttonItem.targetName.split(',');
          let activityNameArr = buttonItem.lable.split(',');
          for (let j = 0; j < activityArr.length; j++) {
            doretreattoactivityArr.push(buttonItem);
          }
          for (let k = 0; k < doretreattoactivityArr.length; k++) {
            let obj = JSON.parse(JSON.stringify(doretreattoactivityArr[k]));
            obj.targetName = activityArr[k];
            obj.lable = '退回到【' + activityNameArr[k] + '】节点';
            retreatButtonsArr.push(obj);
          }
        }
      }
      // 处理全部按钮，赋给state
      let buttonsArray = [];
      let flowButtonsArr = [...buttonListRight, ...defaultButtonArray];
      let retreatButtonsNum = 0; // 退回按钮出现次数
      for (let i = 0; i < flowButtonsArr.length; i++) {
        let btnItem = flowButtonsArr[i];
        if (
          btnItem.event === 'doretreattodraft' ||
          btnItem.event === 'doretreattoprev' ||
          btnItem.event === 'doretreattowant' ||
          btnItem.event === 'doretreattoactivity'
        ) {
          // 退回类按钮
          retreatButtonsNum++;
          if (retreatButtonsArr.length == 1) {
            buttonsArray.push(btnItem); // 一个退回按钮不作集合
          } else if (retreatButtonsArr.length > 1 && retreatButtonsNum == 1) {
            buttonsArray.push({
              retreatButtons: retreatButtonsArr
            }); // 多个退回按钮作为一个集合
          }
          if (
            !Store.state.bpmEngine.installedBpmRetreat &&
            !Store.state.bpmEngine.installingBpmRetreat
          ) {
            commit('installModule', 'bpmRetreat');
            import('@/store/modules/bpm/bpmRetreat').then(res => {
              Store.registerModule(['bpmRetreat'], res.default);
              commit('installedModule', 'bpmRetreat');
            });
            import('@/store/modules/bpm/bpmSave').then(res => {
              Store.registerModule(['bpmSave'], res.default);
              commit('installedModule', 'bpmSave');
            });
          }
        } else {
          // 其他按钮
          buttonsArray.push(btnItem);
        }
      }
      state.flowButtons = [...buttonsArray];
      for (let i = 0; i < state.flowButtons.length; i++) {
        let item = state.flowButtons[i];
        if (item.event === 'doformsave') {
          // 暂存
          if (Store.state.bpmEngine.installedBpmSave) {
            dispatch('BpmSaveInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmSave) {
            commit('installModule', 'bpmSave');
            import('@/store/modules/bpm/bpmSave').then(res => {
              Store.registerModule(['bpmSave'], res.default);
              commit('installedModule', 'bpmSave');
              dispatch('BpmSaveInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'dosubmit' || item.event === 'dostart') {
          // 提交
          if (Store.state.bpmEngine.installedBpmSubmit) {
            dispatch('BpmSubmitInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmSubmit) {
            commit('installModule', 'bpmSubmit');
            import('@/store/modules/bpm/bpmSubmit').then(res => {
              Store.registerModule(['bpmSubmit'], res.default);
              commit('installedModule', 'bpmSubmit');
              for (let i = 0; i < state.flowButtons.length; i++) {
                let btn = state.flowButtons[i];
                if (btn.event === 'dosubmit' || btn.event === 'dostart') {
                  dispatch('BpmSubmitInitFunc', {
                    data: btn
                  });
                }
              }
            });
          }
        } else if (item.event === 'dotransmitsubmit') {
          // 已阅
          if (Store.state.bpmEngine.installedBpmTransmitSubmit) {
            dispatch('BpmTransmitSubmitInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmTransmitSubmit) {
            commit('installModule', 'bpmTransmitSubmit');
            import('@/store/modules/bpm/bpmTransmitSubmit').then(res => {
              Store.registerModule(['bpmTransmitSubmit'], res.default);
              commit('installedModule', 'bpmTransmitSubmit');
              dispatch('BpmTransmitSubmitInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'dowithdraw') {
          // 拿回
          if (Store.state.bpmEngine.installedBpmWithdraw) {
            dispatch('BpmWithdrawInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmWithdraw) {
            commit('installModule', 'bpmWithdraw');
            import('@/store/modules/bpm/bpmWithdraw').then(res => {
              Store.registerModule(['bpmWithdraw'], res.default);
              commit('installedModule', 'bpmWithdraw');
              dispatch('BpmWithdrawInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'dosupplement') {
          // 补发
          if (Store.state.bpmEngine.installedBpmSupplement) {
            dispatch('BpmSupplementInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmSupplement) {
            commit('installModule', 'bpmSupplement');
            import('@/store/modules/bpm/bpmSupplement').then(res => {
              Store.registerModule(['bpmSupplement'], res.default);
              commit('installedModule', 'bpmSupplement');
              dispatch('BpmSupplementInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'dotransmit') {
          // 发送阅知即流程转发
          if (Store.state.bpmEngine.installedBpmTransmit) {
            dispatch('BpmTransmitInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmTransmit) {
            commit('installModule', 'bpmTransmit');
            import('@/store/modules/bpm/bpmTransmit').then(res => {
              Store.registerModule(['bpmTransmit'], res.default);
              commit('installedModule', 'bpmTransmit');
              dispatch('BpmTransmitInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'doadduser') {
          // 加签
          if (Store.state.bpmEngine.installedBpmAdduser) {
            dispatch('BpmAdduserInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmAdduser) {
            commit('installModule', 'bpmAdduser');
            import('@/store/modules/bpm/bpmAdduser').then(res => {
              Store.registerModule(['bpmAdduser'], res.default);
              commit('installedModule', 'bpmAdduser');
              dispatch('BpmAdduserInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'dowithdrawassignee') {
          // 减签
          if (Store.state.bpmEngine.installedBpmWithdrawassignee) {
            dispatch('BpmWithdrawassigneeInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmWithdrawassignee) {
            commit('installModule', 'bpmWithdrawassignee');
            import('@/store/modules/bpm/bpmWithdrawassignee').then(res => {
              Store.registerModule(['bpmWithdrawassignee'], res.default);
              commit('installedModule', 'bpmWithdrawassignee');
              dispatch('BpmWithdrawassigneeInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'dosupersede') {
          // 流程转办
          if (Store.state.bpmEngine.installedBpmSupersede) {
            dispatch('BpmSupersedeInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmSupersede) {
            commit('installModule', 'bpmSupersede');
            import('@/store/modules/bpm/bpmSupersede').then(res => {
              Store.registerModule(['bpmSupersede'], res.default);
              commit('installedModule', 'bpmSupersede');
              dispatch('BpmSupersedeInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'dopresstodo') {
          // 发送催办
          if (Store.state.bpmEngine.installedBpmPresstodo) {
            dispatch('BpmPresstodoInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmPresstodo) {
            commit('installModule', 'bpmPresstodo');
            import('@/store/modules/bpm/bpmPresstodo').then(res => {
              Store.registerModule(['bpmPresstodo'], res.default);
              commit('installedModule', 'bpmPresstodo');
              dispatch('BpmPresstodoInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'dostepuserdefined') {
          // 自定义选人
          if (Store.state.bpmEngine.installedBpmStepuserdefined) {
            dispatch('BpmStepuserdefinedInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmStepuserdefined) {
            commit('installModule', 'bpmStepuserdefined');
            import('@/store/modules/bpm/bpmStepuserdefined').then(res => {
              Store.registerModule(['bpmStepuserdefined'], res.default);
              commit('installedModule', 'bpmStepuserdefined');
              dispatch('BpmStepuserdefinedInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'dotaskreader') {
          // 增加读者
          if (Store.state.bpmEngine.installedBpmTaskreader) {
            dispatch('BpmTaskreaderInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmTaskreader) {
            commit('installModule', 'bpmTaskreader');
            import('@/store/modules/bpm/bpmTaskreader').then(res => {
              Store.registerModule(['bpmTaskreader'], res.default);
              commit('installedModule', 'bpmTaskreader');
              dispatch('BpmTaskreaderInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'dostartsubprocess') {
          // 发起子流程
          if (Store.state.bpmEngine.installedBpmStartsubflow) {
            dispatch('BpmStartsubflowInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmStartsubflow) {
            commit('installModule', 'bpmStartsubflow');
            import('@/store/modules/bpm/bpmStartsubflow').then(res => {
              Store.registerModule(['bpmStartsubflow'], res.default);
              commit('installedModule', 'bpmStartsubflow');
              dispatch('BpmStartsubflowInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'doglobalidea') {
          // 意见修改
          if (Store.state.bpmEngine.installedBpmGlobalidea) {
            dispatch('BpmGlobalideaInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmGlobalidea) {
            commit('installModule', 'bpmGlobalidea');
            import('@/store/modules/bpm/bpmGlobalidea').then(res => {
              Store.registerModule(['bpmGlobalidea'], res.default);
              commit('installedModule', 'bpmGlobalidea');
              dispatch('BpmGlobalideaInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'doglobaljump') {
          // 流程跳转
          if (Store.state.bpmEngine.installedBpmGlobaljump) {
            dispatch('BpmGlobaljumpInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmGlobaljump) {
            commit('installModule', 'bpmGlobaljump');
            import('@/store/modules/bpm/bpmGlobaljump').then(res => {
              Store.registerModule(['bpmGlobaljump'], res.default);
              commit('installedModule', 'bpmGlobaljump');
              dispatch('BpmGlobaljumpInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'doglobalsuspend') {
          // 流程暂停
          if (Store.state.bpmEngine.installedBpmGlobalsuspend) {
            dispatch('BpmGlobalsuspendInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmGlobalsuspend) {
            commit('installModule', 'bpmGlobalsuspend');
            import('@/store/modules/bpm/bpmGlobalsuspend').then(res => {
              Store.registerModule(['bpmGlobalsuspend'], res.default);
              commit('installedModule', 'bpmGlobalsuspend');
              dispatch('BpmGlobalsuspendInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'doglobalresume') {
          // 流程恢复
          if (Store.state.bpmEngine.installedBpmGlobalresume) {
            dispatch('BpmGlobalresumeInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmGlobalresume) {
            commit('installModule', 'bpmGlobalresume');
            import('@/store/modules/bpm/bpmGlobalresume').then(res => {
              Store.registerModule(['bpmGlobalresume'], res.default);
              commit('installedModule', 'bpmGlobalresume');
              dispatch('BpmGlobalresumeInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'doglobalend') {
          // 流程结束
          if (Store.state.bpmEngine.installedBpmGlobalend) {
            dispatch('BpmGlobalendInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmGlobalend) {
            commit('installModule', 'bpmGlobalend');
            import('@/store/modules/bpm/bpmGlobalend').then(res => {
              Store.registerModule(['bpmGlobalend'], res.default);
              commit('installedModule', 'bpmGlobalend');
              dispatch('BpmGlobalendInitFunc', {
                data: item
              });
            });
          }
        } else if (item.event === 'dofocus') {
          // 关注工作
          if (Store.state.bpmEngine.installedBpmFocus) {
            dispatch('BpmFocusInitFunc', {
              data: item
            });
          } else if (!Store.state.bpmEngine.installingBpmFocus) {
            commit('installModule', 'bpmFocus');
            import('@/store/modules/bpm/bpmFocus').then(res => {
              Store.registerModule(['bpmFocus'], res.default);
              commit('installedModule', 'bpmFocus');
              dispatch('BpmFocusInitFunc', {
                data: item
              });
            });
          }
        }
      }
    },
    /**
     * 流程启动后
     * @param result
     */
    afterStart: ({ dispatch, commit, state }, result) => {
      state.isStart = false;
      commit('setDefineId', result.defineId);
      commit('setEntryId', result.entryId);
      commit('setExecutionId', result.executionId);
      commit('setTaskId', result.taskId);
      commit('setActivityname', result.activityname);
      // state.defaultForm.setId(result.formId);
    },
    //获取表单权限数据
    controlFormInput: ({ dispatch, commit, state }) => {
      //if(state.isStart==false){
      let detailParam = {
        url: bpmUtils.baseurl + '/business/getFormSecuritys/v1',
        method: 'post'
      };
      let parameter = {
        defineId: Store.state.bpmModel.defineId,
        activityname: Store.state.bpmModel.activityname
      };
      httpAction(detailParam.url, parameter, detailParam.method)
        .then(res => {
          if (res.success) {
            state.formSecuritys = res.result;
          }
        })
        .catch(() => {});
      //}
    },
    setFlowButtons: ({ dispatch, commit, state }, flowButtons) => {
      state.flowButtons = flowButtons;
    },
    setIsButtonLoading: ({ dispatch, commit, state }, params) => {
      commit('setIsButtonLoading', params);
      if (!params && state.loadingSaveState) {
        commit('setLoadingSaveState', false);
      }
    },
    setLoadingSaveState: ({ dispatch, commit, state }, params) => {
      commit('setLoadingSaveState', params);
    }
  },
  getters: {
    /**
     * 权限按钮
     */
    getFlowButtons: state => () => {
      return state.flowButtons;
    },
    getIsButtonLoading: state => () => {
      return state.isButtonLoading;
    },
    getLoadingState: state => {
      return state.loadingState;
    },
    getLoadingSaveState: state => {
      return state.loadingSaveState;
    }
  }
};
export default bpmditor;
