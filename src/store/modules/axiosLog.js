/**
 * @description 此部分内容实现MainLayout下，标签页管理功能
 */
const axiosLog = {
  state: {
    /* 日志开关 */
    isAxiosLog: true,
    /* 日志可视化开关 */
    isAxiosLogVisible: true,
    /** 最大记录数 */
    maxLength: 50,
    /** 日志列表 */
    logList: []
  },
  mutations: {
    /**
     * @description 记录新日志
     * @param obj 新日志
     */
    ADD_AXIOS_LOG: (state, obj) => {
      state.logList.splice(0, 0, obj);
      if (state.logList.length > state.maxLength) {
        state.logList.splice(-1, 1);
      }
    }
  },
  getters: {
    getAxiosList: state => {
      return state.logList;
    }
  },
  actions: {}
};

export default axiosLog;
