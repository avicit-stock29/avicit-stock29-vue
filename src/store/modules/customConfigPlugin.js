import Vue from 'vue';
import { putCustomCfg } from '@api/api';
import { USER_INFO } from '@/store/mutationTypes';

/**
 * @description 提交个性化配置数据到数据库(观察者模式)
 * @param {vuex.$store对象} store
 */
function createCommitCustomCfgPlugin(customConfig) {
  return store => {
    // 当 store 初始化后调用
    store.subscribe((mutation, state) => {
      // 修改全局配置
      if (mutation.type === 'SAVE_GLOBAL_ITEM_CFG') {
        let obj = {
          userId: Vue.ls.get(USER_INFO).id,
          data: state.customConfig.customCfg
        };
        Vue.ls.set(state.customConfig.customCfgKeys.G_USER_CFG, obj);
        let params = {
          userId: Vue.ls.get(USER_INFO).id,
          customedCode: state.customConfig.customCfgKeys.G_USER_CFG,
          customedContent: JSON.stringify(
            state.customConfig.customCfg[state.customConfig.customCfgKeys.G_CFG]
          )
        };
        putCustomCfg(params);
      } else if (
        mutation.type === 'SAVE_MODULE_COLUMNS' ||
        mutation.type === 'SAVE_MODULE_SEARCH_HISTORY' ||
        mutation.type === 'SAVE_MODULE_PAGER_RULE' ||
        mutation.type === 'SAVE_MODULE_ITEM_CFG'
      ) {
        // 修改模块配置
        let obj = {
          userId: Vue.ls.get(USER_INFO).id,
          data: state.customConfig.customCfg
        };
        Vue.ls.set(state.customConfig.customCfgKeys.G_USER_CFG, obj);
        let params = {
          userId: Vue.ls.get(USER_INFO).id,
          customedCode: mutation.payload.key,
          customedContent: JSON.stringify(state.customConfig.customCfg[mutation.payload.key])
        };
        putCustomCfg(params);
      }
    });
  };
}
const commitCustomCfgPlugin = createCommitCustomCfgPlugin(new Vue());
export default commitCustomCfgPlugin;
