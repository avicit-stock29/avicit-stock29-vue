import { saveVisitMenu } from '@api/api';
/**
 * @description 此部分内容实现MainLayout下，标签页管理功能
 */
const tabMenu = {
  state: {
    /** 需要缓存（默认缓存所有已打开标签）的组件名列表 */
    keepAliveCompList: [],
    /** 已打开的标签页视图根组件实例集合 */
    openedViewList: [],
    /** 标签页预览图 */
    previewList: [],
    /** 接收刷新指令的标签页组件名，'' 表示当前未接收刷新指令 */
    refreshMenuName: '',
    /** 标签页刷新任务状态 (true:待执行, false:已执行或无任务) */
    isMenuRefresh: false
  },
  mutations: {
    /**
     * @description 记录新打开的标签页
     * @param obj 新打开标签页的根组件实例
     */
    ADD_MENU: (state, obj) => {
      if (state.openedViewList.indexOf(obj) === -1) {
        state.openedViewList.push(obj);
        state.previewList.push(null);
      }
    },
    UPDATE_MENU_PREVIEW: (state, obj) => {
      if (state.openedViewList.indexOf(obj.vm) !== -1) {
        state.previewList.splice(state.openedViewList.indexOf(obj.vm), 1, obj.previewCanvas);
      }
    },
    /**
     * @description 清空集合，保存参数指定的唯一标签页
     * @param obj 新打开标签页的根组件实例
     */
    RESET_BY_MENU: (state, obj) => {
      state.openedViewList = [obj];
      state.previewList = [null];
    },
    /**
     * @description 根据组件名删除已记录的标签页
     * @param name 标签页组件名
     */
    REMOVE_MENU_BY_NAME: (state, name) => {
      let _idx = -1;
      state.openedViewList.map((item, idx) => {
        if (item.$options.name && item.$options.name === name) {
          _idx = idx;
        }
      });
      if (_idx !== -1) {
        state.openedViewList.splice(_idx, 1);
        state.previewList.splice(_idx, 1);
      }
    },
    /**
     * @description 根据组件实例删除已记录的标签页
     * @param obj 目标组件实例
     */
    REMOVE_MENU_BY_OBJ: (state, obj) => {
      const index = state.openedViewList.indexOf(obj);
      if (index !== -1) {
        state.openedViewList.splice(index, 1);
        state.previewList.splice(index, 1);
      }
    },
    /**
     * @description 接收标签页刷新指令，记录目标组件名，
     * @param name 视图组件名
     */
    SET_REFRESH_MENU_NAME: (state, name) => {
      state.refreshMenuName = name;
      state.isMenuRefresh = true;
    },
    /**
     * @description 更新标签页刷新任务状态
     * @param index 目标组件索引值
     */
    SET_MENU_REFRESH: (state, isMenuRefresh) => {
      state.isMenuRefresh = isMenuRefresh;
    },
    SET_KEEPALIVE_COMP_LIST: (state, compList) => {
      state.keepAliveCompList = compList;
    }
  },
  getters: {
    /** 返回需要缓存的组件名列表 */
    getKeepAliveCompList: state => {
      return state.keepAliveCompList;
    },
    /** 返回已打开标签页组件集合 */
    getOpenedViewList: state => {
      return state.openedViewList;
    },
    /** 返回已打开标签页预览视图集合 */
    getPreviewList: state => {
      return state.previewList;
    },
    /** 返回最后一次接收刷新指令的标签页视图组件名 */
    getRefreshMenuName: state => {
      return state.refreshMenuName;
    },
    /** 返回标签页刷新任务状态 */
    getMenuRefresh: state => {
      return state.isMenuRefresh;
    }
  },
  actions: {
    // 保存最近访问历史
    saveVisitMenu({ commit, state }, routeId) {
      saveVisitMenu(routeId);
    }
  }
};

export default tabMenu;
