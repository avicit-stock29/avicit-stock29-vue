export default {
  message: {
    title: '变成中文了'
  },
  placeholder: {
    enter: '请输入文字'
  },
  brands: {
    nike: '耐克',
    adi: '阿弟打死',
    nb: '牛逼',
    ln: '李宁'
  },
  basemould: {
    add: '添加',
    edit: '编辑',
    detail: '详情',
    delete: '删除',
    save: '保存',
    import: '导入',
    export: '导出',
    inquire: '查询',
    advanceSearch: '高级查询',
    ok: '确定',
    cancel: '取消',
    back: '返回',
    clear: '清空',
    search: '请输入',
    or: '或',
    from: '第',
    to: '到第',
    items: '条',
    total: '共',
    delWarning: '请确保上一条数据修改完毕！',
    delEnquire: '确认要删除选中的数据吗？',
    delSuccess: '删除成功！',
    saveWarning: '请先修改数据！',
    saveSuccess: '保存成功！',
    requiredWarning: '为必填字段！',
    scriptWarning: '非法含有JS脚本完整标签！',
    maxLengthWarning: '不能超过',
    characters: '个字符'
  }
};
