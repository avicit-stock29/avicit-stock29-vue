export default {
  message: {
    title: 'English Style'
  },
  placeholder: {
    enter: 'Please type something'
  },
  brands: {
    nike: 'Nike',
    adi: 'Adidas',
    nb: 'New Banlance',
    ln: 'LI Ning'
  },
  basemould: {
    add: 'add',
    edit: 'edit',
    detail: 'detail',
    delete: 'delete',
    save: 'save',
    import: 'import',
    export: 'export',
    inquire: 'inquire',
    advanceSearch: 'advancedQuery',
    ok: 'ok',
    cancel: 'cancel',
    back: 'back',
    clear: 'clear',
    search: 'please input',
    or: 'or',
    from: 'from',
    to: 'to',
    items: 'items',
    total: 'total',
    delWarning: 'Please ensure that the last data is modified!',
    delEnquire: 'Are you sure you want to delete the selected data ?',
    delSuccess: 'Delete successfully!',
    saveWarning: 'Please modify the data first!',
    saveSuccess: 'save successful!',
    requiredWarning: 'is a mandatory field!',
    scriptWarning: 'illegally contains the full tag of the JS scrip!',
    maxLengthWarning: "can't exceed",
    characters: 'characters'
  }
};
