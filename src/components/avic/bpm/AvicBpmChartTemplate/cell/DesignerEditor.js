﻿/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable no-redeclare */
/* eslint-disable no-var */
///<jscompress sourcefile="MyBase.js" />
import { bpmUtils } from '@utils/avic/bpm/bpmUtils';

let CountUtils = function () {
  let start = 0;
  let end = 0;
  let task = 0;
  let java = 0;
  let sql = 0;
  let ws = 0;
  let state = 0;
  let fork = 0;
  let join = 0;
  let exclusive = 0;
  let subprocess = 0;
  let foreach = 0;
  let jms = 0;
  let custom = 0;
  let swimlane = 0;
  let selfId = 2;
  return {
    getSelfId() {
      return '' + selfId++;
    },
    getStart() {
      return ++start;
    },
    getEnd() {
      return ++end;
    },
    getTask() {
      return ++task;
    },
    getJava() {
      return ++java;
    },
    getSql() {
      return ++sql;
    },
    getWs() {
      return ++ws;
    },
    getState() {
      return ++state;
    },
    getFork() {
      return ++fork;
    },
    getJoin() {
      return ++join;
    },
    getExclusive() {
      return ++exclusive;
    },
    getSubprocess() {
      return ++subprocess;
    },
    getForeach() {
      return ++foreach;
    },
    getJms() {
      return ++jms;
    },
    getCustom() {
      return ++custom;
    },
    getSwimlane() {
      return ++swimlane;
    },
    setSelfId(value) {
      if (value >= selfId) {
        selfId = value + 1;
      }
    },
    setStart(value) {
      if (value > start) {
        start = value;
      }
    },
    setEnd(value) {
      if (value > end) {
        end = value;
      }
    },
    setTask(value) {
      if (value > task) {
        task = value;
      }
    },
    setJava(value) {
      if (value > java) {
        java = value;
      }
    },
    setSql(value) {
      if (value > sql) {
        sql = value;
      }
    },
    setWs(value) {
      if (value > ws) {
        ws = value;
      }
    },
    setState(value) {
      if (value > state) {
        state = value;
      }
    },
    setFork(value) {
      if (value > fork) {
        fork = value;
      }
    },
    setJoin(value) {
      if (value > join) {
        join = value;
      }
    },
    setExclusive(value) {
      if (value > exclusive) {
        exclusive = value;
      }
    },
    setSubprocess(value) {
      if (value > subprocess) {
        subprocess = value;
      }
    },
    setForeach(value) {
      if (value > foreach) {
        foreach = value;
      }
    },
    setJms(value) {
      if (value > jms) {
        jms = value;
      }
    },
    setCustom(value) {
      if (value > custom) {
        custom = value;
      }
    },
    setSwimlane(value) {
      if (value > swimlane) {
        swimlane = value;
      }
    }
  };
};
function offset(options) {
  if (arguments.length) {
    return options === undefined
      ? this
      : each(function (i) {
          // eslint-disable-next-line no-undef
          setOffset(this, options, i);
        });
  }

  let docElem,
    win,
    box = { top: 0, left: 0 },
    elem = options,
    doc = elem && elem.ownerDocument;

  if (!doc) {
    return;
  }

  docElem = doc.documentElement;

  // Make sure it's not a disconnected DOM node
  if (!contains(docElem, elem)) {
    return box;
  }

  // If we don't have gBCR, just use 0,0 rather than error
  // BlackBerry 5, iOS 3 (original iPhone)
  if (typeof elem.getBoundingClientRect !== typeof undefined) {
    box = elem.getBoundingClientRect();
  }
  win = getWindow(doc);
  return {
    top: box.top + (win.pageYOffset || docElem.scrollTop) - (docElem.clientTop || 0),
    left: box.left + (win.pageXOffset || docElem.scrollLeft) - (docElem.clientLeft || 0)
  };
}
function getWindow(elem) {
  return isWindow(elem)
    ? elem
    : elem.nodeType === 9
    ? elem.defaultView || elem.parentWindow
    : false;
}
function isWindow(obj) {
  return obj != null && obj == obj.window;
}
function isNative(fn) {
  // eslint-disable-next-line no-undef
  return rnative.test(fn + '');
}
function contains(docElem, elem) {
  isNative(docElem.contains) || docElem.compareDocumentPosition
    ? function (a, b) {
        let adown = a.nodeType === 9 ? a.documentElement : a,
          bup = b && b.parentNode;
        return (
          a === bup ||
          !!(
            bup &&
            bup.nodeType === 1 &&
            (adown.contains
              ? adown.contains(bup)
              : a.compareDocumentPosition && a.compareDocumentPosition(bup) & 16)
          )
        );
      }
    : function (a, b) {
        if (b) {
          while ((b = b.parentNode)) {
            if (b === a) {
              return true;
            }
          }
        }
        return false;
      };
}

function trim(res) {
  if (res) {
    return res.replace(/\s+/g, '');
  }
  return res;
}
function each(obj, callback, args) {
  let value,
    i = 0,
    length = obj.length,
    isArray = isArraylike(obj);

  if (args) {
    if (isArray) {
      for (; i < length; i++) {
        value = callback.apply(obj[i], args);

        if (value === false) {
          break;
        }
      }
    } else {
      for (i in obj) {
        value = callback.apply(obj[i], args);

        if (value === false) {
          break;
        }
      }
    }

    // A special, fast, case for the most common use of each
  } else {
    if (isArray) {
      for (; i < length; i++) {
        value = callback.call(obj[i], i, obj[i]);

        if (value === false) {
          break;
        }
      }
    } else {
      for (i in obj) {
        value = callback.call(obj[i], i, obj[i]);

        if (value === false) {
          break;
        }
      }
    }
  }

  return obj;
}
function isArraylike(obj) {
  let length = obj.length,
    type = $type(obj);

  if ($isWindow(obj)) {
    return false;
  }

  if (obj.nodeType === 1 && length) {
    return true;
  }

  return (
    type === 'array' ||
    (type !== 'function' &&
      (length === 0 || (typeof length === 'number' && length > 0 && length - 1 in obj)))
  );
}
function $isWindow(obj) {
  return obj != null && obj == obj.window;
}
let class2type = {};
function $type(obj) {
  if (obj == null) {
    return String(obj);
  }
  return typeof obj === 'object' || typeof obj === 'function'
    ? class2type[class2type.toString.call(obj)] || 'object'
    : typeof obj;
}

let _iconType = '2';
function Map() {
  this.elements = new Array();

  // 获取MAP元素个数
  this.size = function () {
    return this.elements.length;
  };

  // 判断MAP是否为空
  this.isEmpty = function () {
    return this.elements.length < 1;
  };

  // 删除MAP所有元素
  this.clear = function () {
    this.elements = new Array();
  };

  // 向MAP中增加元素（key, value)
  this.put = function (_key, _value) {
    this.elements.push({
      key: _key,
      value: _value
    });
  };

  // 删除指定KEY的元素，成功返回True，失败返回False
  this.remove = function (_key) {
    let bln = false;
    try {
      for (let i = 0; i < this.elements.length; i++) {
        if (this.elements[i].key == _key) {
          this.elements.splice(i, 1);
          return true;
        }
      }
    } catch (e) {
      bln = false;
    }
    return bln;
  };

  // 获取指定KEY的元素值VALUE，失败返回NULL
  this.get = function (_key) {
    try {
      for (let i = 0; i < this.elements.length; i++) {
        if (this.elements[i].key == _key) {
          return this.elements[i].value;
        }
      }
    } catch (e) {
      return null;
    }
  };

  // 获取指定索引的元素（使用element.key，element.value获取KEY和VALUE），失败返回NULL
  this.element = function (_index) {
    if (_index < 0 || _index >= this.elements.length) {
      return null;
    }
    return this.elements[_index];
  };

  // 判断MAP中是否含有指定KEY的元素
  this.containsKey = function (_key) {
    let bln = false;
    try {
      for (let i = 0; i < this.elements.length; i++) {
        if (this.elements[i].key == _key) {
          bln = true;
        }
      }
    } catch (e) {
      bln = false;
    }
    return bln;
  };

  // 判断MAP中是否含有指定VALUE的元素
  this.containsValue = function (_value) {
    let bln = false;
    try {
      for (let i = 0; i < this.elements.length; i++) {
        if (this.elements[i].value == _value) {
          bln = true;
        }
      }
    } catch (e) {
      bln = false;
    }
    return bln;
  };

  // 获取MAP中所有VALUE的数组（ARRAY）
  this.values = function () {
    let arr = new Array();
    for (let i = 0; i < this.elements.length; i++) {
      arr.push(this.elements[i].value);
    }
    return arr;
  };

  // 获取MAP中所有KEY的数组（ARRAY）
  this.keys = function () {
    let arr = new Array();
    for (let i = 0; i < this.elements.length; i++) {
      arr.push(this.elements[i].key);
    }
    return arr;
  };
}

/**
 * MyBase
 */
function MyBase(designerEditor, id, tagName) {
  this.designerEditor = designerEditor;
  this.id = id; // ID,流程的ID为流程的KEY
  this.tagName = tagName;
  this.cell; // 对应mxCell,通过getCell()获取
  this.forkJoinArr = [];
  // eslint-disable-next-line no-undef
  this.defineId = typeof _type !== 'undefined' ? (_type == 2 ? _pdId : _id) : null;
}
/****************初始化、组装xml入口***************************/

/**
 * 为流程跟踪提供的特殊方法
 * @param xmlValue
 * @param rootXml
 */
MyBase.prototype.initTracks = function (xmlValue, rootXml) {
  this.name = xmlValue.getAttribute('name');
  this.alias = trim(xmlValue.getAttribute('alias'));
  this.g = xmlValue.getAttribute('g');
  rootXml.appendChild(this.createMxCell(xmlValue)); // 创建mxCell
  this.designerEditor.myLoadMap.put(this.name, this); //加载时的赋值，用于连线加载时按照name来寻找target节点
};

/**
 * 初始化基本信息，流程不需要该方法
 */
MyBase.prototype.initJBXX = function () {
  document.querySelector('#' + this.id).querySelector('#xian_shi_ming_cheng').value = trim(
    this.alias
  );
  //$("#" + this.id).find("#xian_shi_ming_cheng").val(trim(this.alias));
  document.querySelector('#' + this.id).querySelector('#luo_ji_biao_shi').value = this.name;
  //$("#" + this.id).find("#luo_ji_biao_shi").val(this.name);
};

/**
 * 获取当前对应的mxCell
 *
 * @returns
 */
MyBase.prototype.getCell = function () {
  return this.designerEditor.editor.graph.getModel().getCell(this.id);
};
/**
 * 解析字符串G，连线重写了该方法
 *
 * @returns {object}
 */
MyBase.prototype.getmxCellG = function () {
  let arr = this.g.split(',');
  let result = {};
  result.x = arr[0];
  result.y = arr[1];
  result.width = arr[2];
  result.height = arr[3];
  return result;
};
/**
 * 通过name解析数字，例如end1解析出数字:1
 */
MyBase.prototype.resolve = function () {
  return Number(this.name.replace(this.tagName, ''));
};
/**
 * 构建mxCell,连线子类改写了该方法，流程不需要该方法
 */
MyBase.prototype.createMxCell = function (xmlValue) {
  let mxCellG = this.getmxCellG();
  let tagCell = bpmUtils.createElement(this.tagName);
  tagCell.setAttribute('id', this.id);
  if (_iconType == '1') {
    //新图元
    if (
      this.tagName != 'start' &&
      this.tagName != 'end' &&
      this.tagName != 'decision' &&
      this.tagName != 'fork' &&
      this.tagName != 'join'
    ) {
      tagCell.setAttribute('label', trim(this.alias));
    }
  } else {
    tagCell.setAttribute('label', trim(this.alias));
  }
  if (this.designerEditor.useNewDesigner) {
    if (bpmUtils.notNull(xmlValue.firstElementChild)) {
      tagCell.appendChild(xmlValue.firstElementChild);
    } else {
      tagCell.appendChild(xmlValue.firstChild);
    }
    return tagCell;
  }
  let mxCell = bpmUtils.createElement('mxCell');
  let mxGeometry = bpmUtils.createElement('mxGeometry');
  mxCell.setAttribute('style', this.style);
  mxCell.setAttribute('vertex', '1');
  mxCell.setAttribute('parent', '1');

  mxGeometry.setAttribute('x', mxCellG.x);
  mxGeometry.setAttribute('y', mxCellG.y);
  mxGeometry.setAttribute('width', mxCellG.width);
  mxGeometry.setAttribute('height', mxCellG.height);
  mxGeometry.setAttribute('as', 'geometry');

  mxCell.appendChild(mxGeometry);
  tagCell.appendChild(mxCell);
  return tagCell;
};

/*******************组装xml、解析xml*******************************************/
/**********整理一下其他基本方法****************************/
/**
 * 往tagNodeXml中添加对应key-value,value进行空值判断
 *
 * @param attrName
 * @param attrValue
 * @param tagNodeXml
 */
MyBase.prototype.putAttr = function (attrName, attrValue, tagNodeXml) {
  if (bpmUtils.notNull(attrValue)) {
    tagNodeXml.setAttribute(attrName, attrValue);
  }
};
/**
 * 从tagNodeXml中取出指定attrName的值
 *
 * @param tagNodeXml
 * @param attrName
 */
MyBase.prototype.getAttr = function (tagNodeXml, attrName) {
  return trim(tagNodeXml.getAttribute(attrName));
};
/**
 * 创建节点meta添加到知道tagNodeXml中
 * @param metaName
 * @param metaValue
 * @param tagNodeXml
 */
MyBase.prototype.addMeta = function (metaName, metaValue, tagNodeXml) {
  if (bpmUtils.notNull(metaValue)) {
    let meta = bpmUtils.createElement('meta');
    meta.setAttribute('name', metaName);
    meta.appendChild(bpmUtils.createTextNode(metaValue));
    tagNodeXml.appendChild(meta);
  }
};

/**
 * 加载初始化备注部分
 * @param tagNodeXml
 */
MyBase.prototype.createRemarkDom = function (tagNodeXml) {
  this.setDomValByMeta('bei_zhu_xin_xi', tagNodeXml, 'remark');
};
MyBase.prototype.addForkJoin = function (forkJoinId) {
  this.forkJoinArr.push(forkJoinId);
};
MyBase.prototype.hasForkJoin = function () {
  return this.forkJoinArr.length > 0 ? true : false;
};

MyBase.prototype.getUserSelectTextFieldValue = function (userList) {
  let showUserList = '';
  let selectedDept = '';
  let selectedUser = '';
  let selectedPosition = '';
  let selectedRole = '';
  let selectedGroup = '';
  let selectedRelation = '';
  for (let i = 0; i < userList.length; i++) {
    let user = userList[i];
    //		【部门】研发中心#普通员工;【用户】张三;【角色】一般用户;【群组】群组1;【岗位】普通员工;
    let type = user.type;
    if (type == 'dept') {
      if ('' == user.position.positionId) {
        selectedDept += user.name + ';';
      } else {
        selectedDept += user.name + '#' + user.position.positionName + ';';
      }
    } else if (type == 'user') {
      selectedUser += user.name + ';';
    } else if (type == 'position') {
      selectedPosition += user.name + ';';
    } else if (type == 'role') {
      selectedRole += user.name + ';';
    } else if (type == 'group') {
      selectedGroup += user.name + ';';
    } else if (type == 'relation') {
      selectedRelation += user.name;
      if (user.deptlevel) selectedRelation += '#' + user.deptlevel.deptlevelName;
      if (user.position) selectedRelation += '#' + user.position.positionName;
      if (user.step) selectedRelation += '#' + user.step.stepName;
      if (user.customfunction) selectedRelation += '#' + user.customfunction.customfunctionName;
      if (user.variable) selectedRelation += '#用户#' + user.variable.variableAlias;
      if (user.intersection) selectedRelation += '#交集选人#' + user.intersection.intersectionAlias;
      selectedRelation += ';';
    }
  }
  if (selectedDept != '') {
    selectedDept = '【部门】' + selectedDept;
    showUserList += selectedDept;
  }
  if (selectedUser != '') {
    selectedUser = '【用户】' + selectedUser;
    showUserList += selectedUser;
  }
  if (selectedPosition != '') {
    selectedPosition = '【岗位】' + selectedPosition;
    showUserList += selectedPosition;
  }
  if (selectedRole != '') {
    selectedRole = '【角色】' + selectedRole;
    showUserList += selectedRole;
  }
  if (selectedGroup != '') {
    selectedGroup = '【群组】' + selectedGroup;
    showUserList += selectedGroup;
  }
  if (selectedRelation != '') {
    selectedRelation = '【关系】' + selectedRelation;
    showUserList += selectedRelation;
  }
  return showUserList;
};

/**
 * 获取全局节点实例
 */
MyBase.prototype.processInstance = function () {
  let process = null;
  let values = this.designerEditor.myCellMap.values();
  each(values, function (i, n) {
    if (n.tagName == 'process') {
      process = n;
      return false;
    }
  });
  return process;
};
///<jscompress sourcefile="MyCustom.js" />
/**
 * custom extends MyBase
 */
function MyCustom(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'custom');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/task_custom.png;';
}
MyCustom.prototype = new MyBase();

///<jscompress sourcefile="MyEnd.js" />
/**
 * end extends MyBase
 */
function MyEnd(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'end');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/end_event_terminate.png;';
}
MyEnd.prototype = new MyBase();

///<jscompress sourcefile="MyExclusive.js" />
/**
 * exclusive extends MyBase
 */
function MyExclusive(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'decision');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/gateway_exclusive.png;';
}
MyExclusive.prototype = new MyBase();

///<jscompress sourcefile="MyForeach.js" />
/**
 * foreach extends MyBase
 */
function MyForeach(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'foreach');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/gateway_foreach.png;';
}
MyForeach.prototype = new MyBase();

///<jscompress sourcefile="MyFork.js" />
/**
 * fork extends MyBase
 */
function MyFork(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'fork');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/gateway_fork.png;';
}
MyFork.prototype = new MyBase();

///<jscompress sourcefile="MyJava.js" />
/**
 * java extends MyBase
 */
function MyJava(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'java');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/task_java.png;';
}
MyJava.prototype = new MyBase();

///<jscompress sourcefile="MyJms.js" />
/**
 * jms extends MyBase
 */
function MyJms(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'jms');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/task_jms.png;';
}
MyJms.prototype = new MyBase();

///<jscompress sourcefile="MyJoin.js" />
/**
 * join extends MyBase
 */
function MyJoin(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'join');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/gateway_join.png;';
}
MyJoin.prototype = new MyBase();

///<jscompress sourcefile="MySql.js" />
/**
 * sql extends MyBase
 */
function MySql(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'sql');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/task_sql.png;';
}
MySql.prototype = new MyBase();

///<jscompress sourcefile="MyStart.js" />
/**
 * start extends MyBase
 */
function MyStart(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'start');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/start_event_empty.png;';
}
MyStart.prototype = new MyBase();

///<jscompress sourcefile="MyState.js" />
/**
 * state extends MyBase
 */
function MyState(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'state');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/task_wait.png;';
}
MyState.prototype = new MyBase();

///<jscompress sourcefile="MySubprocess.js" />
/**
 * sub-process extends MyBase
 */
function MySubprocess(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'sub-process');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/task_subprocess.png;';
}
MySubprocess.prototype = new MyBase();
/**
 * 为流程跟踪提供的特殊方法
 * @param xmlValue
 * @param rootXml
 */
MySubprocess.prototype.initTracks = function (xmlValue, rootXml) {
  this.name = xmlValue.getAttribute('name');
  this.alias = trim(xmlValue.getAttribute('alias'));
  this.g = xmlValue.getAttribute('g');
  this.deploymentId = this.getAttr(xmlValue, 'sub-process-id');
  rootXml.appendChild(this.createMxCell(xmlValue)); // 创建mxCell
  this.designerEditor.myLoadMap.put(this.name, this); //加载时的赋值，用于连线加载时按照name来寻找target节点
};
///<jscompress sourcefile="MySwimlane.js" />
/**
 * swimlane extends MyBase
 */
function MySwimlane(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'swimlane');
  this.swimlaneType = '';
}
MySwimlane.prototype = new MyBase();

///<jscompress sourcefile="MyTask.js" />
/**
 * task extends MyBase
 */
function MyTask(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'task');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/task_human.png;';
}
MyTask.prototype = new MyBase();
///<jscompress sourcefile="MyTransition.js" />
/**
 * transition extends MyBase
 */
function MyTransition(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'transition');
}
MyTransition.prototype = new MyBase();

/**
 * 初始化属性信息
 */
MyTransition.prototype.init = function () {
  this.initBase();
  let targetName = this.designerEditor.myCellMap.get(this.getCell().target.id).name;
  this.name = 'to ' + targetName;
  this.alias = 'to ' + targetName;
  this.initJBXX(); // 初始化基本信息

  // 观察全局变量变化
  this.observeGlobalVariable();
  // 新建的时候同步一次全局属性
  this.processInstance().notify();
  this.initEvent();
  // 初始化流转事件
  this.nodeEvent = new NodeEvent(this.designerEditor, this.id, 'avic-digital-human-task-event');
};
/**
 * 为流程跟踪提供的特殊方法
 * @param xmlValue
 * @param rootXml
 */
MyTransition.prototype.initTracks = function (xmlValue, rootXml, tagId) {
  this.name = xmlValue.getAttribute('name');
  this.alias = trim(xmlValue.getAttribute('alias'));
  this.g = xmlValue.getAttribute('g');
  rootXml.appendChild(this.createMxCell(tagId, xmlValue)); // 创建mxCell
};
/**
 * 加载时初始化元素信息
 *
 * @param xmlValue
 * @param rootXml
 */
MyTransition.prototype.initLoad = function (xmlValue, rootXml, tagId) {
  this.initBase();
  this.name = xmlValue.getAttribute('name');
  this.alias = trim(xmlValue.getAttribute('alias'));
  this.g = xmlValue.getAttribute('g');
  this.order = xmlValue.getAttribute('order');
  document.querySelector('#' + this.id).querySelector('#xian_shi_shun_xu').value = this.order;
  //$("#" + this.id).find("#xian_shi_shun_xu").val(this.order);
  this.description = xmlValue.getAttribute('description');
  document.querySelector('#' + this.id).querySelector('#miao_shu').value = this.description;
  //$("#" + this.id).find("#miao_shu").val(this.description);
  this.initJBXX(); // 初始化基本信息
  /*****以上是公共的*******/
  /*****以下是公共的*******/
  rootXml.appendChild(this.createMxCell(tagId, xmlValue)); // 创建mxCell

  // 观察全局变量变化
  this.observeGlobalVariable();
  // 路由条件
  this.initEvent();
  // 回写路由条件
  this.createConditionDom('zhi_xing_fang_shi', 'table-executor', xmlValue);
  // 初始化流转事件
  this.nodeEvent = new NodeEvent(this.designerEditor, this.id, 'avic-digital-human-task-event');
  // 回写流转事件
  this.nodeEvent.setEventDom(xmlValue);
  //回写超时流转
  this.createTimeOutDom(xmlValue);
};
/**
 * 组装processXML的自定义信息
 *
 * @param node
 */
MyTransition.prototype.getOtherAttr = function (node) {
  this.putAttr('to', this.designerEditor.myCellMap.get(this.cell.target.id).name, node);
  /*其他*/

  this.putAttr(
    'order',
    trim(document.querySelector('#' + this.id).querySelector('#xian_shi_shun_xu').value),
    node
  );
  this.putAttr(
    'description',
    trim(document.querySelector('#' + this.id).querySelector('#miao_shu').value),
    node
  );
  // 获取路由条件xml
  this.setConditionXml('zhi_xing_fang_shi', 'table-executor', node);
  // 获取流转事件xml
  this.nodeEvent.setEventXml(node, 'event');
  //超时流转xml
  this.saveTimeOutXml(node);
};
/**
 * 重写name监听事件，不需要往连线上写值
 *
 * @param value
 */
MyTransition.prototype.labelChanged = function (value) {
  this.alias = value;
};
/**
 * 构建mxCell
 */
MyTransition.prototype.createMxCell = function (tagId, xmlValue) {
  //流程图的处理
  let targetName = this.name.split(' ')[1];
  let targetNode = this.designerEditor.myLoadMap.get(targetName);
  if (
    targetNode.tagName == 'fork' ||
    targetNode.tagName == 'join' ||
    targetNode.tagName == 'foreach'
  ) {
    this.designerEditor.myCellMap.get(tagId).addForkJoin(targetNode.id);
  }

  if (this.designerEditor.useNewDesigner) {
    if (bpmUtils.notNull(xmlValue.firstElementChild)) {
      return xmlValue.firstElementChild;
    } else {
      return xmlValue.firstChild;
    }
  }
  let mxCell = bpmUtils.createElement('mxCell');
  let mxGeometry = bpmUtils.createElement('mxGeometry');
  mxCell.setAttribute('id', this.id);
  mxCell.setAttribute('value', '');
  mxCell.setAttribute('edge', '1');
  mxCell.setAttribute('parent', '1');
  mxCell.setAttribute('source', tagId);
  mxCell.setAttribute('target', this.designerEditor.myLoadMap.get(targetName).id);

  mxGeometry.setAttribute('relative', '1');
  mxGeometry.setAttribute('as', 'geometry');

  //扩展，如果记录了连线位置，则给point
  let mxCellG = this.getmxCellG();
  if (bpmUtils.notNull(mxCellG)) {
    let mxArray = bpmUtils.createElement('Array');
    mxArray.setAttribute('as', 'points');
    let mxPoint = bpmUtils.createElement('mxPoint');
    mxPoint.setAttribute('x', mxCellG.x);
    mxPoint.setAttribute('y', mxCellG.y);
    mxArray.appendChild(mxPoint);
    mxGeometry.appendChild(mxArray);
  }

  mxCell.appendChild(mxGeometry);
  return mxCell;
};
/**** 	启动条件各种事件结束	 ****/
///<jscompress sourcefile="MyWs.js" />
/**
 * ws extends MyBase
 */
function MyWs(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'ws');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/task_webservice.png;';
}
MyWs.prototype = new MyBase();

///<jscompress sourcefile="MyRules.js" />
/**
 * rules extends MyBase
 */
function MyRules(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'rules');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/task_rule.png;';
}
MyRules.prototype = new MyBase();

///<jscompress sourcefile="MyText.js" />
/**
 * text extends MyBase
 */
function MyText(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'text');
}
MyText.prototype = new MyBase();

///<jscompress sourcefile="MyRest.js" />
/**
 * rest extends MyBase
 */
function MyRest(designerEditor, id) {
  MyBase.call(this, designerEditor, id, 'rest');
  this.style =
    'symbol;image=avicit/platform6/bpmreform/bpmdesigner/editors/images/48/task_rest.png;';
}
MyRest.prototype = new MyBase();

///<jscompress sourcefile="DesignerEditor.src.js" />
/**
 * 编辑器
 */
function DesignerEditor() {}
// 编辑器
DesignerEditor.prototype.editor = null;
DesignerEditor.prototype.setEditor = function (editor) {
  this.editor = editor;
};
DesignerEditor.prototype.setIconType = function (iconType) {
  _iconType = iconType;
};
// 定义存储数据的map对象
DesignerEditor.prototype.myCellMap = null;
// 流程key
DesignerEditor.prototype.processKey = null;
// 临时map，按名称记录对象
DesignerEditor.prototype.myLoadMap = null;
// 自增序列
DesignerEditor.prototype.countUtils = new CountUtils();
// 当前对象ID
DesignerEditor.prototype.myCurCellId = null;
// 全局属性区域
DesignerEditor.prototype.homeDiv = null;
// 节点属性区域
DesignerEditor.prototype.profileDiv = null;
// controller
DesignerEditor.prototype.controlPath = 'platform/bpm/designer/';
// 是否是新的设计器编辑的流程，意味着加载解析方式的不同
DesignerEditor.prototype.useNewDesigner = false;
// 横向泳道
DesignerEditor.prototype.swimlane1 = 0;
// 纵向泳道
DesignerEditor.prototype.swimlane2 = 0;
// 校验后是否允许保存
DesignerEditor.prototype.allowSave = true;
//是否自由流程
DesignerEditor.prototype.isUflow = false;

/**
 * 创建元素对象
 */
DesignerEditor.prototype.createBaseByTagName = function (tagName, id, vertex, edge) {
  let baseNode = null;
  if (vertex) {
    if (!this.myCellMap) {
      this.myCellMap = new Map();
    }
    if (!this.myLoadMap) {
      this.myLoadMap = new Map();
    }
    if (tagName == 'start') {
      baseNode = new MyStart(this, id);
    } else if (tagName == 'end') {
      baseNode = new MyEnd(this, id);
    } else if (tagName == 'task') {
      baseNode = new MyTask(this, id);
    } else if (tagName == 'java') {
      baseNode = new MyJava(this, id);
    } else if (tagName == 'sql') {
      baseNode = new MySql(this, id);
    } else if (tagName == 'ws') {
      baseNode = new MyWs(this, id);
    } else if (tagName == 'state') {
      baseNode = new MyState(this, id);
    } else if (tagName == 'fork') {
      baseNode = new MyFork(this, id);
    } else if (tagName == 'join') {
      baseNode = new MyJoin(this, id);
    } else if (tagName == 'decision') {
      baseNode = new MyExclusive(this, id);
    } else if (tagName == 'sub-process') {
      baseNode = new MySubprocess(this, id);
    } else if (tagName == 'foreach') {
      baseNode = new MyForeach(this, id);
    } else if (tagName == 'jms') {
      baseNode = new MyJms(this, id);
    } else if (tagName == 'custom') {
      baseNode = new MyCustom(this, id);
    } else if (tagName == 'rules') {
      baseNode = new MyRules(this, id);
    } else if (tagName == 'swimlane') {
      baseNode = new MySwimlane(this, id);
    } else if (tagName == 'text') {
      baseNode = new MyText(this, id);
    } else if (tagName == 'rest') {
      baseNode = new MyRest(this, id);
    }
  } else if (edge) {
    baseNode = new MyTransition(this, id);
  }
  return baseNode;
};
/**
 * 判断是否横向泳道
 */
DesignerEditor.prototype.isSwimlane1 = function (cell) {
  return cell.getAttribute('cellType') == 'swimlane1';
};

/**
 * 添加元件事件
 *
 * @param cells
 */
DesignerEditor.prototype.addCell = function (cell) {
  if (!this.myCellMap.containsKey(cell.id)) {
    let nodeBase = this.createBaseByTagName(cell.value.nodeName, cell.id, cell.vertex, cell.edge);
    nodeBase.init();
    this.myCellMap.put(cell.id, nodeBase);
  }
};
/**
 * 删除元件事件
 *
 * @param cells
 */
DesignerEditor.prototype.removeCells = function (cells) {
  for (let i = 0; i < cells.length; i++) {
    let cell = cells[i];
    this.myCellMap.get(cell.id).remove();
    this.myCellMap.remove(cell.id);
    if (cell.value.nodeName == 'swimlane' && cell.children != null) {
      this.removeCells(cell.children);
    }
  }
};
/**
 * 双击元件事件
 *
 * @param cells
 */
DesignerEditor.prototype.doubleClickCell = function (cell) {
  if (bpmUtils.notNull(cell)) {
    if (cell.value.nodeName == 'sub-process') {
      this.myCellMap.get(cell.id).doubleClickCell();
    }
  }
};

/**
 * 循环各个元素进行转换
 */
DesignerEditor.prototype.cycleTags = function (tagName, process, rootXml, transitionArr) {
  let tags = process.getElementsByTagName(tagName);
  for (let i = 0; i < tags.length; i++) {
    if (tagName == 'text' && !bpmUtils.notNull(tags[i].getAttribute('id'))) {
      continue;
    }
    let tagId = this.countUtils.getSelfId();
    if (this.useNewDesigner) {
      tagId = tags[i].getAttribute('id');
      this.countUtils.setSelfId(Number(tagId));
    }
    let tagNode = this.createBaseByTagName(tagName, tagId, 1, 0);
    tagNode.initTracks(tags[i], rootXml);
    this.myCellMap.put(tagId, tagNode);
    let transitions = tags[i].getElementsByTagName('transition');
    for (let j = 0; j < transitions.length; j++) {
      let transitionObject = {};
      transitionObject.xml = transitions[j];
      transitionObject.sourceId = tagId;
      transitionArr.push(transitionObject);
    }
  }
};
/**
 * 循环各个元素进行转换
 */
DesignerEditor.prototype.converToGraphXml = function (data) {
  // process
  let process = data.getElementsByTagName('process')[0];
  this.useNewDesigner = 'true' == process.getAttribute('useNewDesigner') ? true : false;

  let graphXml = bpmUtils.createElement('mxGraphModel');
  let rootXml = bpmUtils.createElement('root');
  let workflowXml = bpmUtils.createElement('Workflow');
  workflowXml.setAttribute('id', '0');
  let mxCell0 = bpmUtils.createElement('mxCell');
  workflowXml.appendChild(mxCell0);
  let layerXml = bpmUtils.createElement('Layer');
  layerXml.setAttribute('id', '1');
  let mxCell1 = bpmUtils.createElement('mxCell');
  mxCell1.setAttribute('parent', '0');
  layerXml.appendChild(mxCell1);
  rootXml.appendChild(workflowXml);
  rootXml.appendChild(layerXml);

  let transitionArr = new Array(); // 先循环节点，连线只是存储，最后再执行连线初始化，因为连线需要寻找target，即目标节点
  this.cycleTags('swimlane', process, rootXml, transitionArr);
  this.cycleTags('start', process, rootXml, transitionArr);
  this.cycleTags('end', process, rootXml, transitionArr);
  this.cycleTags('task', process, rootXml, transitionArr);
  this.cycleTags('java', process, rootXml, transitionArr);
  this.cycleTags('sql', process, rootXml, transitionArr);
  this.cycleTags('ws', process, rootXml, transitionArr);
  this.cycleTags('state', process, rootXml, transitionArr);
  this.cycleTags('fork', process, rootXml, transitionArr);
  this.cycleTags('join', process, rootXml, transitionArr);
  this.cycleTags('decision', process, rootXml, transitionArr);
  this.cycleTags('sub-process', process, rootXml, transitionArr);
  this.cycleTags('foreach', process, rootXml, transitionArr);
  this.cycleTags('jms', process, rootXml, transitionArr);
  this.cycleTags('custom', process, rootXml, transitionArr);
  this.cycleTags('rules', process, rootXml, transitionArr);
  this.cycleTags('rest', process, rootXml, transitionArr);
  this.cycleTags('text', process, rootXml, transitionArr);
  for (let i = 0; i < transitionArr.length; i++) {
    let transitionId = this.countUtils.getSelfId();
    if (this.useNewDesigner) {
      transitionId = transitionArr[i].xml.getAttribute('_id');
      this.countUtils.setSelfId(Number(transitionId));
    }
    let transitionNode = new MyTransition(this, transitionId);
    transitionNode.initTracks(transitionArr[i].xml, rootXml, transitionArr[i].sourceId);
    this.myCellMap.put(transitionId, transitionNode);
  }

  graphXml.appendChild(rootXml);
  return graphXml;
};

DesignerEditor.prototype.validMoveCells = function (cells, dx, dy, clone, target, evt, mapping) {
  if (target != null && target.value.nodeName == 'swimlane') {
    for (let i = 0; i < cells.length; i++) {
      if (cells[i].value.nodeName == 'swimlane') {
        console.warn('泳道内不能放置泳道');
        return false;
      }
    }
  }
  return true;
};
/**
 * 自定义undo操作，只undo移动节点，出现添加或删除节点，则clear
 * @param manager
 * @param undoableEdit
 * @returns {Boolean}
 */
DesignerEditor.prototype.validUndoCell = function (manager, undoableEdit) {
  for (let i = 0; i < undoableEdit.changes.length; i++) {
    // eslint-disable-next-line no-undef
    if (undoableEdit.changes[i].constructor != mxGeometryChange) {
      manager.clear();
      return false;
    }
  }
  return true;
};
DesignerEditor.prototype.goLeft = function (editor) {
  let cells = editor.graph.getSelectionCells();
  if (bpmUtils.notNull(cells)) {
    editor.graph.moveCells(cells, -1, 0);
  }
};
DesignerEditor.prototype.goUp = function (editor) {
  let cells = editor.graph.getSelectionCells();
  if (bpmUtils.notNull(cells)) {
    editor.graph.moveCells(cells, 0, -1);
  }
};
DesignerEditor.prototype.goRight = function (editor) {
  let cells = editor.graph.getSelectionCells();
  if (bpmUtils.notNull(cells)) {
    editor.graph.moveCells(cells, 1, 0);
  }
};
DesignerEditor.prototype.goDown = function (editor) {
  let cells = editor.graph.getSelectionCells();
  if (bpmUtils.notNull(cells)) {
    editor.graph.moveCells(cells, 0, 1);
  }
};
DesignerEditor.prototype.autoLayout = function (editor) {
  let cells = editor.graph.getChildVertices();
  if (bpmUtils.notNull(cells)) {
    let o = {};
    let vCells = new Map();
    if (this.swimlane1 > 0) {
      for (let i = 0; i < cells.length; i++) {
        if (cells[i].vertex && cells[i].value.nodeName == 'swimlane') {
          let y = Number(cells[i].getGeometry().y);
          vCells.put(y, cells[i]);
          if (!bpmUtils.notNull(o.miny) || o.miny > y) {
            o.miny = y;
          }
          if (!bpmUtils.notNull(o.maxy) || o.maxy < y) {
            o.maxy = y;
          }
        }
      }
      if (vCells.size() > 1) {
        let keys = vCells.keys();
        keys = keys.sort(function (a, b) {
          return a - b;
        });
        let width = vCells.get(keys[0]).getGeometry().width;
        let x = vCells.get(keys[0]).getGeometry().x;
        let h = 0;
        for (let i = 0; i < keys.length; i++) {
          vCells.get(keys[i]).getGeometry().width = width;
          editor.graph.moveCells([vCells.get(keys[i])], 1, 0);
          editor.graph.moveCells(
            [vCells.get(keys[i])],
            x - vCells.get(keys[i]).getGeometry().x,
            o.miny + h - Number(keys[i])
          );
          h += vCells.get(keys[i]).getGeometry().height;
          vCells.remove(keys[i]);
        }
      }
    } else {
      for (let i = 0; i < cells.length; i++) {
        if (cells[i].vertex && cells[i].value.nodeName == 'swimlane') {
          let x = Number(cells[i].getGeometry().x);
          vCells.put(x, cells[i]);
          if (!bpmUtils.notNull(o.minx) || o.minx > x) {
            o.minx = x;
          }
          if (!bpmUtils.notNull(o.maxx) || o.maxx < x) {
            o.maxx = x;
          }
        }
      }
      if (vCells.size() > 1) {
        let keys = vCells.keys();
        keys = keys.sort(function (a, b) {
          return a - b;
        });
        let height = vCells.get(keys[0]).getGeometry().height;
        let y = vCells.get(keys[0]).getGeometry().y;
        let w = 0;
        for (let i = 0; i < keys.length; i++) {
          vCells.get(keys[i]).getGeometry().height = height;
          editor.graph.moveCells([vCells.get(keys[i])], 0, 1);
          editor.graph.moveCells(
            [vCells.get(keys[i])],
            o.minx + w - Number(keys[i]),
            y - vCells.get(keys[i]).getGeometry().y
          );
          w += vCells.get(keys[i]).getGeometry().width;
          vCells.remove(keys[i]);
        }
      }
    }
  }
};
DesignerEditor.prototype.comWidth = function (editor) {
  let cells = editor.graph.getSelectionCells();
  if (bpmUtils.notNull(cells)) {
    let o = {};
    let vCells = new Map();
    for (let i = 0; i < cells.length; i++) {
      if (cells[i].vertex && cells[i].value.nodeName != 'swimlane') {
        let x = Number(cells[i].getGeometry().x);
        vCells.put(x, cells[i]);
        if (!bpmUtils.notNull(o.minx) || o.minx > x) {
          o.minx = x;
        }
        if (!bpmUtils.notNull(o.maxx) || o.maxx < x) {
          o.maxx = x;
        }
      }
    }
    if (vCells.size() > 1) {
      let w = (o.maxx - o.minx) / (vCells.size() - 1);
      let keys = vCells.keys();
      keys = keys.sort(function (a, b) {
        return a - b;
      });
      for (let i = 0; i < keys.length; i++) {
        editor.graph.moveCells([vCells.get(keys[i])], o.minx + w * i - Number(keys[i]), 0);
        vCells.remove(keys[i]);
      }
    }
  }
};
DesignerEditor.prototype.comHeight = function (editor) {
  let cells = editor.graph.getSelectionCells();
  if (bpmUtils.notNull(cells)) {
    let o = {};
    let vCells = new Map();
    for (let i = 0; i < cells.length; i++) {
      if (cells[i].vertex && cells[i].value.nodeName != 'swimlane') {
        let y = Number(cells[i].getGeometry().y);
        vCells.put(y, cells[i]);
        if (!bpmUtils.notNull(o.miny) || o.miny > y) {
          o.miny = y;
        }
        if (!bpmUtils.notNull(o.maxy) || o.maxy < y) {
          o.maxy = y;
        }
      }
    }
    if (vCells.size() > 1) {
      let h = (o.maxy - o.miny) / (vCells.size() - 1);
      let keys = vCells.keys();
      keys = keys.sort(function (a, b) {
        return a - b;
      });
      for (let i = 0; i < keys.length; i++) {
        editor.graph.moveCells([vCells.get(keys[i])], 0, o.miny + h * i - Number(keys[i]));
        vCells.remove(keys[i]);
      }
    }
  }
};
/**
 * 重写的mxUtils中的方法，因为原方法在处理text类型时会多加一个空格
 *
 * @param node
 * @param tab
 * @param indent
 * @returns
 */
DesignerEditor.prototype.getPrettyXml = function (node, tab, indent) {
  let result = [];

  if (node != null) {
    tab = tab || '  ';
    indent = indent || '';

    // eslint-disable-next-line no-undef
    if (node.nodeType == mxConstants.NODETYPE_TEXT) {
      result.push(node.nodeValue);
    } else {
      result.push(indent + '<' + node.nodeName);

      // Creates the string with the node attributes
      // and converts all HTML entities in the values
      let attrs = node.attributes;

      if (attrs != null) {
        for (let i = 0; i < attrs.length; i++) {
          // eslint-disable-next-line no-undef
          let val = mxUtils.htmlEntities(attrs[i].nodeValue);
          result.push(' ' + attrs[i].nodeName + '="' + val + '"');
        }
      }

      // Recursively creates the XML string for each
      // child nodes and appends it here with an
      // indentation
      let tmp = node.firstChild;

      if (tmp != null) {
        let temIndent = '';
        result.push('>');
        // eslint-disable-next-line no-undef
        if (tmp.nodeType != mxConstants.NODETYPE_TEXT) {
          temIndent = indent;
          result.push('\n');
        }

        while (tmp != null) {
          result.push(this.getPrettyXml(tmp, tab, indent + tab));
          tmp = tmp.nextSibling;
        }

        result.push(temIndent + '</' + node.nodeName + '>\n');
      } else {
        result.push('/>\n');
      }
    }
  }

  return result.join('');
};

/**
 * Created by xb on 2017/5/25.
 */

/**
 * 选择表单
 * @param fieldId       页面隐藏域，用于存储表单id
 * @param fieldName     页面显示域，用于存储表单name
 * @param parentAreaId  可选参数；当同一页面存在多处选择表单时，用于传参父区域唯一标识
 * @param isBpm         可选参数Y/N；是否走流程表单
 * @param formsType     可选参数bpm/eform；bpm为流程添加表单、eform为设计器设计表单
 * @constructor
 */
function SelectPublishEform(fieldId, fieldName, parentAreaId, isBpm, formsType) {
  this.fieldId = fieldId;
  this.fieldName = fieldName;
  this.parentAreaId = parentAreaId;
  this.isBpm = isBpm;
  this.formsType = formsType;
}
SelectPublishEform.prototype = new SelectPublishEform();

function NodeEvent(designerEditor, id, eventFlagClass) {
  this.designerEditor = designerEditor;
  this.id = id;
  // 事件包围器
  // this.wrapper = eventFlagClass ? document.querySelector("#"+id+",."+eventFlagClass) : document.querySelector("#"+id+",avic-digital-flow-event");
  // 输入区域表格事件
  // this.constantsTable = this.wrapper.find("table[name=constants]");
  this.isModified = false;
  //this.init();
  return this;
}
NodeEvent.prototype = new NodeEvent();

function BpmDocUploader(option) {
  return this;
}
BpmDocUploader.prototype = new BpmDocUploader();

DesignerEditor.prototype.allEdges = [];

DesignerEditor.prototype.draw = function (obj, entryId) {
  let _self = this;
  let subprocess = obj.subprocess;
  let userInfo = obj.userInfo;
  let currentArray = obj.redsquare;
  if (currentArray != null && currentArray.length > 0) {
    each(currentArray, function (i, xywh) {
      let array = xywh.split(',');
      let name = array[4];
      let node = _self.myLoadMap.get(name);
      node.isCurrent = 'true';
      let cell = node.getCell();
      cell.showTooltipName = name;
      if (_iconType == '1') {
        //新图元
        var newStyle = _self.editor.graph.model
          .getStyle(cell)
          .replace('/bpmn-img-v1/default-icon/', '/bpmn-img-v1/run-icon/');
        _self.editor.graph.model.setStyle(cell, newStyle);
      } else {
        if (node.tagName == 'task') {
          var newStyle = _self.editor.graph.model
            .getStyle(cell)
            .replace(
              '/bpmn-img-v2/norun-icon/task_human.png',
              '/bpmn-img-v2/run-icon/task_human.gif'
            );
          _self.editor.graph.model.setStyle(cell, newStyle);
        } else if (node.tagName == 'state') {
          var newStyle = _self.editor.graph.model
            .getStyle(cell)
            .replace(
              '/bpmn-img-v2/norun-icon/task_wait.png',
              '/bpmn-img-v2/run-icon/task_wait.gif'
            );
          _self.editor.graph.model.setStyle(cell, newStyle);
        } else if (node.tagName == 'sub-process') {
          var newStyle = _self.editor.graph.model
            .getStyle(cell)
            .replace(
              '/bpmn-img-v2/norun-icon/task_subprocess.png',
              '/bpmn-img-v2/run-icon/task_subprocess.gif'
            );
          _self.editor.graph.model.setStyle(cell, newStyle);
        }

        //_self.editor.graph.setCellStyles("imageBorder", "red", [cell]);
      }
      //			if(name.indexOf("Subprocess") != -1){
      //				node.actNodeClick = function() {
      //					$.ajax({
      //						type : "POST",
      //						data : {
      //							entryId : entryId,
      //							subprocess : name
      //						},
      //						url : "platform/bpm/business/getSubprocess",
      //						dataType : "text",
      //						success : function(result) {
      //							if(bpmUtils.bpmUtils.notNull(result)){
      //								var dialog = layer.open({
      //									type : 2,
      //									title: "子流程",
      //									area : [ "800px", "450px" ],
      //									content : "avicit/platform6/bpmreform/bpmdesigner/picture/picAndTracks.jsp?entryId=" + result
      //								});
      //								layer.full(dialog);
      //							}else{
      //								bpmUtils.warning("数据错误！");
      //							}
      //						}
      //					});
      //				};
      //			}
      let _list = eval('subprocess.' + name);
      if (bpmUtils.notNull(_list)) {
        node.actNodeClick = function () {
          let list = eval('subprocess.' + name);
          _self.clickNode(list);
        };
      }
      let list = eval('subprocess.' + name);
      if (bpmUtils.notNull(list)) {
        var newStyle = _self.editor.graph.model
          .getStyle(cell)
          .replace('/task_human.png', '/task_human_sub.png');
        _self.editor.graph.model.setStyle(cell, newStyle);
      }

      //节点旁边显示处理人
      if (bpmUtils.notNull(userInfo)) {
        let userText = eval('userInfo.' + node.name);
        let reasonableFinishTime = eval('userInfo.' + node.name + '_reasonableFinishTime');
        if (bpmUtils.notNull(userText) && _self && _self.drawUserText) {
          _self.drawUserText(cell, userText, reasonableFinishTime);
        }
      }
    });
  }

  let histArray = obj.greensign;
  if (histArray != null && histArray.length > 0) {
    each(histArray, function (i, xywh) {
      var xywh = histArray[i];
      let array = xywh.split(',');
      let name = array[4];
      let node = _self.myLoadMap.get(name);
      node.isHistory = 'true';
      let cell = node.getCell();
      cell.showTooltipName = name;
      cell.isHistory = 'true';
      if (_iconType == '1') {
        //新图元
        var newStyle = _self.editor.graph.model
          .getStyle(cell)
          .replace('/bpmn-img-v1/default-icon/', '/bpmn-img-v1/over-icon/');
        _self.editor.graph.model.setStyle(cell, newStyle);
      } else {
        /**
				 * var id = _self.countUtils.getSelfId();
				var vertex = _self.editor.graph.createVertex(cell.getParent(), id, "", cell.getGeometry().x + 20, cell.getGeometry().y + 17, 16, 16, "symbol;image=/assets/bpm/images/tick.png;");
				vertex.pid = cell.id;
				_self.editor.graph.addCell(vertex, cell.getParent());
				vertex.showTooltipName = name;
				 */

        var newStyle = _self.editor.graph.model
          .getStyle(cell)
          .replace('/bpmn-img-v2/norun-icon/', '/bpmn-img-v2/over-icon/');
        _self.editor.graph.model.setStyle(cell, newStyle);
      }
      //			if(name.indexOf("Subprocess") != -1){
      //				node.actNodeClick = function() {
      //					$.ajax({
      //						type : "POST",
      //						data : {
      //							entryId : entryId,
      //							subprocess : name
      //						},
      //						url : "platform/bpm/business/getSubprocess",
      //						dataType : "text",
      //						success : function(result) {
      //							if(bpmUtils.bpmUtils.notNull(result)){
      //								var dialog = layer.open({
      //									type : 2,
      //									title: "子流程",
      //									area : [ "1000px", "500px" ],
      //									content : "avicit/platform6/bpmreform/bpmdesigner/picture/picAndTracks.jsp?entryId=" + result
      //								});
      //								layer.full(dialog);
      //							}else{
      //								bpmUtils.warning("数据错误！");
      //							}
      //						}
      //					});
      //				};
      //			}

      let _list = eval('subprocess.' + name);
      if (bpmUtils.notNull(_list)) {
        node.actNodeClick = function () {
          let list = eval('subprocess.' + name);
          _self.clickNode(list);
        };
      }
      let list = eval('subprocess.' + name);
      if (bpmUtils.notNull(list)) {
        var newStyle = _self.editor.graph.model
          .getStyle(cell)
          .replace('/task_human.png', '/task_human_sub.png');
        _self.editor.graph.model.setStyle(cell, newStyle);
      }

      //节点旁边显示处理人
      if (bpmUtils.notNull(userInfo)) {
        let userText = eval('userInfo.' + node.name);
        let endTime = eval('userInfo.' + node.name + '_endTime');
        if (bpmUtils.notNull(userText) && _self && _self.drawUserText) {
          _self.drawUserText(cell, userText, endTime);
        }
      }
    });
  }
  let lineArray = obj.redline;
  if (lineArray != null && lineArray.length > 0) {
    let lastRetreatCell = null;
    for (let i = 0; i < lineArray.length; i++) {
      let xywh = lineArray[i];
      let array = xywh.split(',');
      let start = array[4];
      let end = array[5];
      if (bpmUtils.notNull(start, end)) {
        let startNode = this.myLoadMap.get(start);
        let startCell = startNode.getCell();
        let endCell = this.myLoadMap.get(end).getCell();
        let edgeArr = this.editor.graph.getEdgesBetween(startCell, endCell, true);
        if (edgeArr.length > 0) {
          if (this.allEdges) {
            this.allEdges = this.allEdges.concat(edgeArr);
          } else {
            this.allEdges = edgeArr;
          }
        } else if (startNode && startNode.hasForkJoin()) {
          let forkJoinCell = null;
          var forkJoinNode = null;
          for (let j = 0; j < startNode.forkJoinArr.length; j++) {
            let forkJoinId = startNode.forkJoinArr[j];
            var forkJoinNode = this.myCellMap.get(forkJoinId);
            forkJoinCell = forkJoinNode.getCell();
            let tempEdgeArr = this.editor.graph.getEdgesBetween(forkJoinCell, endCell, true);
            if (tempEdgeArr.length > 0) {
              break;
            }
            forkJoinCell = null;
            forkJoinNode = null;
          }
          if (forkJoinNode == null) {
            continue;
          }
          forkJoinNode.isHistory = 'true';
          if (_iconType == '1') {
            //新图元
            var newStyle = this.editor.graph.model
              .getStyle(forkJoinCell)
              .replace('/bpmn-img-v1/default-icon/', '/bpmn-img-v1/over-icon/');
            this.editor.graph.model.setStyle(forkJoinCell, newStyle);
          }
          edgeArr = this.editor.graph.getEdgesBetween(forkJoinCell, endCell, true);
          if (edgeArr.length > 0) {
            /**
						 * if(this.allEdges){
							this.allEdges = this.allEdges.concat(edgeArr);
						}else{
							this.allEdges = edgeArr;
						}
						edgeArr = this.editor.graph.getEdgesBetween(startCell, forkJoinCell, true);
						if(this.allEdges){
							this.allEdges = this.allEdges.concat(edgeArr);
						}else{
							this.allEdges = edgeArr;
						}
						*/
            this.allEdges = this.allEdges.concat(edgeArr);
            edgeArr = this.editor.graph.getEdgesBetween(startCell, forkJoinCell, true);
            this.allEdges = this.allEdges.concat(edgeArr);
            var newStyle = _self.editor.graph.model
              .getStyle(forkJoinCell)
              .replace(
                '/bpmn-img-v2/norun-icon/gateway_fork.png',
                '/bpmn-img-v2/default-icon/gateway_fork.png'
              );
            newStyle = _self.editor.graph.model
              .getStyle(forkJoinCell)
              .replace(
                '/bpmn-img-v2/norun-icon/gateway_join.png',
                '/bpmn-img-v2/default-icon/gateway_join.png'
              );
            _self.editor.graph.model.setStyle(forkJoinCell, newStyle);
          } else {
            if (_iconType == '1') {
              //新图元，不要线
              continue;
            }
            var newStyle = this.editor.graph.model
              .getStyle(startCell)
              .replace('/task_human.png', '/task_human_back.png');
            this.editor.graph.model.setStyle(startCell, newStyle);
            /**
						 * 
						 
						debugger;
						var jumpLine = this.addLine(startCell, endCell);
						this.editor.graph.setCellStyles("strokeColor", "red", [jumpLine]);
						*/
            //this.allEdges.push(jumpLine);
          }
        } else {
          if (edgeArr.length > 0) {
            continue;
          }
          if (_iconType == '1') {
            //新图元，不要线
            continue;
          }
          lastRetreatCell = startCell;
          /** 
					var jumpLine = this.addLine(startCell, endCell);
					this.editor.graph.setCellStyles("strokeColor", "red", [jumpLine]);
					*/
          //this.allEdges.push(jumpLine);
        }
      }
    }
    if (_iconType == '1') {
      //新图元，不要线变色
    } else {
      /**try{
				this.editor.graph.setCellStyles("strokeColor", "blue", this.allEdges);
			}catch(e){
				console.log(e);
			}
			*/

      if (lastRetreatCell) {
        var newStyle = this.editor.graph.model
          .getStyle(lastRetreatCell)
          .replace('/task_human.png', '/task_human_back.png');
        this.editor.graph.model.setStyle(lastRetreatCell, newStyle);
      }
      this.editor.graph.setCellStyles('strokeWidth', '2', this.allEdges);
      this.editor.graph.setCellStyles('strokeColor', '#2a93ff', this.allEdges);

      //			this.editor.graph.removeCells(this.allEdges);
      //			this.editor.graph.addCells(this.allEdges, this.editor.graph.getDefaultParent());
    }
  }
};
DesignerEditor.prototype.addLine = function (startCell, endCell) {
  let id = this.countUtils.getSelfId();
  this.editor.graph.insertEdge(
    this.editor.graph.getDefaultParent(),
    id,
    '',
    startCell,
    endCell,
    ''
  );
  let jumpLine = this.editor.graph.getModel().getCell(id);
  let state = this.editor.graph.view.getState(jumpLine);
  let handler = this.editor.graph.createHandler(state);

  let geo = this.editor.graph.getCellGeometry(state.cell);
  if (geo != null) {
    geo = geo.clone();
    let pt = this.getPoint(startCell, endCell);
    let index = mxUtils.findNearestSegment(state, pt.x, pt.y);
    handler.convertPoint(pt, true);
    if (geo.points == null) {
      geo.points = [pt];
    } else {
      geo.points.splice(index, 0, pt);
    }
    handler.graph.getModel().setGeometry(state.cell, geo);
    handler.destroy();
  }
  return jumpLine;
};
DesignerEditor.prototype.getPoint = function (start, end) {
  let startX = 0;
  let startY = 0;
  if (start.parent.id != '1') {
    startX = start.parent.getGeometry().x;
    startY = start.parent.getGeometry().y;
  }
  let x1 = startX + start.getGeometry().x;
  let y1 = startY + start.getGeometry().y;

  let endX = 0;
  let endY = 0;
  if (end.parent.id != '1') {
    endX = end.parent.getGeometry().x;
    endY = end.parent.getGeometry().y;
  }
  let x2 = endX + end.getGeometry().x;
  let y2 = endY + end.getGeometry().y;

  x1 = x1 + start.getGeometry().width / 2;
  y1 = y1 + start.getGeometry().height / 2;

  x2 = x2 + end.getGeometry().width / 2;
  y2 = y2 + end.getGeometry().height / 2;

  let x, y;
  if (Math.abs(y1 - y2) < 60) {
    x = x2 > x1 ? x2 - Math.abs(x1 - x2) / 2 : x2 + Math.abs(x1 - x2) / 2;
    if (y1 < y2) {
      y = y1 - 20;
    } else {
      y = y2 - 20;
    }
  } else {
    y = y2 > y1 ? y2 - Math.abs(y1 - y2) / 2 : y2 + Math.abs(y1 - y2) / 2;
    if (x1 > x2) {
      x = x1 + (start.getGeometry().width + 20);
    } else {
      x = x2 + (end.getGeometry().width + 20);
    }
  }
  x = x < 1 ? 1 : x;
  y = y < 1 ? 1 : y;
  let top = offset(this.editor.graph.container).top;
  let pt = mxUtils.convertPoint(this.editor.graph.container, x, y + top);
  return pt;
};
mxEditor.prototype.setGraphContainer = function (a) {
  null == this.graph.container &&
    (this.graph.init(a),
    (this.rubberband = new mxRubberband(this.graph)),
    this.disableContextMenu && mxEvent.disableContextMenu(a),
    mxClient.IS_QUIRKS && new mxDivResizer(a));
};

export default DesignerEditor;
