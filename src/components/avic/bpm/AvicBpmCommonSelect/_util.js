/**
 * 判断是否自动选人
 * @param {*} event
 */
export function isAutoSelectUser(event) {
  if (
    event == 'doretreattodraft' ||
    event == 'doretreattoprev' ||
    event == 'dowithdraw' ||
    event == 'doretreattowant' ||
    event == 'doretreattoactivity'
  ) {
    return true;
  }
  return false;
}

//设置用户 页签数据
export function setUserList(list, treecheckable = true) {
  let userList = [];
  if (list) {
    list.map(user => {
      userList.push({
        key: user.id,
        attributes: {
          deptId: user.deptId,
          deptName: user.deptName,
          nodeType: 'user'
        },
        nodeType: 'user',
        title: user.name,
        deptId: user.deptId,
        deptName: user.deptName,
        givenName: user.givenName,
        isLeaf: true,
        children: null,
        id: user.id,
        name: user.name,
        orderBy: user.orderBy,
        disableCheckbox: false,
        disabled: false,
        checked: false
      });
    });
  }
  return userList;
}
