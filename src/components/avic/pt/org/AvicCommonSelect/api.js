import { httpAction } from '@/api/manage';

//初始化加载用户或则部门
const getInitUserInfo = (userids = [], deptids = []) =>
  httpAction(
    '/api/appsys/common/select/getSelectedUserInfo/v1',
    userids,
    'POST',
    true,
    false,
    false
  );
//人员，部门默认选中
const getInitDeptInfo = (deptids = []) =>
  httpAction(
    '/api/appsys/common/select/getSelectedDeptInfo/v1',
    deptids,
    'POST',
    true,
    false,
    false
  );
//获取当前组织
const searchOrgName = (id = '', viewScope = '', defaultOrgId = '') =>
  httpAction(
    '/api/appsys/common/select/searchOrgName/v1',
    {
      id,
      defaultOrgId,
      viewScope
    },
    'POST',
    true,
    false,
    false
  );
//获取组织列表
const selectOrgName = (viewScope = '', defaultOrgId = '') =>
  httpAction(
    '/api/appsys/common/select/selectOrgName/v1',
    {
      viewScope,
      defaultOrgId
    },
    'POST',
    true,
    false,
    false
  );
//获取角色
const getInitRoleInfo = (
  roleUrl = '',
  roleMethodType = 'POST',
  searchText = '',
  viewScope = '',
  defaultOrgId = '',
  orgId = '',
  appId = '',
  rows = 20,
  page = 1,
  sord = 'asc'
) =>
  httpAction(
    roleUrl,
    {
      searchText,
      viewScope,
      defaultOrgId,
      orgId,
      appId,
      rows,
      page,
      sord
    },
    roleMethodType,
    true,
    false,
    false
  );
//获取群组
const getInitGroupInfo = (
  groupUrl = '',
  groupMethodType = 'POST',
  searchText = '',
  viewScope = '',
  defaultOrgId = '',
  orgId = '',
  appId = '',
  rows = 20,
  page = 1,
  sord = 'asc'
) =>
  httpAction(
    groupUrl,
    {
      searchText,
      viewScope,
      defaultOrgId,
      orgId,
      appId,
      rows,
      page,
      sord
    },
    groupMethodType,
    true,
    false,
    false
  );
//获取岗位
const getInitPositionInfo = (
  positionUrl = '',
  positionMethodType = 'POST',
  searchText = '',
  viewScope = '',
  defaultOrgId = '',
  orgId = '',
  appId = '',
  rows = 20,
  page = 1,
  sord = 'asc'
) =>
  httpAction(
    positionUrl,
    {
      searchText,
      viewScope,
      defaultOrgId,
      orgId,
      appId,
      rows,
      page,
      sord
    },
    positionMethodType,
    true,
    false,
    false
  );

export {
  getInitDeptInfo,
  searchOrgName,
  selectOrgName,
  getInitRoleInfo,
  getInitGroupInfo,
  getInitPositionInfo,
  getInitUserInfo
};
