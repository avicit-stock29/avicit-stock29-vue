import { formDataPretreatment } from '@utils/avic/common/formDataPretreatment';
import moment from 'moment';
import { mapGetters } from 'vuex';
export const BaseMixin = {
  name: 'BaseMixin',
  data() {
    return {
      showSearchForm: false, //高级查询form 表单内容的显隐控制
      showSearchTag: false, // 筛选条件的显隐控制
      advancedSearchParam: {}, //查询参数
      searchForm: this.$form.createForm(this),
      advRefreshedTableRefName: '', // 高级查询绑定的表格ref
      advancedTagParam: [], //筛选条件的参数
      initFormData: {} //form 表单预处理前的数据
    };
  },
  methods: {
    ...mapGetters(['userInfo']),
    moment,
    // 时间-开始时间的设置。开始时间大于结束时间
    disabledStartDate(startValue, dataindex) {
      const endValue = this.searchForm.getFieldValue(dataindex);
      if (!startValue || !endValue) {
        let year = moment().year() - 1;
        return startValue && startValue < moment().subtract(year, 'years');
      }
      return startValue.valueOf() > endValue.valueOf();
    },
    // 时间-结束时间的设置，结束时间小于开始时间
    disabledEndDate(endValue, dataindex) {
      const startValue = this.searchForm.getFieldValue(dataindex);
      if (!endValue || !startValue) {
        let year = moment().year() - 1;
        return endValue && endValue < moment().subtract(year, 'years');
      }
      return startValue.valueOf() >= endValue.valueOf();
    },
    // 筛选条件tag 清空筛选
    clearAdvanceSearch() {
      this.handleAdvancedSearchReset();
      this.showSearchForm = false;
      this.showSearchTag = false;
    },
    // 筛选条件 单项删除
    clearTagItem(e, itemDataIndex) {
      this.upDataAdvancedSearchParam(e, itemDataIndex);
    },
    /**
     * 点击高级查询按钮, 打开或者关闭高级查询
     * @param {String} refName 高级查询绑定的表格refName
     */
    handleAdvancedSearch(refName) {
      this.showSearchForm = !this.showSearchForm;
      this.showSearchTag = !this.showSearchForm;
      this.advRefreshedTableRefName = refName;
      this.$emit('onChange', this.showSearchForm, 'open');
    },
    // 点击查询
    handleAdvancedSearchConfirm() {
      // 高级查询 查询按钮 点击事件
      this.searchForm.validateFields((err, values) => {
        if (!err) {
          this.initFormData = JSON.parse(JSON.stringify(values));
          this.advancedSearchParam = formDataPretreatment(values, this.realAdvancedSearchFormData);
          this.setAdvancedTagParam(this.advancedSearchParam);
          this.$emit('search', {
            refName: this.advRefreshedTableRefName,
            param: {
              type: 'search',
              advancedSearchParam: this.advancedSearchParam, // 高级查询数据传给父组件
              isDeal: true // 标志位,重新请求表格数据
            }
          });
        }
      });
      this.showSearchForm = false;
      this.showSearchTag = !this.showSearchForm;
      this.$emit('onChange', this.showSearchForm, 'search');
    },
    // 点击重置
    handleAdvancedSearchReset() {
      // 高级查询 重置按钮 点击事件
      this.searchForm.resetFields(); // 清空form
      this.advancedSearchParam = {}; // 清空form序列化数据
      this.advancedTagParam = [];
      this.$emit('search', {
        refName: this.advRefreshedTableRefName,
        param: {
          type: 'reset',
          advancedSearchParam: this.advancedSearchParam, // 高级查询数据传给父组件
          isDeal: true // 标志位,重新请求表格数据
        }
      });
      this.$emit('onChange', this.showSearchForm, 'reset');
    },
    // 点击关闭
    handleAdvancedSearchClose() {
      this.showSearchForm = false;
      this.showSearchTag = true;
      this.$emit('onChange', this.showSearchForm, 'close');
    },
    //设置刷选条件的tab 数据
    setAdvancedTagParam(tagParam) {
      this.advancedTagParam = [];
      for (let key in tagParam) {
        let item = {};
        if (
          tagParam[key] !== undefined &&
          tagParam[key] !== '' &&
          tagParam[key] !== null &&
          tagParam[key] !== {} &&
          tagParam[key]['names'] !== '' &&
          tagParam[key] !== []
        ) {
          item = {
            dataIndex: key,
            value: tagParam[key]
          };
          this.realAdvancedSearchFormData.map(tim => {
            if (tim['dataIndex'] == key) {
              item['title'] = tim['title'];
              // 通用代码的value值转化为 lookupName
              if (tim['type'] === 'checkbox') {
                let arr = item.value.split(';');
                let itemValue = [];
                arr.map(el => {
                  let length = tim.list.length;
                  for (let i = 0; i < length; i++) {
                    if (tim.list[i].lookupCode == el) {
                      itemValue.push(tim.list[i].lookupName);
                    }
                  }
                });
                item.value = itemValue.join(';');
              } else if (tim.list && tim.list.length > 0) {
                let length = tim.list.length;
                for (let i = 0; i < length; i++) {
                  if (tim.list[i].lookupCode == item.value) {
                    item.value = tim.list[i].lookupName;
                  }
                }
              } else if (
                tim['type'] === 'date' ||
                tim['type'] === 'datetime' ||
                tim['type'] === 'datetimerange' ||
                tim['type'] === 'daterange'
              ) {
                item.value = moment(item.value).format(tim.format);
              } else if (tim['type'] === 'commonselect') {
                item.value = this.initFormData[key].names;
              }
            } else if (tim['dataIndex'] + 'Begin' == key) {
              item['title'] = tim['title'] + '开始日期';
              item.value = moment(item.value).format(tim.format);
            } else if (tim['dataIndex'] + 'End' == key) {
              item['title'] = tim['title'] + '结束日期';
              item.value = moment(item.value).format(tim.format);
            }
          });
          this.advancedTagParam.push(item);
        }
      }
    },
    //更新刷选条件的tab 数据
    upDataAdvancedSearchParam(e, key) {
      let item = {};
      item[key] = null;
      this.searchForm.setFieldsValue(item);
      this.handleAdvancedSearchConfirm(e);
    },
    // 查询按钮获取焦点
    setSearchFocus() {
      document.getElementById('$_searchBtn').focus();
    },
    // 隐藏高级查询内容区和tag 内容区
    hideAdvanceSearch() {
      this.showSearchTag = false;
      this.showSearchForm = false;
    },
    //页面刷新时，清空查询条件和tag 清除,并关闭高级查询
    clearSearchParam() {
      this.searchForm.resetFields(); // 清空form
      this.advancedSearchParam = {}; // 清空form序列化数据
      this.advancedTagParam = [];
      this.showSearchTag = false;
      this.showSearchForm = false;
    }
  }
};
