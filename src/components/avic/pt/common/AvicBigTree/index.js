import AvicBigTree from './index.vue';

AvicBigTree.install = function (Vue) {
  Vue.component(AvicBigTree.name, AvicBigTree);
};

export default AvicBigTree;
