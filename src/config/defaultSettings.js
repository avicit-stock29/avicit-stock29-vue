/**
 * 项目默认配置项
 * primaryColor - 默认主题色
 * navTheme - sidebar theme ['dark', 'light'] 两种主题
 * colorWeak - 色盲模式
 * layout - 整体布局方式 ['sidemenu', 'topmenu'] 两种布局
 * fixedHeader - 固定 Header : boolean
 * fixSiderbar - 固定左侧菜单栏 ： boolean
 * autoHideHeader - 向下滚动时，隐藏 Header : boolean
 * contentWidth - 内容区布局： 流式 |  固定
 * contentHeight - 内容区高度布局： 滚动 |  固定
 * topSideMulti - 横向导航溢出处理: 滚动条 | 左右箭头 | 点点点
 *
 * storageOptions: {} - Vue-ls 插件配置项 (localStorage/sessionStorage)
 *
 */
export default {
  systemTitle: '业务基础云平台',
  manageSystemDomain: 'http://117.36.76.70:31758', // 控制台--站点
  manageSystemUrl: '/platform/console/index', // 控制台--首页
  messageRequestInternal: null, // 消息请求时间间隔配置,为null就从接口取
  primaryColor: {
    name: 'blue-initial',
    skin: 'blue-initial',
    color: {
      '@primary-color': '#0e94cd' // 全局主色
    }
  },
  // navTheme: 'light', // theme for nav menu
  /* 主题风格：
    hplusmenu(h+风格)、
    traditionmenu(传统风格)、
    simplemenu(简约风格)、
    winmenu(Win风格), 
    暂停使用的：
    favmenu、
    topsidemenu
  */
  layout: 'hplusmenu',
  // contentWidth: 'Fixed', // layout of content: Fluid or Fixed, only works when layout is topmenu
  contentHeight: 'Auto', // 新主题暂不支持
  topSideMulti: 'Scroll', // 新主题暂不支持
  fixedHeader: true, // 新主题暂不支持
  fixSiderbar: true, // 新主题暂不支持
  autoHideHeader: false, //  新主题暂不支持
  colorWeak: false, //  新主题暂不支持
  multipage: false, //  新主题暂不支持
  tokenTime: 3 * 60 * 60 * 1000, // token默认有效时间3小时
  defaultPageSize: 20, // 表格翻页初始条目数
  tablePageSize: ['10', '20', '30', '50', '100', '200'], //表格翻页可选条目数
  // vue-ls options
  storageOptions: {
    namespace: 'pro__', // key prefix
    name: 'ls', // name variable Vue.[ls] or this.[$ls],
    storage: 'session' // storage name session, local
  },
  superAdmin: [
    // 拥有特定菜单权限的超管
    'admin',
    'iqs'
  ],
  // 首页配置
  homePageConfig: {
    path: '/dashboard',
    component: () => import('@/views/avic/pt/system/dashboard'),
    icon: 'home',
    title: '首页',
    name: 'dashboard'
  },
  // 无须登录路由白名单，从已注册的路由信息的path里取值填充数组
  whiteList: ['/user/login'],
  // http 基础配置文件
  http: {
    defaultContentType: 'application/json;charset=UTF-8', // http 请求默认格式
    errorMessage: true, //是否错误提示
    loading: true, //异步请求是否显示加载动画
    loadingLock: true, //异步加载动画是否锁屏
    timeout: 60000 //htttp请求超时时间
  }
};
