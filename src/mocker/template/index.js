const mpmprojectinfo = Object.assign(
  {},
  {
    // 按条件分页查询
    'POST /api/demo/pms/mpmprojectinfo/MpmProjectInfoRest/searchByPage/v1': (req, res) => {
      return res.json({
        "responseBody": {
          "pageParameter": {
            "rows": 20,
            "page": 1,
            "totalPage": 1,
            "totalCount": 6
          },
          "result": [
            {
              "id": "402880e771a751f60171a751f6980000",
              "projectCode": "906",
              "projectName": "项目名称906",
              "projectManager": "8a69ee397114903301714f102be413ca",
              "projectManagerAlias": "马彭泽",
              "projectDept": "8a69ee397114903301714f04028013ba",
              "projectDeptAlias": "生产管理业务部",
              "projectRole": "1",
              "projectGroup": "1",
              "projectPosition": "1",
              "projectStatus": "1",
              "projectTechs": "1",
              "secretLevel": "1",
              "projectContractAmount": 0,
              "projectStartTime": "2020-04-14",
              "createdBy": "1",
              "creationDate": "2020-04-23T13:54:58.000+0000"
            },
            {
              "id": "8a69dacb7187475f017187475f3f0000",
              "projectCode": "001",
              "projectName": "项目名称001",
              "projectManager": "8a69ee397114903301714f102be413ca",
              "projectManagerAlias": "马彭泽",
              "projectDept": "8a69ee397114903301714f04028013ba",
              "projectDeptAlias": "生产管理业务部",
              "projectRole": "1",
              "projectGroup": "1",
              "projectPosition": "1",
              "projectStatus": "1",
              "projectTechs": "1",
              "secretLevel": "1",
              "projectContractAmount": null,
              "projectStartTime": null,
              "createdBy": "8a69dafc6dcd5770016dd7b8063f026c",
              "creationDate": "2020-04-17T08:35:33.000+0000"
            }
          ]
        },
        "message": null,
        "errorDesc": null,
        "retCode": "200"
      });
    },
    // 按条件不分页查询
    'POST /api/demo/pms/mpmprojectinfo/MpmProjectInfoRest/search/v1': (req, res) => {
      return res.json({
        "responseBody": [
          {
            "id": "8a69dacb7187475f017187475f3f0000",
            "projectCode": "001",
            "projectName": "001",
            "projectManager": "8a69ee397114903301714f102be413ca",
            "projectManagerAlias": "马彭泽",
            "projectDept": "8a69ee397114903301714f04028013ba",
            "projectDeptAlias": "生产管理业务部",
            "projectRole": "1",
            "projectGroup": "1",
            "projectPosition": "1",
            "projectStatus": "1",
            "projectTechs": "1",
            "secretLevel": "1",
            "projectContractAmount": null,
            "projectStartTime": null,
            "createdBy": "8a69dafc6dcd5770016dd7b8063f026c",
            "creationDate": "2020-04-17T08:35:33.000+0000"
          },
          {
            "id": "402880e771a751f60171a751f6980000",
            "projectCode": "906",
            "projectName": "项目名称383",
            "projectManager": "8a69ee397114903301714f102be413ca",
            "projectManagerAlias": "马彭泽",
            "projectDept": "8a69ee397114903301714f04028013ba",
            "projectDeptAlias": "生产管理业务部",
            "projectRole": "1",
            "projectGroup": "1",
            "projectPosition": "1",
            "projectStatus": "1",
            "projectTechs": "1",
            "secretLevel": "1",
            "projectContractAmount": 0,
            "projectStartTime": "2020-04-14",
            "createdBy": "1",
            "creationDate": "2020-04-23T13:54:58.000+0000"
          }
        ],
        "message": null,
        "errorDesc": null,
        "retCode": "200"
      });
    },
    // 通过主键查询单条记录
    'GET /api/demo/pms/mpmprojectinfo/MpmProjectInfoRest/get/v1/:id': (req, res) => {
      return res.json({
        "responseBody": null,
        "message": null,
        "errorDesc": null,
        "retCode": "200"
      });
    },
    // 新增对象
    'POST /api/demo/pms/mpmprojectinfo/MpmProjectInfoRest/save/v1': (req, res) => {
      return res.json({
        "responseBody": "4028802b71aa2b440171aa2b440b0000",
        "message": null,
        "errorDesc": null,
        "retCode": "200"
      });
    },
    // 修改全部对象字段
    'POST /api/demo/pms/mpmprojectinfo/MpmProjectInfoRest/updateAll/v1': (req, res) => {
      return res.json({
        "responseBody": 1,
        "message": null,
        "errorDesc": null,
        "retCode": "200"
      });
    },
    // 按主键单条删除
    'POST /api/demo/pms/mpmprojectinfo/MpmProjectInfoRest/deleteById/v1': (req, res) => {
      return res.json({
        "responseBody": 1,
        "message": null,
        "errorDesc": null,
        "retCode": "200"
      });
    }
  }
)

module.exports = mpmprojectinfo;
