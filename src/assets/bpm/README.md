
2、目录规划方案说明
      
      文件夹通过v1 和 v2 区分新旧版本的图标
      
      - bpm
        - config 流程绘图的xml 文件
             -  bpmn-xml-v1    绘制使用bpmn   规范的图标 流程图 xml文件
                     wfeditor-commons.xml    绘制 使用bpmn 规范的图标的流程图 xml 文件
                     wfgraph-commons.xml     绘制 使用bpmn 规范的图标的流程图 xml 文件
                     workfloweditor.xml      绘制 使用bpmn 规范的图标的流程图 xml 文件
             -  bpmn-xml-v2  绘制使用bpmn 规范的图标 流程图 xml文件
                     wfeditor-commons.xml    绘制 使用新版简化图标的流程图 xml 文件
                     wfgraph-commons.xml     绘制 使用新版简化图标的流程图 xml 文件
                     workfloweditor.xml      绘制 使用新版简化图标的流程图 xml 文件
             
        - images 
             - bpmn-img-v1  bpmn 规范图标 
               - default-icon    bpmn 规范的默认图标 
               - over-icon       bpmn 规范的运行完成的流程图标
               - run-icon        bpmn 规范的运行中的流程图标
               
             - bpmn-img-v2  像素为48px的新版简化图标   
               - norun-icon      标记还没运行到和可能就不会运行的节点图标
               - over-icon       运行完成的流程图标
               - run-icon        运行中的流程图标
               - default-icon          流程默认图标 
                   end_event_terminate.png  节点默认图标
                   gateway_exclusive.png    节点默认图标
                    ...         
         - mxgraph  mxgraph 插件的相关资源 
