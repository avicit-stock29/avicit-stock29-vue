1、目录说明

     styles 与 theme 文件合并到styles 文件中，通过子文件夹区分与皮肤相关的样式

     - styles
       - common 与换肤无关的样式统一存放在commom 文件夹下
             base-style.less
             base-flow-detail.less
             base-form.less
             base-modal.less
             base-surface-panel.less
             base-surface-toolbar.less
             base-table.css
             base-theme.less   主题的公共样式
             base-tree-form.less

       - skin   与换肤相关的样式   （说明:与主题相关样式均抽离到下方theme/layout/ThemePublicStyle主题文件夹中）
             antv-skin.less
             avic-skin.less
             components-skin.less
             flow-skin.less
             skin-loader.less
             skin-register.less
             themes-skin.less   主题的换肤文件

        avic-default.less   圆角大小、高度、颜色等公共变量定义文件
        README.md  描述每个文件夹用途

2、common 文件夹说明

| 文件名称                  | 文件说明                                                     | 文件路径 url |
| ------------------------- | ------------------------------------------------------------ | ------------ |
| base-flow-detail.less     | 流程详情页表单内容的公共样式                                 | ----         |
| base-form.less            | 公共表单的样式                                               | ----         |
| base-modal.less           | 添加、编辑、详情弹窗的样式                                   | ---          |
| base-style.less           | 覆盖原生标签的样式                                           | ----         |
| base-surface-panel.less   | 模板页面的 panel                                             | ----         |
| base-surface-toolbar.less | 添加按钮、编辑按钮、高级查询、普通输入框所在区域的工具条样式 | ----         |
| base-table.less           | 原生表格的样式，（目前只有四表联动使用）                     | ----         |
| base-theme.less           | 主题的公共样式                                               | ----         |
| base-tree-form.less       | 树单表的表单样式                                             | ----         |

3、skin 文件夹说明

| 文件名称               | 文件说明                                     | ---- |
| ---------------------- | -------------------------------------------- | ---- |
| antv-skin.less         | 覆盖原生 antd 的样式，实现原生组件的换肤效果 | ---- |
| avic-skin.less         | 与皮肤相关的需要换肤的样式                   | ---  |
| components-skin.less   | 组件的样式                                   | ---- |
| flow-skin.less         | 流程相关组件的样式                           | ---- |
| **_skin-loader.less_** | **_主题颜色定义文件，全局样式注册文件_**     | ---- |
| skin-register.less     | 组件、主题等换肤文件注册入口                 | ---- |
| themes-skin.less       | 只加载作用于全局的、需要跟随皮肤换色的样式   | ---- |
